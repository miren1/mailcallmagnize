<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CallingController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//http://chetantrivedi.in/Xcalling/fallback/voice.php (twilio old link)
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('/incoming-call', [CallingController::class,'incomingcall']);
Route::any('/gather-url', [CallingController::class,'incominggather']);
Route::post('/gather-url2', [CallingController::class,'incominggather2']);

Route::any('/record-url', [CallingController::class,'incomingrecord']);
Route::any('/call-satatus', [CallingController::class,'callStatus']);
Route::any('/recive-sms', [CallingController::class,'reciveSms']);
Route::any('/message-gather-url', [CallingController::class,'MessageGather']);
Route::any('/get-sms-status-inbox', [CallingController::class,'getMessageStatus']);



// Route::get('migrate', function () {
//     Artisan::call('migrate');
//     dd("Cache is cleared");
// });


