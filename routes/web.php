<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\SignupValidationController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
//login check
Route::post('/login-check',[App\Http\Controllers\Auth\LoginController::class,'logincheck'])->name('logincheck');

Route::get('/mail/send',[App\Http\Controllers\MailController::class,'send'])->name('mailsend');

Route::get('/test-tw',[App\Http\Controllers\UserController::class,'testtw']);
Route::get('/',[FrontController::class,'index'])->name('fronthome');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/register-user',[App\Http\Controllers\UserController::class,'registerdata'])->name('register-user');
Route::get('/refillaccount',[FrontController::class,'refillaccount'])->name('refillaccount');
Route::post('/inmatesphonenumber',[FrontController::class,'inmatesphonenumber'])->name('inmatesphonenumber');
/*Route::post('/inmatesphonenumber',[FrontController::class,'inmatesphonenumber'])->name('inmatesphonenumber');*/
Route::get('/check-inmate-number',[FrontController::class,'checkNumber']);

Route::get('/inmate-name-check',[SignupValidationController::class,'checkNameNumber']);

Route::get('/area-code-check',[SignupValidationController::class,'areaCodeCheck']);

Route::get('/contact-detail-check',[SignupValidationController::class,'contactDetailsCheck']);

Route::get('/last-question-check',[SignupValidationController::class,'lastquestionCheck']);

Route::get('/password-check',[SignupValidationController::class,'passwordCheck']);

Route::get('/help',[FrontController::class,'help'])->name('help');

Route::get('/about',[FrontController::class,'aboutus'])->name('aboutus');

Route::get('/#features',[FrontController::class,'index'])->name('features');

Route::get('/#pricing', [FrontController::class, 'index'])->name('pricing');

Route::get('/#faq', [FrontController::class, 'index'])->name('faq');

Route::get('/#feedback',[FrontController::class,'index'])->name('feedback');

Route::get('/#contact',[FrontController::class,'index'])->name('contact');

Route::get('/new/user/{id}',[App\Http\Controllers\UserController::class ,'newuserRegister'])->name('new.user');

Route::post('/new/user/register',[App\Http\Controllers\UserController::class,'newuserupdate'])->name('updatenewuser');

Route::post('check-details-newuser',[App\Http\Controllers\UserController::class,'checkandupdatenewuser'])->name('checkandupdatenewuser');

Route::post('register-via-link',[App\Http\Controllers\UserController::class,'registervialink'])->name('registervialink');

Route::get('select-number',[SignupValidationController::class, 'areaCodeCheck'])->name('selectNumber');


Route::group(['middleware' => ['auth','checkadmin']], function () {
    //Admin route
    Route::get('/amazonepolly',[App\Http\Controllers\PriceController::class,'amazonepolly'])->name('amazonepolly');
    Route::post('/Amazonepollyupdate',[App\Http\Controllers\PriceController::class,'Amazonepollyupdate'])->name('Amazonepollyupdate');
    Route::get('/Users',[App\Http\Controllers\UserController::class,'index'])->name('users');
    Route::get('/users/list',[App\Http\Controllers\UserController::class,'userslist'])->name('users.list');
    Route::get('/user/add',[App\Http\Controllers\UserController::class,'adduser'])->name('add.user');
    Route::post('/user/store',[App\Http\Controllers\UserController::class,'storeuser'])->name('user.store');
    Route::get('/users_accept/{id}/{type}',[App\Http\Controllers\UserController::class,'users_accept'])->name('users.accept');
    Route::get('/viewuser/{id}',[App\Http\Controllers\UserController::class,'viewuser'])->name('viewuser');
    Route::get('/todaynewuser',[App\Http\Controllers\UserController::class,'todaynewuser'])->name('todaynewuser.list');
   Route::get('/allpendingplace',[App\Http\Controllers\UserController::class,'allpendingplace'])->name('allpendingplace.list');
    //Route::get('/rejecteduser',[App\Http\Controllers\UserController::class,'rejecteduser'])->name('rejecteduser.list');
    Route::get('/price',[App\Http\Controllers\PriceController::class,'index'])->name('price');
    Route::post('/price',[App\Http\Controllers\PriceController::class,'create']);
    Route::get('place',[App\Http\Controllers\PlaceController::class,'index'])->name('place');
    Route::get('/place/list',[App\Http\Controllers\PlaceController::class,'placelist'])->name('place.list');
    Route::get('/place/add',[App\Http\Controllers\PlaceController::class,'create'])->name('place.add');
    Route::post('/place/store',[App\Http\Controllers\PlaceController::class,'store'])->name('place.store');
    Route::get('/place/edit/{id}',[App\Http\Controllers\PlaceController::class,'edit'])->name('place.edit');
    Route::post('/place/update/{id}',[App\Http\Controllers\PlaceController::class,'update'])->name('place.update');
    Route::delete('/place/delete/{id}',[App\Http\Controllers\PlaceController::class,'delete'])->name('place.delete');
    Route::get('/editprofile',[App\Http\Controllers\UserController::class,'editprofile'])->name('editprofile');
    Route::post('/editprofile/update/{id}',[App\Http\Controllers\UserController::class,'editprofile_update'])->name('adminprofile');
    
    Route::get('Admin/changepassword',[App\Http\Controllers\ChangePasswordController::class,'adminindex'])->name('adminchangepassword');
    Route::post('update/changepassword',[App\Http\Controllers\ChangePasswordController::class,'adminupdatechangepassword'])->name('adminupdatechangepassword');    
    // Route::get('/changepassword',[App\Http\Controllers\ChangePasswordController::class,'index'])->name('changepassword.admin');
    // Route::post('change-password',[App\Http\Controllers\ChangePasswordController::class,'store'])->name('changepassword.store');
    Route::get('Pendingusers',[App\Http\Controllers\UserController::class,'pendinguserslist'])->name('pendinguser.list');
    Route::get('/ivr',[App\Http\Controllers\IvrController::class,'index'])->name('ivr');
    Route::get('/ivr_list',[App\Http\Controllers\IvrController::class,'ivr_list'])->name('avr_list');
    // Route::post('/ivr_list_ajax',[App\Http\Controllers\IvrController::class,'ivr_list_ajax'])->name('ivr_list_ajax');
    Route::get('/addIvr',[App\Http\Controllers\IvrController::class,'addIvr'])->name('addIvr');
    Route::get('/Ivr/edit/{id}',[App\Http\Controllers\IvrController::class,'editIvr'])->name('Ivr.edit');
    Route::post('/Ivr/update/{id}',[App\Http\Controllers\IvrController::class,'ivrupdate'])->name('ivrupdate');
    Route::post('/ivrstore',[App\Http\Controllers\IvrController::class,'ivrstore'])->name('ivrstore');
    Route::delete('/Ivr/delete/{id}',[App\Http\Controllers\IvrController::class,'Ivrdelete'])->name('Ivrdelete');
    Route::get('pendingnumbers',[App\Http\Controllers\PlaceController::class,'pendingnumbers'])->name('pendingnumbers');
    Route::get('/alltransaction',[App\Http\Controllers\UserController::class,'allttransaction'])->name('allttransaction');
    Route::get('/alltransaction_list',[App\Http\Controllers\UserController::class,'transactionhistory_list'])->name('alltransaction');
     Route::get('/prison_numberAccept/{id}/{type}',[App\Http\Controllers\PlaceController::class,'prison_numberAccept'])->name('prison_number.accept');
});
Route::group(['middleware'=>['auth','checkuser']],function () {

    Route::get('/calls',[App\Http\Controllers\FrontController::class,'calls'])->name('calls');
    Route::get('/calls_list',[App\Http\Controllers\FrontController::class,'calls_list'])->name('calls_list');
    Route::get('/profile',[App\Http\Controllers\ProfileController::class,'profile'])->name('profile');
    Route::post('/refillwallet',[App\Http\Controllers\RefillaccountController::class,'refillcharges'])->name('refillcharges');
    Route::get('/transactionhistory',[App\Http\Controllers\FrontController::class,'transactionhistory'])->name('transactionhistory');
    Route::get('/transactionhistory_list',[App\Http\Controllers\FrontController::class,'transaction_list'])->name('transaction.list');
    Route::post('/updateprofile',[App\Http\Controllers\ProfileController::class,'updateprofile'])->name('updateprofile');
    Route::get('/changepassword',[App\Http\Controllers\ProfileController::class,'changepassword'])->name('changepassword');
    Route::post('/updatechangepassword',[App\Http\Controllers\ProfileController::class,'updatechangepassword'])->name('updatechangepassword');
    Route::post('/refillexistingcard',[App\Http\Controllers\ProfileController::class,'refillexistingcard'])->name('refillexistingcard');

    Route::get('/accountRefill',[App\Http\Controllers\RefillaccountController::class,'refill'])->name('refill');

    
    Route::get('/history',[App\Http\Controllers\RefillaccountController::class,'history'])->name('history');

    Route::get('/canclesubscription',[App\Http\Controllers\RefillaccountController::class,'canclesubscription'])->name('canclesubscription');
    Route::post('/subscription',[App\Http\Controllers\RefillaccountController::class,'subscription'])->name('subscription');
    Route::get('/voicemail&message',[App\Http\Controllers\FrontController::class,'voicemail_message'])->name('voicemail&message');
    Route::get('/voicemail_list',[App\Http\Controllers\FrontController::class,'voicemail_list'])->name('voicemail_list');
    Route::get('/message_list',[App\Http\Controllers\FrontController::class,'message_list'])->name('message_list');
});
Route::post('/subcribtionResponse',[App\Http\Controllers\RefillaccountController::class,'subcribtionResponse']);

Route::get('artisan',function(){

    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    return '<h1>Clea r Config cleared</h1>';
});
Route::get('env',function(){
    dd(env('STRIPE_PUBLISHABLE_KEY'));
});