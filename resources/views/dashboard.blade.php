  @extends('Admin.master')
  @section('content')
      
  <!-- Start Content-->
  <div class="container-fluid">
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>  
    <!-- end page title --> 
@if(Auth::user()->type == 'admin')
    <div class="row">
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
                                <i class="fe-user font-22 avatar-title text-primary"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-end">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$totaluser}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total<br> All Users</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div>
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
        
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                                <i class="fe-user font-22 avatar-title text-success"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-end">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$newuser}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total <br>New User</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div>
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
        
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                                <i class="fe-map-pin font-22 avatar-title text-info"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-end">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$pendingplace}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total<br> Pending Place</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div>
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                                <i class="fe-dollar-sign font-22 avatar-title text-info"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-end">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$rifillaccount}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total <br>Refill Amount</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div>
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
    </div>
    <!-- end row-->   
      
    <div class="row">    
    <div class="col-md-6 ">
            <div class="card">
                <div class="card-body">
                    <div class="float-end">
                    </div>
                <h4 class="page-title-box">New Users</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                <table id="todaynewuser" class="table dt-responsive nowrap w-100">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Inmate's Name</th>
                                            <th>User's Name</th>
                                            <th>User's Number</th>
                                            <th>User's Email</th>
                                            </tr>
                                    </thead>
                                </table>
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
                    </div>
                    <!-- end row -->
                </div>
            </div> <!-- end card -->
    </div>
    <!-- end row -->
    <div class="col-md-6 ">
        <div class="card">
            <div class="card-body">
                <div class="float-end">
                </div>
            <h4 class="page-title-box">Pending Place</h4>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                            <table id="allpendingplace" class="table dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Prison Type</th>
                                      <th>Prison Number</th>
                                      <th>Name</th>
                                      <th>Location</th>
                                      <th>Status</th>
                                      <th width="100px">Action</th>
                                    </tr>
                                </thead>
                               
                            </table>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
                </div>
                <!-- end row -->
            </div>
        </div> <!-- end card -->
</div>

</div> <!-- container -->

@endif

@if(Auth::user()->type == 'user')

<div class="row">
   @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

        <div class="col-md-6 col-xl-4">
            <div class="widget-rounded-circle card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                                <i class="fe-phone-call font-22 avatar-title text-success"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-end">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{App\Models\Calls::where('message_type','=','call')->where('user_id','=',Auth::user()->id)->count()}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Calls </p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div>
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
        
        <div class="col-md-6 col-xl-4">
            <div class="widget-rounded-circle card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                                <i class="fe-voicemail font-22 avatar-title text-info"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-end">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{App\Models\Recording::where('type','=','voice')->where('user_id','=',Auth::user()->id)->count()}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total VoiceMail</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div>
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
        <div class="col-md-6 col-xl-4">
            <div class="widget-rounded-circle card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                                <i class="fe-message-square font-22 avatar-title text-info"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-end">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">{{App\Models\Recording::where('type','=','message')->where('user_id','=',Auth::user()->id)->count()}}</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Messages</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div>
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
    </div>
    <!-- end row-->   
    
        </div>
    </div> <!-- end card -->
</div>
<!-- end row -->
    </div>


@endif


</div> <!-- content -->

@endsection
@section('script')
<script type="text/javascript">

 $(function () {
      
      var table = $('#todaynewuser').DataTable({
              searching: false,
              processing: true,
              serverSide: true,
              dataType: "json",
              pageLength: 5,
              type: "get",
              ajax: {
                  url: "/todaynewuser",
                  dataType: "json",
                  type: "get",
                  data: {
                      receipt_no: function() {
                          return $("#receipt_no").val();
                      },                },
              },
              columns: [{
                      data: 'DT_RowIndex',
                      orderable: false,
                      searchable: false,
                      targets: 0
                  },
                  {
                      data: 'inmates_name',
                  },
                  {
                      data: 'name',
                  },
                  {
                      data: 'number',
                  },
                  {
                      data: 'email',
                  },
              ]
          });

      var table = $('#allpendingplace').DataTable({
              searching: false,
              processing: true,
              serverSide: true,
              dataType: "json",
              pageLength: 5,
              type: "get",
              ajax: {
                  url: "/allpendingplace",
                  dataType: "json",
                  type: "get",
                  data: {
                      receipt_no: function() {
                          return $("#receipt_no").val();
                      },                },
              },
              columns: [{
                      data: 'DT_RowIndex',
                      orderable: false,
                      searchable: false,
                      targets: 0
                  },
                  {
                      data: 'prison_type',
                  },
                  {
                      data: 'prison_number',
                  },
                  {
                      data: 'prison_name',
                  },
                  {
                      data: 'location',
                  },
                  {
                      data: 'status',
                  },
                  {
                      data: 'action',
                      orderable: false,
                      searchable: false,
                  },
              ]
          });
      $(document).on('click','.serviacceptbtn' , function(e){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id  = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        e.preventDefault();
        // alert(type);
    Swal.fire({
        title: 'Are you sure?',
        text: 'You want to accept this Prison Number',
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: `Yes`,
        denyButtonText: `No`,
        }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {

            var data = {

                "_token": "{{ csrf_token() }}",
                "id": id,
                "data-type": type,
            };

            $.ajax({

                type: "get",
                url: '/prison_numberAccept/'+ id + '/'+ type,   
                data: data,
                success: function(result){
                    if(result.success){
                        Swal.fire({
                            title:'Prison Number Successfully Accepted',
                            icon:"success",
                        });
                      table.ajax.reload();
                    }
                //    $('#users').data.reload();
                //    table.reload();
                }
                
            })
         

        } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
        }
        })
    });
    $(document).on('click','.servirejecttbtn' , function(e){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id  = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        e.preventDefault();
        // alert(type);
    Swal.fire({
        title: 'Are you sure?',
        text: 'You want to reject this Prison Number',
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: `Yes`,
        denyButtonText: `No`,
        }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {

            var data = {

                "_token": "{{ csrf_token() }}",
                "id": id,
                "data-type": type,
            };

            $.ajax({

                type: "get",
                url: '/prison_numberAccept/'+ id + '/'+ type,   
                data: data,
                success: function(result){
                    if(result.success){
                        Swal.fire({
                            title: 'Prison Number Successfully Rejected',
                            icon:"success",
                        });
                      table.ajax.reload();
                    }
                //    $('#users').data.reload();
                //    table.reload();
                }
                
            })
         

        } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
        }
        })
    });
   @if(Session::has('error'))
        Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: '{{ Session::get("error") }}'
    })
    @endif   
});
</script>
@endsection

