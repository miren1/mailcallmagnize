<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Log In | MailCall Communication</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- App css -->
        <link href="{{asset('css/config/creative/bootstrap.min.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
        <link href="{{asset('css/config/creative/app.min.css')}}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

        <link href="{{asset('css/config/creative/bootstrap-dark.min.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
        <link href="{{asset('css/config/creative/app-dark.min.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />
         <!-- Sweet Alert-->
         <link href="{{asset('libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- icons -->
        <link href="{{asset('css/icons.min.css')}}" rel="stylesheet" type="text/css" />

    </head>

    <body class="loading authentication-bg authentication-bg-pattern" style="background-color:#1A7BFF;">

        <div class="account-pages mt-5 mb-5" >
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-4">
                        <div class="card bg-pattern">

                            <div class="card-body p-4">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="text-center w-75 m-auto">
                                    <div class="auth-logo">
                                        {{-- <a href="index.html" class="logo logo-dark text-center">
                                        <span class="logo-lg">
                                        <img src="{{asset('images/logo-dark.png')}}" alt="" height="22">
                                        </span>
                                        </a> --}}
                                        <a href="{{route('home')}}" class="logo logo-light text-center">
                                            <span class="logo-lg">
                                                <img src="{{asset('images/login-logo.png')}}" alt="" height="34">
                                            </span>
                                        </a>
                                    </div>
                                    <p class="text-muted mb-4 mt-3">Enter your email address and get reset password link. </p>
                                </div>
                                <form method="POST" action="{{ route('password.email') }}">
                                    @csrf

                                    <div class="mb-3">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                                    <div class="mb-3" style="text-align: center;">
                                        <!-- <div class="col-md-8 offset-md-4"> -->
                                            <button type="submit" class="btn btn-primary">
                                                Send Password Reset Link
                                            </button>
                                        <!-- </div> -->
                                    </div>
                                </form>
                            </div> <!-- end card-body -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->
        <!-- Vendor js -->
        <script src="{{asset('js/vendor.min.js')}}"></script>
         <!-- Sweet Alerts js -->
         <script src="{{asset('libs/sweetalert2/sweetalert2.all.min.js')}}"></script>
        <!-- App js -->
        <script src="{{asset('js/app.min.js')}}"></script>       
    </body>
</html>
