
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Log In | MailCall Communication</title>
         <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 
		<!-- App css -->
		<link href="{{asset('css/config/creative/bootstrap.min.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
		<link href="{{asset('css/config/creative/app.min.css')}}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

		<link href="{{asset('css/config/creative/bootstrap-dark.min.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
		<link href="{{asset('css/config/creative/app-dark.min.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />
         <!-- Sweet Alert-->
         <link href="{{asset('libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
		<!-- icons -->
		<link href="{{asset('css/icons.min.css')}}" rel="stylesheet" type="text/css" />
		

    </head>

    <body class="loading authentication-bg authentication-bg-pattern" style="background-color:#1A7BFF;">

        <div class="account-pages mt-5 mb-5" >
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-4">
                        <div class="card bg-pattern">

                            <div class="card-body p-4">
                                
                                <div class="text-center w-75 m-auto">
                                    <div class="auth-logo">
                                        {{-- <a href="index.html" class="logo logo-dark text-center">
                                            <span class="logo-lg">
                                                <img src="{{asset('images/logo-dark.png')}}" alt="" height="22">
                                            </span>
                                        </a> --}}
                    
                                        <a href="{{route('home')}}" class="logo logo-light text-center">
                                            <span class="logo-lg">
                                                <img src="{{asset('images/login-logo.png')}}" alt="" height="34">
                                            </span>
                                        </a>
                                    </div>
                                    
                                    <p class="text-muted mb-4 mt-3">Enter your email address and password to access admin panel.</p>
                                </div>
                                <form method="POST" action="{{ route('login') }}" id="loginform">
                                    @csrf
            
                                    <div class="mb-3">
                                        <label for="email" class="form-label">{{ __('Email Address') }}</label>
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Enter your email" value="{{ old('email') }}">
                                            <a style="color: red;" ><span id="emailerror"></span></a>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        
                                    </div>
            
                                    <div class="mb-3">
                                        <label for="password" class="form-label">{{ __('Password') }}</label>
                                        <div class="input-group input-group-merge">    
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Enter your password">
                                            <div class="input-group-text" data-password="false">
                                                <span class="password-eye"></span>
                                            </div>

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                       <a style="color: red;"><span id="passworderror"></span></a>
                                    </div>
            
                                    <div class="mb-3">
                                      <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember">
            
                                                <label class="form-check-label" for="remember">
                                                    {{ __('Remember Me') }}
                                                </label>
                                            </div>
                                       </div>
            
                                    <div class="text-center d-grid">
                                        <button type="button" class="btn btn-primary" id="submitbtn">
                                                {{ __('Login') }}
                                            </button>
                                        </div>
                                </form>
                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                <p> <a href="{{url('/password/reset')}}" class="text-white-50 ms-1">Forgot your password?</a></p>
                                <p class="text-white-50">Don't have an account? <a href="{{route('register')}}" class="text-white ms-1"><b>Sign Up</b></a></p>
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->
        <!-- Vendor js -->
        <script src="{{asset('js/vendor.min.js')}}"></script>
         <!-- Sweet Alerts js -->
         <script src="{{asset('libs/sweetalert2/sweetalert2.all.min.js')}}"></script>
        <!-- App js -->
        <script src="{{asset('js/app.min.js')}}"></script>
        <script type="text/javascript">

            $(document).ready(function(){

                $(document).on('keydown','#email',function(){
                    $('#emailerror').html('');
                });
                $(document).on('keydown','#password',function(){
                    $('#passworderror').html('');
                });


                $('#submitbtn').click(function(){
                    var email  = $("#email").val();
                    var password = $("#password").val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                                                
                        method: "POST",
                        url: 'login-check',
                        dataType: "json",
                        data: {  email:email, password:password , _token: '{{ csrf_token() }}' },
                        success: function (response) {
                           if (response.status=='false') {
                             var password = $("#password").val();
                             if (password) {

                             // Swal.fire({
                             //      title: 'Wait for admin conformation!',
                             //      icon: 'warning',
                             //      confirmButtonText: 'Ok'
                             //    })
                             }else
                             {
                                $("#passworderror").html('This field is required!');
                             }
                            // location.reload();
                        }else if (response.status=='not_exists') {
                            var email = $("#email").val();
                            var password = $("#password").val();
                            if (email == '' && password == '') {
                                $("#passworderror").html('This field is required!');
                                $("#emailerror").html('This field is required!');
                                return false;
                            }else{

                            Swal.fire({
                                  title: 'Email is not registered!',
                                  icon: 'warning',
                                  confirmButtonText: 'Ok'
                                })
                            }

                        }else
                        {
                            
                            $('#loginform').submit();
                            //submit form

                              /* Swal.fire({
                                  title: 'Good job',
                                  text: 'waiting for admin confirmation!',
                                  icon: 'warning',
                                  timer: 3000,
                                  confirmButtonText: 'Ok'
                                })*/
                        }                                                          
                    },
                        error: function (textStatus, errorThrown) {
                            Success = false;//doesn't go here
                        }
                    });   
                });
            });
        </script>        
    </body>
</html>
