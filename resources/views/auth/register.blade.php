<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Register & Signup | MailCall Communication</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}">

		<!-- App css -->
		<link href="{{asset('css/config/creative/bootstrap.min.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
		<link href="{{asset('css/config/creative/app.min.css')}}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

		<link href="{{asset('css/config/creative/bootstrap-dark.min.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
		<link href="{{asset('css/config/creative/app-dark.min.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />

		<!-- icons -->
		<link href="{{asset('css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Sweet Alert-->
         <link href="{{asset('libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
          <link href="{{asset('css/step.css')}}" rel="stylesheet" type="text/css" />
          


    </head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <body class="loading authentication-bg authentication-bg-pattern" style="background-color:#1A7BFF;">

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 ">
                        <div class="card bg-pattern">

                            <div class="card-body">
                                
                                <div class="text-center w-75 m-auto">
                                    <div class="auth-logo">
                                        <a href="{{route('home')}}" class="logo logo-dark text-center">
                                            <span class="logo-lg">
                                                <img src="{{asset('images/login-logo.png')}}" alt="" height="34">
                                            </span>
                                        </a>
                                    </div>
                                    <!-- <p class="text-muted mb-4 mt-3">To get started, we need information about your inmate.</p> -->
                                    &nbsp;
                                    &nbsp;
                                </div>

                                <div class="row justify-content-center">
                                {{--<div class="card">
                                    <div class="card-body" style="height: 15rem;">
                                        <div class="container">
                                             <div class="panel">
                                                <div class="panel-body wizard-content"> --}}
                                                    <form id="example-form" action="#" class="tab-wizard wizard-circle wizard clearfix wizard-validation">
                                                        <h6>Step1</h6>
                                                        <section>
                                                            <br/>
                                                             <div class="row">
                                                            <h4 style="color: black; font-size: 21px;font-family:'Nunito',sans-serif;">To get started, we need information about your inmate.</h4>
                                                            <!-- <div class="col-12"> -->
                                                                <div class="row mb-3">
                                                                    <label class="col-xs-4 form-label" for="inmates_name">Inmate's name</label>
                                                                    <div class="col-xs-4">
                                                                        <input type="text" class="form-control" placeholder="Enter Inmate's name" id="inmates_name" name="inmates_name" required>
                                                                         <div class="invalid-feedback">
                                                                           This field is required.
                                                                          </div>
                                                                    </div>
                                                                    <span class="text-danger inmates_name_error"></span>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label class="col-xs-4 form-label" for="inmatedocnumber">Inmate's DOC number</label>
                                                                    <div class="col-xs-4">
                                                                  <input type="number" id="doc_number" min="0" placeholder="Enter Inmate's DOC number" name="doc_number" class="form-control" required>
                                                                    </div>
                                                                    <span class="text-danger doc_number_error"></span>
                                                                </div>
                                                                
                                                                <div class="row mb-3">
                                                                    <label class="col-xs-4 form-label" for="inmates_location">Inmate's location(optional)</label>
                                                                    <div class="col-xs-4">
                                                                        <input type="text" id="inmates_location" placeholder="Enter Inmate's location(optional)" name="inmates_location" class="form-control">
                                                                    </div>
                                                                </div>
                                                            <!-- </div> end col -->
                                                            <div class="d-flex justify-content-end">
                                                                <button type="button" class="btn btn-primary" id="first_next">Next</button>
                                                            </div>
                                                            </div> <!-- end row -->
                                                            &nbsp;                 
                                                        </section>
                                                        <h6>Step2</h6>
                                                        <section>
                                                            <div class="row">
                                                            <h4 style="color: black; font-size: 21px;font-family:'Nunito',sans-serif;">Choose the area code that you'd like for <a style="color:red;"><span id="nameofinmates"></span>'s</a> new number. For some customers, local calls are cheaper than long distance calls, so consider an area code local to their facility.</h4>

                                                            <div class="col-12">
                                                                <div class="row mb-3">
                                                                    <label class="col-xs-4 form-label" for="area_code">Area code</label>
                                                                    <div class="col-xs-4">
                                                                        <input type="number" min="0" class="form-control" placeholder="Enter Area code" id="area_code" name="area_code">
                                                                    </div>
                                                                    <span class="text-danger area_code_error"></span>
                                                                </div>
                                                                <div class="col-12"> 
                                                                <div class="row mb-3"> 
                                                                <div class="col-xs-4">          
                                                                <button class="btn btn-success" id="getNumber">Get Number</button>
                                                                </div> 
                                                                </div>
                                                            </div>
                                                                <div class="col-12">
                                                                <div class="row mb-3">
                                                                    <div class="col-xs-4">
                                                                        <select name="select" id="selectnumber" class="form-control" required>
                                                                          <option value="null">Please select Number</option>
                                                                         
                                                                          </select>
                                                                          <span class="text-danger selectnumber_error"></span>
                                                                        <span class="text-danger select_number_error"></span>
                                                                      </div>
                                                                    </div>
                                                                </div>      
                                                            </div> <!-- end col -->
                                                             <div class="d-flex justify-content-end">
                                                                <button type="button" class="btn btn-primary mt-4 b-0 area_prev">Prev</button>
                                                                &nbsp;
                                                                <button type="button" class="btn btn-primary float-right mt-4 b-0" id="second_next">Next</button>
                                                            </div>
                                                        </div> <!-- end row -->
                                                            &nbsp;      
                                                        </section>
                                                        <h6>Step3</h6>
                                                        <section>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                        <h4 style="color: black; font-size: 21px;font-family:'Nunito',sans-serif;">Are you <a style="color:red;"><span id="nameinmates"></span>'s</a> main contact?</h4>
                                                                    <div class="row mb-3">
                                                                            <div class="col-xs-4">
                                                                                <button type="button" id="Yes" class="form-control btn btn-info">Yes, I am</button>
                                                                            </div>
                                                                    </div>
                                                                    <div class="row mb-3">
                                                                         <div class="col-xs-4">
                                                                        <button type="button" id="No" class="form-control btn btn-info">No, I am not</button>
                                                                             
                                                                         </div>
                                                                    </div>
                                                                </div> 
                                                                 <div class="d-flex justify-content-end">
                                                                <button type="button" class="btn btn-primary float-right mt-4 b-0 area_prev">Prev</button>
                                                            </div>
                                                             </div> 
                                                        </section>
                                                        <h6>Test</h6>
                                                        <section>
                                                            <div class="row">
                                                            <h4 style="color: black; font-size: 21px;font-family:'Nunito',sans-serif;">Enter your contact information. <a style="color: red;font-weight: bold;"><span id="contact-information"></span>'s</a> will have quick access to your number from the main menu when they call.</h4>
                                                            <div class="col-12">
                                                                <div class="row mb-3">
                                                                    <label class="col-xs-4 form-label" for="name">Contact name</label>
                                                                    <div class="col-xs-4">
                                                                        <input type="text" id="contactname" placeholder="Enter your name" name="name" class="form-control" required>
                                                                    </div>
                                                                    <span class="text-danger contact_name_error"></span>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label class="col-xs-4 form-label" for="number">Contact phone number</label>
                                                                    <div class="col-xs-4">
                                                                        <input type="text" id="number" pattern="[0-9]+" placeholder="Enter phone number" name="number" class="form-control" required>
                                                                    </div>
                                                                    <span class="text-danger contact_number_error"></span>

                                                                </div>
                                                                
                                                                <div class="row mb-3">
                                                                    <label class="col-xs-4 form-label" for="email">Contact email address</label>
                                                                    <div class="col-xs-4">
                                                                        <input type="email" id="email" placeholder="Enter email address" name="email" class="form-control" required>
                                                                    </div>
                                                                    <span class="text-danger contact_email_error"></span>

                                                                    <span class="text-danger email_error"></span>
                                                                </div>
                                                            </div> <!-- end col -->
                                                               <div class="d-flex justify-content-end">
                                                                <button type="button" class="btn btn-primary mt-4 b-0 area_prev">Prev</button>
                                                                &nbsp;
                                                                <button type="button" class="btn btn-primary float-right mt-4 b-0" id="fourth_next">Next</button>
                                                            </div>
                                                        </div> <!-- end row -->
                                                        </section>
                                                        <h6>New test</h6>
                                                        <section>
                                                            <div class="row">
                                                            <h4 style="color: black; font-size: 21px;font-family:'Nunito',sans-serif;">Just one last question. How did you hear about Mailcall Communication?</h4>

                                                            <div class="col-12">
                                                                <div class="row mb-3">
                                                                    <label class="col-xs-4 form-label" for="myselect">Select the option</label>
                                                                    <div class="col-xs-4">
                                                                        <select name="lastquestion" id="myselect" class="form-control" required>
                                                                           <option selected disabled>Please Select...</option>
                                                                           <option value="Inmate Request">Inmate Request</option>
                                                                           <option value="Refferd By Friend">Refferd By Friend</option>
                                                                           <option value="Prison Legal News">Prison Legal News</option>
                                                                           <option value="Inmate Shopper">Inmate Shopper</option>
                                                                           <option value="Facebook">Facebook</option>
                                                                           <option value="Twiter">Twiter</option>
                                                                           <option value="None of the above">None of the above</option>
                                                                          </select>
                                                                    </div>
                                                                    <span class="text-danger select_error"></span>
                                                                </div>                                                              
                                                            </div> <!-- end col -->
                                                             <div class="d-flex justify-content-end">
                                                                <button type="button" class="btn btn-primary mt-4 b-0 area_prev">Prev</button>
                                                                &nbsp;
                                                                <button type="button" class="btn btn-primary float-right mt-4 b-0" id="fifth_next">Next</button>
                                                            </div>
                                                        </div> <!-- end row -->
                                                        </section>
                                                        <h6>Test</h6>
                                                        <section>
                                                            <div class="row">
                                                            <h4 style="color: black; font-size: 21px;font-family:'Nunito',sans-serif;">Enter information about <a style="color:red;"><span id="inmatenames"></span></a> main contact security</h4>
                                                            <div class="col-12">
                                                                <div class="row mb-3">
                                                                    <label class="col-xs-4 form-label" for="prison_number">Prison Number(Optional)</label>
                                                                    <div class="col-xs-4">
                                                                        <input type="number" min="0" class="form-control" placeholder="Enter prison number(optional)" id="prison_number" name="prison_number">
                                                                    </div>
                                                                    <span class="text-danger prison_number_error"></span>
                                                                </div>
                                                                <div class="row mb-3">
                                                                                              
                                                                        <label for="password" class="form-labe">Password</label>
                                                                         <div class="col-xs-4">
                                                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Enter your password" name="password" required>
                                                                         </div>  
                                                                         
                                                                   <span class="text-danger password_error"></span>
                                                                </div>
                                                                
                                                                  <div class="row mb-3">
                                                                                            
                                                                    <label for="password-confirm" class="form-labe">Confirm Password</label>
                                                                     <div class="col-xs-4">
                                                                    <input type="password" class="form-control" placeholder="Enter your Confirm password" name="password_confirmation" id="password_confirmation" required>

                                                                    </div>                                                           
                                                                     <span class="text-danger confirm_password_error"></span>
                                                                </div>
                                                            </div> <!-- end col -->
                                                              <div class="d-flex justify-content-end">
                                                                <button type="button" class="btn btn-primary mt-4 b-0 area_prev">Prev</button>
                                                                &nbsp;
                                                                <button type="button" class="btn btn-primary float-right mt-4 b-0" id="six_next">Next</button>
                                                            </div>
                                                        </div> <!-- end row -->
                                                        </section>
                                                        <h6>test</h6>
                                                        <section>
                                                            <div class="row">
                                                            <h4 style="color: black; font-size: 21px;font-family:'Nunito',sans-serif;">How much money would you like to put on the account?</h4>
                                                            <div class="col-12">
                                                                <div class="row mb-3">
                                                                    <div class="col-xs-4">
                                                                        <select name="lastquestion" id="amount_added" class="form-control" required>
                                                                          <option value="{{$data->phone_number}}">${{$data->phone_number}}</option>
                                                                          <option value="10">$10</option>
                                                                          <option value="15">$15</option>
                                                                          <option value="20">$20</option>
                                                                          <option value="25">$25</option>
                                                                          <option value="30">$30</option>
                                                                          <option value="35">$35</option>
                                                                          <option value="40">$40</option>
                                                                          <option value="45">$45</option>
                                                                          <option value="50">$50</option>
                                                                          <option value="60">$60</option>
                                                                          <option value="70">$70</option>
                                                                          <option value="80">$80</option>
                                                                          <option value="90">$90</option>
                                                                          <option value="100">$100</option>
                                                                          <option value="110">$110</option>
                                                                          <option value="120">$120</option>
                                                                          <option value="130">$130</option>
                                                                          <option value="140">$140</option>
                                                                          <option value="150">$150</option>
                                                                          <option value="160">$160</option>
                                                                          <option value="170">$170</option>
                                                                          <option value="180">$180</option>
                                                                          <option value="190">$190</option>
                                                                          <option value="200">$200</option>
                                                                          </select>
                                                                          <div class="col-xs-4">
                                                                              <p style="color: black; font-size: 21px;font-family:'Nunito',sans-serif;">Now we need your billing information. $<a style="color:red;"><span id="amountadded"></span></a> will be billed to your account immediately and we will provide a new, private, phone number for your inmate. The balance on the account will be $<span id="amountadded"></span>, credit that he or she can use to make calls and leave messages.</p>
                                                                          </div>
                                                                            <div class="col-xs-4">
                                                                                <div class="row">
                                                                                       <label>
                                                                                          <div id="card-element"></div>
                                                                                        </label>

                                                                                        <label for="card-element"></label>
                                                                                        <div id="card-element"></div>
                                                                                </div>
                                                                            </div>
                                                            
                                                                            <input id="cardID" name="cardID" class="form-control" type="hidden"value="">
                                                                          <button type="button" class="form-control btn btn-info" id="submitBtn">Purchase</button>
                                                                    </div>
                                                                </div>                                                           <div class="d-flex justify-content-end">
                                                                <button type="button" class="btn btn-primary float-right area_prev">Prev</button>
                                                            </div>     
                                                            </div> <!-- end col -->
                                                            
                                                        </div> <!-- end row -->
                                                        </section>
                                                    </form>
                                                {{-- </div>
                                            </div> 
                                        </div>
                                        <!-- end step wizard -->
                                    </div>
                                </div>--}}
                            </div>
                                {{-- <div id="wizard">
                                    <h1>First Step</h1>
                                    <div>First Content</div>
                                 
                                    <h1>Second Step</h1>
                                    <div>Second Content</div>
                                </div> --}}
                            
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->
        <!-- Vendor js -->
        <script src="{{asset('js/vendor.min.js')}}"></script>

        <!-- App js -->
        <script src="{{asset('js/app.min.js')}}"></script>
          <!-- Sweet Alerts js -->
         <script src="{{asset('libs/sweetalert2/sweetalert2.all.min.js')}}"></script>

        <script src="{{asset('js/jquery.steps.min.js')}}"></script>
        <!-- <script src="{{asset('js/jquery.steps.js')}}"></script> -->

        <script src="https://js.stripe.com/v3/"></script>
        
        <script>




        var stepsWizard;
        var form = $("#example-form");
        $(document).on('keydown','#inmates_name',function(){
            $('.inmates_name_error').html('');
        });
         $(document).on('keydown','#doc_number',function(){
            $('.doc_number_error').html('');
        });
        $(document).on('keydown','#area_code',function(){
            $('.area_code_error').html('');
            $('.select_number_error').html('');

        });
         $(document).on('keydown','#contactname',function(){
            $('.contact_name_error').html('');
        });
        $(document).on('keydown','#number',function(){
            $('.contact_number_error').html('');
        });
         $(document).on('keydown','#email',function(){
            $('.contact_email_error').html('');
        });
        $(document).on('keydown','#myselect',function(){
            $('.select_error').html('');
        }); 
        $(document).on('keydown','#prison_number',function(){
            $('.prison_number_error').html('');
        });
         $(document).on('keydown','#password',function(){
            $('.password_error').html('');
        });
        $(document).on('keydown','#password_confirm',function(){
            $('.confirm_password_error').html('');
        });

         $(document).on('click','#first_next',function(){
            var inmates_name = $('#inmates_name').val();
            $('#nameofinmates').html(inmates_name);

         });
         $(document).on('click','#second_next',function(){
            var inmates_name = $('#inmates_name').val();
            $('#nameinmates').html(inmates_name);

         });

        $(document).on('click','#first_next',function(){
            var inmates_name = $('#inmates_name').val();
            var doc_number = $('#doc_number').val();

             //$('.inmates_name_error').val();
            $.ajax({
                url: '{{ url('inmate-name-check') }}',
                dataType:'json',
                method:'GET',
                data:{inmates_name:inmates_name, doc_number:doc_number},
                success:function(data){
                    if(data.status == 'false'){
                        if(data.error.inmates_name !== undefined){
                            $('.inmates_name_error').html(data.error.inmates_name[0]);
                        }
                        if(data.error.doc_number !== undefined){
                             $('.doc_number_error').html(data.error.doc_number[0]);
                        }
                    }else{
                        stepsWizard.steps("next");
                    }
                },error:function(error){
                    console.log(error);
                }
            })
        });
        $(document).on('click','#second_next',function(){
         var area_code = $('#area_code').val();   
         var selectnumber = $('#selectnumber').val();
         if (area_code == '') {
            $('.area_code_error').html('The area code field is required');
         }else if(selectnumber == 'null'){
            $('.selectnumber_error').html('This field is required')
         }else{
            stepsWizard.steps("next");
         }
        });
         $(document).on('click','#getNumber',function(){
            var area_code = $('#area_code').val();
            $.ajax({
                url: '{{ url('area-code-check') }}',
                dataType:'json',
                method:'GET',
                data:{ area_code:area_code },
                success:function(data){
                    if(data.status == 'false'){
                        // console.log();
                        if(data.error.area_code !== undefined){
                            $('.area_code_error').html(data.error.area_code[0]);
                        }
                    }else if(data.status == 'notfound'){
                        $('.select_number_error').html('Number Not Fount! Please Enter valid Area Code.');
                    }else{
                        $.each(data.data, function(key, value){  
                            $('#selectnumber')
                             .append($("<option></option>")
                                        .attr("value", value)
                                        .text(value)); 
                             // $("#selectnumber").html(data.data);
                        });  
                        // $.each(data.data, function (i) {
                        //     $('#selectnumber').html();
                        // console.log(data.data);
                        // stepsWizard.steps("next");
                    // }
                    }
                },error:function(error){
                    console.log(error);
                }
            })
        });
        $(document).on('click','#Yes',function(){
            var inmates_name = $('#inmates_name').val();
            $('#contact-information').html(inmates_name);
            stepsWizard.steps("next");
        });
        $(document).on('click','#No',function() {
            var inmates_name = $('#inmates_name').val();
            $('#contact-information').html(inmates_name);
            stepsWizard.steps("next");
        });  
         $(document).on('click','.area_prev',function(){
            stepsWizard.steps("previous");
        });
        $(document).on('click','#fourth_next',function(){
            var contactname = $('#contactname').val();
            var contactnumber = $('#number').val();
            var contactemail = $('#email').val();

             //$('.inmates_name_error').val();
            $.ajax({
                url: '{{ url('contact-detail-check') }}',
                dataType:'json',
                method:'GET',
                data:{contactname:contactname, contactnumber:contactnumber, contactemail:contactemail },
                success:function(data){
                    if(data.status == 'false'){
                        if(data.error.contactname !== undefined){
                            $('.contact_name_error').html(data.error.contactname[0]);
                        }
                        if(data.error.contactnumber !== undefined){
                            $('.contact_number_error').html(data.error.contactnumber[0]);
                        }
                        if(data.error.contactemail !== undefined){
                            $('.contact_email_error').html(data.error.contactemail[0]);
                        }
                    }else{
                        stepsWizard.steps("next");
                    }
                },error:function(error){
                    console.log(error);
                }
            }) 
        });
        $(document).on('click','#fifth_next',function(){
             //$('.inmates_name_error').val();

             var select = $("#myselect").val();

            $.ajax({
                url: '{{ url('last-question-check') }}',
                dataType:'json',
                method:'GET',
                data:{select:select },
                success:function(data){
                    if(data.status == 'false'){
                        
                        if(data.error.select !== undefined){
                            $('.select_error').html(data.error.select[0]);
                        }
                    }else{
                        stepsWizard.steps("next");
                    }
                },error:function(error){
                    console.log(error);
                }
            }) 
        });
         $(document).on('click','#six_next',function(){
             //$('.inmates_name_error').val();
             
             var prison_number = $("#prison_number").val();
             var password = $("#password").val();
             var password_confirmation = $("#password_confirmation").val();
             // alert(password);

            $.ajax({
                url: '{{ url('password-check') }}',
                dataType:'json',
                method:'GET',
                data:{prison_number:prison_number, password:password, password_confirmation:password_confirmation },
                success:function(data){
                    if(data.status == 'false'){
                         if(data.error.prison_number !== undefined){
                            $('.prison_number_error').html(data.error.prison_number[0]);
                        }
                        if(data.error.password !== undefined){
                            $('.password_error').html(data.error.password[0]);
                        }
                        if(data.error.password_confirmation !== undefined){
                            $('.confirm_password_error').html(data.error.password_confirmation[0]);
                        }
                    }else{
                        stepsWizard.steps("next");
                    }
                },error:function(error){
                    console.log(error);
                }
            }) 
        });
        
        
         
        $(document).ready(function(){
            
            stepsWizard = form.steps({
                    headerTag: "h6",
                    bodyTag: "section",
                    transitionEffect: "fade",
                  titleTemplate: '<span class="step">#index#</span> #title#',
                });
            $("a[href$='next']").hide();
            $("a[href$='previous']").hide();
            $("a[href$='finish']").hide();
            //$("#wizard").steps();
            var stripe = Stripe('{{env('STRIPE_PUBLISHABLE_KEY')}}');
            var elements = stripe.elements();

            // Custom styling can be passed to options when creating an Element.
            var style = {
                  base: {
                    // Add your base input styles here. For example:
                    fontSize: '16px',
                    color: '#32325d',
                  },
            };

            // Create an instance of the card Element.
            var card = elements.create('card', {style: style});

            // Add an instance of the card Element into the `card-element` <div>.
            card.mount('#card-element');
           


            $("#submitBtn").click(function(){  
            $(this).prop( "disabled", true );
            stripe.createToken(card).then(function(result) {
                    if (result.error) {
                      // Inform the customer that there was an error.
                      var errorElement = document.getElementById('card-errors');
                      errorElement.textContent = result.error.message;
                    } else {
                        // console.log(result.token.id);
                      // Send the token to your server.
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        var inmates_name = $("#inmates_name").val();
                        var doc_number  = $("#doc_number").val();
                        var inmates_location = $("#inmates_location").val();
                        var area_code = $("#area_code").val();
                        var name = $("#contactname").val();
                        var number = $("#number").val();
                        var email = $("#email").val();
                        var myselect = $("#myselect").val();
                        var amount_added = $("#amount_added").val();
                        var password = $("#password").val();
                        var password_confirm = $("#password_confirm").val();
                        var card_token = $("#cardID").val();
                        var prison_number = $("#prison_number").val();
                        var selectnumber = $('#selectnumber').val();

                           
                         $.ajax({
                            
                            method: "POST",
                            url: "/register-user",
                            dataType: "json",
                            data: { inmates_name: inmates_name, doc_number: doc_number, inmates_location:inmates_location,selectnumber:selectnumber,
                                area_code:area_code, name:name, number:number, email:email, myselect:myselect, amount_added:amount_added, prison_number:prison_number, password:password, password_confirm:password_confirm, card_token:result.token.id
                             },
                            success: function (response) {
                               if (response.success) {
                                Swal.fire({
                                  title: 'SignUp Successfully!',
                                  showDenyButton: false,
                                  showCancelButton: false,
                                  confirmButtonText: `ok`,
                                  denyButtonText: `cancel`,
                                }).then((result) => {
                                  if (result.isConfirmed) {
                                    window.location.href = '{{url('/')}}';
                                  } 
                                })

                               }
                            },
                            error: function (textStatus, errorThrown) {
                                $(this).prop( "disabled", false );    
                                Success = false;//doesn't go here
                            }

                           });
                     }
                  }); 
                
            });

            var amount_added = $("#amount_added").val();
            $("#amountadded").text(amount_added);

            $("#amount_added").change(function(){
                var amount_added = $("#amount_added").val();
                $("#amountadded").text(amount_added);
            });

             $("#next").click(function(){
                var inmates_name = $("#inmates_name").val();
                $("#inmatenames").text(inmates_name);
                
             });
        });
</script>
        
    </body>
</html>




