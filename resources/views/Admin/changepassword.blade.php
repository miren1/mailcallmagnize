@extends('Admin.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Change Password</li>
                    </ol> 
                </div>
                <h4 class="page-title">Change Password</h4>
            </div>
        </div>
    </div>    
 
<div class="row"style="margin-left: 33%;">
<div class="col-lg-6">
    <div class="card" >
        <div class="card-body">
                <div class="text-center mt-2 mb-4">
                    <h2><i class="fas fa-key"></i> Change Password</h2>
                </div>
               
            <form method="POST" action="{{route('adminupdatechangepassword')}}" class="needs-validation" novalidate >
                    @csrf

                   
                <div class="mb-3">
                  <label for="password" class="form-labe">{{ __('Current Password') }}</label>
                    <div class="input-group input-group-merge"> 
                        <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password" required placeholder="Enter current password">
                        <div class="input-group-text" data-password="false">
                            <span class="password-eye"></span>
                        </div>
                    </div>
                    <div class="invalid-feedback">
                        This Current Password is required.
                    </div>
                    @if(session('error'))
                      <div style="color: red; font-weight:bold;">{{session('error')}}</div>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="password" class="form-labe">{{ __('New Password') }}</label>
                    <div class="input-group input-group-merge"> 
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" required placeholder="Enter your new password" name="password" value="">
                        <div class="input-group-text" data-password="false">
                            <span class="password-eye"></span>
                        </div>
                    </div>
                    @error('password')
                                  <div style="color: red;font-weight: bold;">{{ $message }}</div>
                    @enderror
                     <div class="invalid-feedback">
                            This New Password is required.
                        </div>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-labe">{{ __('New Confirm Password') }}</label>
                     <div class="input-group input-group-merge"> 
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" required placeholder="Enter your confirm password" name="password_confirmation" value="">
                         <div class="input-group-text" data-password="false">
                            <span class="password-eye"></span>
                        </div>
                    </div>
                    <div class="invalid-feedback">
                        This New Confirm Password is required.
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-success submit">
                        {{ __('Submit') }}
                    </button>
                </div>
            </form>

         </div>
    </div>
</div>
</div><!-- end row -->
 </div> 
@endsection
