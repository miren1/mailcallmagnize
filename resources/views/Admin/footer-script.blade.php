        <!-- Vendor js -->
        <script src="{{asset('js/vendor.min.js')}}"></script>

        <!-- Plugins js-->
        <script src="{{asset('libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{asset('libs/parsleyjs/parsley.min.js')}}"></script>


        <!-- App js-->
        <script src="{{asset('js/app.min.js')}}"></script>


        <!-- third party js -->
        <script src="{{asset('libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{asset('libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="{{asset('js/pages/datatables.init.js')}}"></script>

         <!-- Sweet Alerts js -->
         <script src="{{asset('libs/sweetalert2/sweetalert2.all.min.js')}}"></script>
          <!-- Tippy js-->
        <script src="{{asset('libs/tippy.js/tippy.all.min.js')}}"></script>

        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

        <!-- Plugins js-->
        <script src="{{asset('libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>

        <!-- Init js-->
        <script src="{{asset('js/pages/form-wizard.init.js')}}"></script>

        <!-- stripe payment -->
        <script src="https://js.stripe.com/v3/"></script>

        
        