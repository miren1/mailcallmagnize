@extends('Admin.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">editprofile</li>
                    </ol> 
                </div>
                <h4 class="page-title">Edit Profile</h4>
            </div>
        </div>
    </div>      
   
<div class="row">
    <div class="card">
        <div class="card-body">
                <div class="text-center mt-2 mb-4">
                    <h2><i class="fe-user"></i>Edit Profile</h2>
                </div>

                <form method="POST" action="{{route('adminprofile',Auth::user()->id)}}" class="needs-validation" novalidate >
                    @csrf

                    <div class="mb-3">
                        <label for="name" class="form-labe">{{ __('Name') }}</label>
                            <input id="name" type="text" class="form-control " name="name" required placeholder="Enter your name" value="{{Auth::user()->name}}">
                            <div class="invalid-feedback">
                                This Name is required.
                            </div>
                    </div>

                    <div class="mb-3">
                        <label for="email" class="form-labe">{{ __('Email Address') }}</label>
                            <input id="email" type="email" class="form-control" required placeholder="Enter your email address" name="email" value="{{Auth::user()->email}}">
                            <div class="invalid-feedback">
                                This Email is required.
                            </div>
                    </div>
                    <div class="mb-3">
                        <label for="number" class="form-labe">{{ __('Phone Number') }}</label>
                            <input id="number" type="text" class="form-control @error('number') is-invalid @enderror" required placeholder="Enter your number" name="number" value="{{Auth::user()->number}}">
                            @error('number')
                                <div style="color: red; font-weight:bold;">{{ $message }}</div>
                            @enderror
                            
                        </div>
                    <div class="text-center">
                    
                            <button type="submit" class="btn btn-success submit">
                                {{ __('Submit') }}
                            </button>
                    </div>
                </form>

            </div>
        </div>
     </div>
     <!-- end row -->
 </div> 
</div>




@endsection