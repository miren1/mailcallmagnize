@extends('Admin.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Voice Setting</li>
                    </ol> 
                </div>
                <h4 class="page-title">Voice Setting</h4>
            </div>
        </div>
    </div>      
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

<div class="row">
    <div >
    <div class="card">
        <div class="card-body">
                <!-- <div class="text-center mt-2 mb-4"> -->
                    <h2><i class="fas fa-bullhorn"></i> Voice Setting</h2>
                <!-- </div> -->

                <form method="POST" action="{{route('Amazonepollyupdate')}}" class="needs-validation" novalidate >
                    @csrf
                    <label for="name">{{ __('Select Amazon Polly Voices') }}</label>
                <div class="col-md-4">

                    <div class="input-group input-group-sm">
                     <select class="form-control" name="voice_type">
                        <option value="Polly.Joanna"{{ ($voicetype->voice_type) == 'Polly.Joanna' ? 'selected' : '' }}>Polly.Joanna(Female)</option>                            
                        <option value="Polly.Ivy" {{ ($voicetype->voice_type) == 'Polly.Ivy' ? 'selected' : '' }}>Polly.Ivy(Female)</option>
                     	<option value="Polly.Joey" {{ ($voicetype->voice_type) == 'Polly.Joey' ? 'selected' : '' }}>Polly.Joey(Male)</option>                        	
                     	<option value="Polly.Justin" {{ ($voicetype->voice_type) == 'Polly.Justin' ? 'selected' : '' }}>Polly.Justin(Male)</option>                       	
                     	<option value="Polly.Kendra"{{ ($voicetype->voice_type) == 'Polly.Kendra' ? 'selected' : '' }} >Polly.Kendra(Female)</option>
                        <option value="Polly.Kimberly"{{ ($voicetype->voice_type) == 'Polly.Kimberly' ? 'selected' : '' }}>Polly.Kimberly(Female)</option>
                        <option value="Polly.Matthew"{{ ($voicetype->voice_type) == 'Polly.Matthew' ? 'selected' : '' }}>Polly.Matthew(Male)</option>
                        <option value="Polly.Salli"{{ ($voicetype->voice_type) == 'Polly.Salli' ? 'selected' : '' }}>Polly.Salli(Female)</option>
                     </select>
                     &nbsp;&nbsp;
                     <div class="input-group-btn">
                            <button type="submit" class="btn btn-success submit form-control">
                                {{ __('Submit') }}
                            </button>
                     </div>   
                     </div> 
                </div>
                
               
                <!-- <div class="text-center"> -->
                <!-- </div> -->
                </form>

            </div>
        </div>
     </div>
     <!-- end row -->
 </div> 
</div>




@endsection