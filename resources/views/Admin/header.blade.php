
  @if(Auth::user()->type == 'admin')
        <!-- Topbar Start -->
        <div class="navbar-custom" style="background-color:#001e30">
            <div class="container-fluid">
                <ul class="list-unstyled topnav-menu float-end mb-0">
                    <li class="dropdown notification-list topbar-dropdown">

                        <a class="nav-link dropdown-toggle nav-user me-0 waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <img src="{{asset('images/users/user-1.jpg')}}" alt="user-image" class="rounded-circle">
                            <span class="pro-user-name ms-1">
                                {{Auth::user()->name}} <i class="mdi mdi-chevron-down"></i>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end profile-dropdown ">
                            <!-- item-->
                            <div class="dropdown-header noti-title">
                                <h6 class="text-overflow m-0">Welcome !</h6>
                            </div>

                            <!-- item-->
                            <a href="{{route('editprofile')}}" class="dropdown-item notify-item">
                                <i class="fe-user"></i>
                                <span>Edit Profile</span>
                            </a>

                            <!-- item-->
                            <a href="{{route('adminchangepassword')}}" class="dropdown-item notify-item">
                                <i class="fas fa-key"></i>
                                <span>Change Password</span>
                            </a>
                             <a href="{{route('amazonepolly')}}" class="dropdown-item notify-item">
                                <i class="fas fa-bullhorn"></i>
                                <span>Voice Setting</span>
                            </a>
                            <div class="dropdown-divider"></div>

                            <!-- item-->
                            <a href="{{ route('logout') }}" class="dropdown-item notify-item"  onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                <i class="fe-log-out"></i>
                                <span>{{ __('Logout') }}</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>

                <!-- LOGO -->
                <div class="logo-box">
                    <a href="{{route('home')}}" class="logo logo-light text-center">
                        <span class="logo-sm">
                            <img src="{{asset('images/mailcall.png')}}" alt="" height="42x">
                        </span>
                        <span class="logo-lg">
                            <img src="{{asset('images/Mailcall-logo.png')}}" alt="" height="40px">
                        </span>
                    </a>
                </div>

                <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                    <li>
                        <button class="button-menu-mobile waves-effect waves-light">
                            <i class="fe-menu"></i>
                        </button>
                    </li>

                    <li>
                        <!-- Mobile menu toggle (Horizontal Layout)-->
                        <a class="navbar-toggle nav-link" data-bs-toggle="collapse" data-bs-target="#topnav-menu-content">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>

                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- end Topbar -->

                    <div class="topnav">
                        <div class="container-fluid">
                            <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

                                <div class="collapse navbar-collapse" id="topnav-menu-content">
                                    <ul class="navbar-nav">
                                        <li class="nav-item">
                                            <a class="nav-link dropdown-toggle arrow-none" href="{{route('home')}}">
                                                <i class="fe-home"></i> Home
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle arrow-none" href="{{route('users')}}">
                                                <i class="fe-user"></i> User Management
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle arrow-none" href="{{route('price')}}" >
                                                <i class="fe-dollar-sign"></i> Price Management
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle arrow-none" href="{{route('place')}}">
                                                <i class="fe-map-pin"></i> Place Management
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle arrow-none" href="{{route('ivr')}}">
                                                <i class="fe-phone"></i> IVR
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle arrow-none" href="{{route('allttransaction')}}">
                                                <i class="fe-dollar-sign"></i> Payment History
                                            </a>
                                        </li>
                                    </ul> <!-- end navbar-->
                                </div> <!-- end .collapsed-->
                            </nav>
                        </div> <!-- end container-fluid -->
                    </div> <!-- end topnav-->
  @endif

  @if(Auth::user()->type == 'user')
   <!-- Topbar Start -->
   <div class="navbar-custom" style="background-color:#001e30">
            <div class="container-fluid">
                <ul class="list-unstyled topnav-menu float-end mb-0">
                    <li class="dropdown d-none d-lg-inline-block topbar-dropdown">
                <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    Balance : ${{Auth::user()->wallet}}
                </a>
            
            </li>
                    <li class="dropdown notification-list topbar-dropdown">
                        <a class="nav-link dropdown-toggle nav-user me-0 waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            
                         
                            <img src="{{asset('images/users/user-1.jpg')}}" alt="user-image" class="rounded-circle">
                            <span class="pro-user-name ms-1">
                                {{Auth::user()->name}} <i class="mdi mdi-chevron-down"></i>
                            </span>

                        </a>
                        <div class="dropdown-menu dropdown-menu-end profile-dropdown ">
                            <!-- item-->
                            <div class="dropdown-header noti-title">
                                <h6 class="text-overflow m-0">Welcome <a style="color: black; font-size: 17px; font-weight: bold;"> {{Auth::user()->name}}!</a></h6>
                            </div>

                            <!-- item-->
                            <a href="{{route('profile')}}" class="dropdown-item notify-item">
                                <i class="fe-user"></i>
                                <span>Profile</span>
                            </a>
                            <!-- item-->
                            <a href="{{route('refill')}}" class="dropdown-item notify-item">
                                <i class="fas fa-charging-station"></i>
                                <span>Refill Account</span>
                            </a>
                            <div class="dropdown-divider"></div>

                            <!-- item-->
                            <a href="{{ route('logout') }}" class="dropdown-item notify-item"  onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                <i class="fe-log-out"></i>
                                <span>{{ __('Logout') }}</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>

                <!-- LOGO -->
                <div class="logo-box">
                    <a href="{{route('home')}}" class="logo logo-light text-center">
                        <span class="logo-sm">
                            <img src="{{asset('images/mailcall.png')}}" alt="" height="42x">
                        </span>
                        <span class="logo-lg">
                            <img src="{{asset('images/Mailcall-logo.png')}}" alt="" height="40px">
                        </span>
                    </a>
                </div>

                <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                    <li>
                        <button class="button-menu-mobile waves-effect waves-light">
                            <i class="fe-menu"></i>
                        </button>
                    </li>

                    <li>
                        <!-- Mobile menu toggle (Horizontal Layout)-->
                        <a class="navbar-toggle nav-link" data-bs-toggle="collapse" data-bs-target="#topnav-menu-content">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>

                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- end Topbar -->

                    <div class="topnav">
                        <div class="container-fluid">
                            <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

                                <div class="collapse navbar-collapse" id="topnav-menu-content">
                                    <ul class="navbar-nav">
                                        <li class="nav-item">
                                            <a class="nav-link dropdown-toggle arrow-none" href="{{route('home')}}">
                                                <i class="fe-home"></i> Home
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link dropdown-toggle arrow-none" href="{{ route('calls') }}">
                                                <i class="fe-phone-call"></i> Calls
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link dropdown-toggle arrow-none" href="{{ route('voicemail&message') }}">
                                                <i class="fe-voicemail"></i> VoiceMail & <i class="fe-message-square"></i> Message
                                            </a>
                                        </li>
                                         <li class="nav-item">
                                            <a class="nav-link dropdown-toggle arrow-none" href="{{ route('transactionhistory') }}">
                                                <i class="fa fa-history"></i> Transaction History
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link dropdown-toggle arrow-none" href="{{ route('refill') }}">
                                                <i class="fas fa-charging-station"></i> Refill Account
                                            </a>
                                        </li>
                                    </ul> <!-- end navbar-->
                                </div> <!-- end .collapsed-->
                            </nav>
                        </div> <!-- end container-fluid -->
                    </div> <!-- end topnav-->

  @endif

