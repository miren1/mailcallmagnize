<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>New User | MailCall Communication</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- App css -->
		<link href="{{asset('css/config/creative/bootstrap.min.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
		<link href="{{asset('css/config/creative/app.min.css')}}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

		<link href="{{asset('css/config/creative/bootstrap-dark.min.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
		<link href="{{asset('css/config/creative/app-dark.min.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />
         <!-- Sweet Alert-->
         <link href="{{asset('libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
		<!-- icons -->
		<link href="{{asset('css/icons.min.css')}}" rel="stylesheet" type="text/css" />

    </head>

    <body class="loading authentication-bg authentication-bg-pattern" style="background-color:#1A7BFF;">

        <div class="account-pages mt-5 mb-5" >
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-4">
                        <div class="card bg-pattern">

                            <div class="card-body p-4">
                                
                                <div class="text-center w-75 m-auto">
                                    <div class="auth-logo">
                                        {{-- <a href="index.html" class="logo logo-dark text-center">
                                            <span class="logo-lg">
                                                <img src="{{asset('images/logo-dark.png')}}" alt="" height="22">
                                            </span>
                                        </a> --}}
                    
                                        <a href="{{route('home')}}" class="logo logo-light text-center">
                                            <span class="logo-lg">
                                                <img src="{{asset('images/login-logo.png')}}" alt="" height="34">
                                            </span>
                                        </a>
                                    </div>
                                    <p class="text-muted mb-4 mt-3">Enter details and access the panel.</p>
                                </div>
                                <form id="loginform">
                                    @csrf
                                    <input id="userId" name="userId" class="form-control" type="hidden"value="{{$Id}}">
                                    <div class="mb-3">
                                        <label for="password" class="form-label">{{ __('Password') }}</label>
                                        <div class="input-group input-group-merge">    
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Enter your password">
                                        <div class="input-group-text" data-password="false">
                                            <span class="password-eye"></span>
                                        </div>
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                                        <a style="color: red;"><span id="passworderror"></span></a>
                                    </div>
                                    <div class="mb-3">
                                        <label for="password-confirm" class="form-label">{{ __('Confirm Password') }}</label>
                                        <div class="input-group input-group-merge">    
                                        <input id="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder="Enter your confirm password">
                                        <div class="input-group-text" data-password="false">
                                            <span class="password-eye"></span>
                                        </div>
                                        @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                                        
                                    </div>    
                                    <div class="mb-3">
                                        <label for="area_code" class="form-labe">{{ __('Area Code') }}</label>
                                            <input id="area_code" type="number" class="form-control @error('area_code') is-invalid @enderror" name="area_code" placeholder="Enter your area code">
                                            @error('area_code')
                                            <div style="color: red; font-weight:bold;">{{ $message }}</div>
                                        @enderror
                                         <a style="color: red;"><span id="area_code_error"></span></a>

                                    </div>   
                                     <div class="mb-3">
                                        <button type="button" class="btn btn-success" id="getnumber">Get Number</button>

                                    </div>  
                                    <div class="mb-3">
                                        <select id="selectnumber" class="form-control">
                                            <option value="null">Please Select Number</option>
                                        </select>
                                        <span class="text-danger select_number_error"></span>
                                    </div>     
                                    <div class="mb-3">
                                        <label class="form-labe" for="amount_added" >{{ __('How much money would you like to put on the account?') }}</label>
                                            <select name="lastquestion" id="amount_added" class="form-control @error('lastquestion') is-invalid @enderror" required>
                                                                          <option value="{{$plan->phone_number}}">${{$plan->phone_number}}</option>
                                                                          <option value="10">$10</option>
                                                                          <option value="15">$15</option>
                                                                          <option value="20">$20</option>
                                                                          <option value="25">$25</option>
                                                                          <option value="30">$30</option>
                                                                          <option value="35">$35</option>
                                                                          <option value="40">$40</option>
                                                                          <option value="45">$45</option>
                                                                          <option value="50">$50</option>
                                                                          <option value="60">$60</option>
                                                                          <option value="70">$70</option>
                                                                          <option value="80">$80</option>
                                                                          <option value="90">$90</option>
                                                                          <option value="100">$100</option>
                                                                          <option value="110">$110</option>
                                                                          <option value="120">$120</option>
                                                                          <option value="130">$130</option>
                                                                          <option value="140">$140</option>
                                                                          <option value="150">$150</option>
                                                                          <option value="160">$160</option>
                                                                          <option value="170">$170</option>
                                                                          <option value="180">$180</option>
                                                                          <option value="190">$190</option>
                                                                          <option value="200">$200</option>
                                                                          </select>
                                        @error('lastquestion')
                                            <div style="color: red; font-weight:bold;">{{ $message }}</div>
                                        @enderror
                                    <a style="color: red;"><span id="select_error"></span></a>

                                    </div>
                                    <div class="mb-3">
                                        <div class="row"> 
                                            <label>
                                              <div id="card-element"></div>
                                            </label>

                                            <label for="card-element"></label>
                                            <div id="card-element"></div>
                                            <input id="newcardID" name="newcardID" class="form-control" type="hidden"value="">
                                        </div>
                                    </div>
                                    <div class="text-center d-grid">
                                        <button type="button" class="btn btn-primary" id="btnsubmit">
                                            {{ __('Submit') }}
                                        </button>
                                    </div>
                                </form>
                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->
        <!-- Vendor js -->
        <script src="{{asset('js/vendor.min.js')}}"></script>
         <!-- Sweet Alerts js -->
         <script src="{{asset('libs/sweetalert2/sweetalert2.all.min.js')}}"></script>
        <!-- App js -->
        <script src="{{asset('js/app.min.js')}}"></script>
        <script src="https://js.stripe.com/v3/"></script>  

        <script type="text/javascript">
        $(document).on('keydown','#password_confirmation',function(){
            $('#passworderror').html('');
        });    
        $(document).on('keydown','#area_code',function(){
            $('#area_code_error').html('');
        });    
        $(document).on('keydown','#amount_added',function(){
            $('#select_error').html('');
        });   
        
        $(document).ready(function(){

            var stripe = Stripe(env('STRIPE_PUBLISHABLE_KEY'));
            var elements = stripe.elements();
            // Custom styling can be passed to options when creating an Element.
            var style = {
                  base: {
                    // Add your base input styles here. For example:
                    fontSize: '16px',
                    color: '#32325d',
                  },
            };
            // Create an instance of the card Element.
            var card = elements.create('card', {style: style});
            // Add an instance of the card Element into the `card-element` <div>.
            card.mount('#card-element');
           
            $("#btnsubmit").click(function(){
                  $(this).prop( "disabled", true );
                  stripe.createToken(card).then(function(result) {
                    if (result.error) {
                      // Inform the customer that there was an error.
                      var errorElement = document.getElementById('card-errors');
                      errorElement.textContent = result.error.message;
                    } else {
                        // console.log(result.token.id);
                      // Send the token to your server.
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        var password = $("#password").val();
                        var confirmpass = $("#password_confirmation").val();
                        var area_code = $("#area_code").val();
                        var userId = $("#userId").val();
                        var newcardID = $("#newcardID").val();
                        var amount_added = $("#amount_added").val();
                        var selectnumber = $("#selectnumber").val();

                       
                          // alert(selectnumber);
                        
                       

                        $.ajax({
                            method: "POST",
                            url: '{{ url('check-details-newuser')}}',
                            dataType: "json",
                            data: { 
                                    password:password,password_confirmation:confirmpass,area_code:area_code,userId:userId,selectnumber:selectnumber
                                    ,card_token:result.token.id,amount_added:amount_added
                             },
                            success: function(response){
                                    if(response.status == 'false'){
                                         if(response.error.password !== undefined){
                                            $('#passworderror').html(response.error.password[0]);

                                        } 
                                        if(response.error.area_code !== undefined){
                                            $('#area_code_error').html(response.error.area_code[0]);
                                        }
                                    }else if (response.status == 'success'){
                                           Swal.fire({
                                              title: 'Signup Successfully!',
                                              showDenyButton: false,
                                              showCancelButton: false,
                                              confirmButtonText: `ok`,
                                              denyButtonText: `cancel`,
                                            }).then((result) => {
                                              if (result.isConfirmed) {
                                                window.location.href = '{{url('/')}}';
                                              } 
                                            })
                                    }else if (response.status == 'error') {

                                        Swal.fire({
                                                title: 'Somthing is wrong!',
                                                icon: 'warning',                                                
                                                confirmButtonText: 'Ok'
                                          }).then((result) => {
                                                if (result.isConfirmed) {

                                                  location.reload();
                                                }
                                          }); 
                                    }else if(response.status == 'exists'){
                                       Swal.fire({
                                              title: 'User is already register please login!',
                                              showDenyButton: false,
                                              showCancelButton: false,
                                              confirmButtonText: `ok`,
                                              denyButtonText: `cancel`,
                                            }).then((result) => {
                                              if (result.isConfirmed) {
                                                window.location.href = '{{url('/')}}';
                                              } 
                                            })
                                    }    
                            },
                            error: function(textStatus, errorThrown)
                            {

                            }
                        });
                    }
                });    
                     
            });

          $("#getnumber").click(function() {
            var area_code = $("#area_code").val();
            if (area_code == '') {
                $('#area_code_error').html('This Area Code is required!');
            }else{
                $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                });
                var area_code = $("#area_code").val();
                $.ajax({
                            method: "GET",
                            url: '{{ url('select-number')}}',
                            dataType: "json",
                            data: { area_code: area_code },
                            success: function(response){ 
                              if(response.status == 'false'){
                                // console.log(response.error.area_code[0]);
                              if(response.error.area_code !== undefined){
                                 $('#area_code_error').html(response.error.area_code[0]);
                                }
                                // else if(response.status == 'notfound'){
                              //     $('.area_code_error').html('mot found');
                              // }else{
                                  // $.each(response.data.data, function(key, value){  
                                  //     $('#selectnumber')
                                  //      .append($("<option></option>")
                                  //                 .attr("value", value)
                                  //                 .text(value)); 
                                  //      // $("#selectnumber").html(data.data);
                                  // });  
                            // } 
                             }else if(response.status == 'notfound'){
                                 $('#area_code_error').html('Number Not found! Please enter valid Area Code.');
                              }else{
                                $.each(response.data, function(key, value){  
                                  $('#selectnumber')
                                    .append($("<option></option>")
                                      .attr("value", value)
                                      .text(value)); 
                                       // $("#selectnumber").html(data.data);
                                });
                             }    
                            },
                            error: function(textStatus, errorThrown)
                            {

                            }

                });
            }
          });
        });
        </script>
    </body>
</html>
