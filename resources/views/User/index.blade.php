@extends('Admin.master')
@section('content')
                      <!-- Start Content-->
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                                            <li class="breadcrumb-item active">users</li>
                                        </ol>
                                    </div>
                                     <h4 class="page-title">User Management</h4>
                                </div>
                            </div>
                        </div>      
                    <div class="row">
                  @if(session()->has('success'))
                      <div class="alert alert-success">
                          {{ session()->get('success') }}
                      </div>
                  @endif
                  @if(session()->has('error'))
                      <div class="alert alert-danger">
                          {{ session()->get('error') }}
                      </div>
                  @endif
                            <div class="card">
                                <div class="card-body">
                                    <div class="float-end" >
                                      <a href="{{route('add.user')}}" style="margin-left: 20px;"><button type="button" class="btn btn-primary btn-sm">Add User</button> </a>
                                    </div>
                                <h4 class="page-title-box">User Management</h4>
                             
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                <table id="users" class="table dt-responsive nowrap w-100">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Inmate's Name</th>
                                                            <th>User's Name</th>
                                                            <th>User's Number</th>
                                                            <th>User's Email</th>
                                                            <th width="100px">Action</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div> <!-- end card body-->
                                        </div> <!-- end card -->
                                    </div><!-- end col-->
                                </div>
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div> <!-- end card -->
                    </div>
                    <!-- end row -->
@endsection

 
@section('script')
   
<script type="text/javascript">

    $(function () {
      
      var table = $('#users').DataTable({
              processing: true,
              serverSide: true,
              dataType: "json",
              pageLength: 10,
              type: "get",
              ajax: {
                  url: "/users/list",
                  dataType: "json",
                  type: "get",
                  data: {
                      receipt_no: function() {
                          return $("#receipt_no").val();
                      },                },
              },
              columns: [{
                      data: 'DT_RowIndex',
                      orderable: false,
                      searchable: true
                  },
                  {
                      data: 'inmates_name',
                  },
                  {
                      data: 'name',
                  },
                  {
                      data: 'number',
                  },
                  {
                      data: 'email',
                  },
                  {
                      data: 'action',
                      orderable: false,
                      searchable: false,
                  },
              ]
          });
});
</script>
@endsection
