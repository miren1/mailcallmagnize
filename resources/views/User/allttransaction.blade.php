@extends('Admin.master')
@section('content')


<div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                                            <li class="breadcrumb-item active">All Payment history</li>
                                        </ol>
                                    </div>
                                     <h4 class="page-title">All Payment History</h4>
                                </div>
                            </div>
                        </div>      
                    <div class="row">
                            <div class="card">
                                <div class="card-body">
                                    <div class="float-end">
                                    </div>
                                <h4 class="page-title-box">All Payment History</h4>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                <table id="alltrans" class="table dt-responsive nowrap w-100">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Payment Date</th>
                                                            <th>User Name</th>
                                                            <th>Transaction ID</th>
                                                            <th>Amount</th>
                                                            <th>Type</th> 
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div> <!-- end card body-->
                                        </div> <!-- end card -->
                                    </div><!-- end col-->
                                </div>
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div> <!-- end card -->
                    </div>
                    <!-- end row -->
@endsection 
 
@section('script')
   
<script type="text/javascript">

    $(function () {
      
      var table = $('#alltrans').DataTable({
              processing: true,
              serverSide: true,
              dataType: "json",
              pageLength: 10,
              type: "get",
              ajax: {
                  url: "/alltransaction_list",
                  dataType: "json",
                  type: "get",
                  data: {
                      receipt_no: function() {
                          return $("#receipt_no").val();
                      },                },
              },
              columns: [{
                      data: 'DT_RowIndex',
                      orderable: false,
                      searchable: true
                  },
                  {
                      data: 'created_at'
                  },
                  {
                  	  data: 'user_id',
                  },
                  {
                      data: 'transaction_id',
                  },
                  {
                      data: 'amount',
                  },
                  {
                      data: 'type',
                  },
                  {
                      data: 'status',
                  },
              ]
          });
    });
</script>
@endsection
