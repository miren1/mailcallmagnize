@extends('Admin.master')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('users')}}">User</a></li>
                        <li class="breadcrumb-item active">Add</li>

                    </ol>
                </div>
                <h4 class="page-title">Add User</h4>
            </div>
        </div>
    </div>      
  
    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="float-end">
                    <a href="{{route('users')}}"><button type="button" class="btn btn-danger btn-sm"><i class="fe-arrow-left" style="font-size: 15px; font-family: sans-serif;">Back</i></button></a>
                </div>
                 <h4 class="page-title-box">Add User</h4>
                    
				<div class="row">
				    <div class="col-lg">
				        <div class="card">
				            <div class="card-body">
				                <form method="POST" action="{{route('user.store')}}" class="needs-validation" novalidate>
				                    @csrf

				                    <div class="mb-3">
				                        <label for="inmates_name" class="form-labe">{{ __('Inmates Name') }}</label>
				                        <input id="inmates_name" type="text" class="form-control @error('inmates_name') is-invalid @enderror" name="inmates_name" required placeholder="Enter your Inmates Name">   
				                        @error('inmates_name')
				                            <div style="color: red; font-weight:bold;">{{ $message }}</div>
				                        @enderror 
				                        <span id="error_inmates_name" ></span>    
				                    </div>
				                    <div class="mb-3">
				                        <label for="prison_number" class="form-labe">{{ __('Prison Number (Optional)') }}</label>
				                        <input id="prison_number" type="text" class="form-control @error('prison_number') is-invalid @enderror" name="prison_number" placeholder="Enter your Prison Number"> 
				                        @error('prison_number')
				                            <div style="color: red; font-weight:bold;">{{ $message }}</div>
				                        @enderror 
				                    </div>
				                    <div class="mb-3">
				                        <label for="doc_number" class="form-labe">{{ __('Inmates Doc Number') }}</label>
				                        <input id="doc_number" type="number" class="form-control @error('doc_number') is-invalid @enderror" name="doc_number" required placeholder="Enter your Inmates Doc Number" min="0">
				                        <span id="error_doc_number" ></span>
				                        @error('doc_number')
				                            <div style="color: red; font-weight:bold;">{{ $message }}</div>
				                        @enderror            
				                    </div>
				                    <div class="mb-3">
				                        <label for="inmates_location" class="form-labe">{{ __('Inmates Location (Optional)') }}</label>
				                            <input id="inmates_location" type="text" class="form-control @error('inmates_location') is-invalid @enderror" name="inmates_location" placeholder="Enter your Inmates Location">
				                            @error('inmates_location')
				                            <div style="color: red; font-weight:bold;">{{ $message }}</div>
				                        @enderror
				                    </div>
				                    <div class="mb-3">
				                        <label for="name" class="form-labe">{{ __('User Name') }}</label>
				                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="username" required placeholder="Enter your Name">
				                            @error('name')
				                            <div style="color: red; font-weight:bold;">{{ $message }}</div>
				                        @enderror
				                    </div>
				                    <div class="mb-3">
				                        <label for="number" class="form-labe">{{ __('User Phone Number') }}</label>
				                            <input id="number" type="number" class="form-control @error('number') is-invalid @enderror" name="number" min="0" required placeholder="Enter your Phone Number">
				                            @error('number')
				                            <div style="color: red; font-weight:bold;">{{ $message }}</div>
				                        @enderror
				                    </div>
				                    <div class="mb-3">
				                        <label for="email" class="form-labe">{{ __('User Email Address') }}</label>
				                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" required placeholder="Enter your Email Address">
				                            @error('email')
				                            <div style="color: red; font-weight:bold;">{{ $message }}</div>
				                        @enderror
				                    </div>
				                    <div class="text-center">
				                            <button type="submit" class="btn btn-success">
				                                Submit
				                            </button>       
				                    </div>
				                </form>
                            </div>
                        </div> <!-- end card -->
                    </div>
                </div>
            </div>
        </div> <!-- end card -->
</div>
@endsection
