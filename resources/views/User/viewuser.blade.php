@extends('Admin.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">ViewUserDetails</li>
                    </ol> 
                </div>
                <h4 class="page-title">View User Detail</h4>
            </div>
        </div>
    </div>      
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 
<div class="row">
	<div class="col-lg-8">
    <div class="card" style="margin-left: 40%;">
        <div class="card-body">
        	 <div class="float-end">
                   <a href="{{route('users')}}"> <button type="button" class="btn btn-danger btn-sm waves-effect waves-light"><i class="fe-arrow-left">Back</i></button></a>

                </div>
                <div class="text-center mt-2 mb-4">
                    <h2><i class="fe-user"></i>ViewUser Details</h2>
                </div>
                <div class="card">
                    <div class="card-body">
                <table>
                		<tr>
                			<td><label for="name" class="form-labe">{{ __('Inmates Name') }}</label></td>
                			<td><input id="name" type="text" class="form-control" name="name" value="{{$viewuser->inmates_name}}"readonly style="border: none;"></td>
                		</tr>
                		<tr>
                			<td><label for="name" class="form-labe">{{ __('Inmates Doc Number') }}</label></td>
                			<td><input id="name" type="text" class="form-control" name="name" value="{{$viewuser->doc_number}}"readonly style="border: none;"></td>
                		</tr>
                		<tr>
                			<td><label for="name" class="form-labe">{{ __('Area Code') }}</label></td>
                			<td><input id="name" type="text" class="form-control" name="name" value="{{$viewuser->area_code}}"readonly style="border: none;"></td>
                		</tr>
                		<tr>
                			<td><label for="name" class="form-labe">{{ __('User Name') }}</label></td>
                			<td><input id="name" type="text" class="form-control" name="name" value="{{$viewuser->name}}"readonly style="border: none;"></td>
                		</tr>
                		<tr>
                			<td><label for="name" class="form-labe">{{ __('User Number') }}</label></td>
                			<td><input id="name" type="text" class="form-control" name="name" value="{{$viewuser->number}}"readonly style="border: none;"></td>
                		</tr>
                		<tr>
                			<td><label for="name" class="form-labe">{{ __('User Email') }}</label></td>
                			<td><input id="name" type="text" class="form-control" name="name" value="{{$viewuser->email}}"readonly style="border: none;"></td>
                		</tr>
                		<tr>
                			<td><label for="name" class="form-labe">{{ __('Wallet') }}</label></td>
                			<td><input id="name" type="text" class="form-control" name="name" value="{{$viewuser->wallet}}"readonly style="border: none;"></td>
                		</tr>
                </table>
            </div>  
         </div>     
            

            </div>
        </div>
     </div>
     <!-- end row -->
 </div> 
</div>




@endsection