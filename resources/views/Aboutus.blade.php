@extends('Front.master')
@section('content')
   <section class="bg-home bg-gradient" id="home">
    <div class="home-center">
        <div class="home-desc-center">
            <div class="text-center">
                <!-- <div class="justify-text-center"> -->
                    <!-- <div class="col-lg-12"> -->
                        <!-- <div class="justify-text-center"> -->
                                        <p style="color: white; font-size: 39px; font-family: 'Nunito', sans-serif;">About Us</p>
                               
                        <!-- </div>   -->
                  <!--   </div>
             -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container-fluid -->
        </div>
    </div>
</section>

<section class="section-sm" id="aboutus">
    <div class="container-fluid">
  
        <div class="row">         
                <p style="color: black; font-family: Cerebri,sans-serif;font-size: 18px; text-align: justify;">If you have any friends or family members currently incarcerated, we understand how tough and emotionally stressful it can be. To not be able to see your loved ones is stressful enough but add to that your inability to reach out to them apart from the prescribed schedule and you cannot help but feel miserable. We have witnessed countless cases where friends and family members of incarcerated people wanted to leave an urgent message or wanted to share something at quick notice but they were not able to do so.</p> 
                <br>
                <p style="color: black;font-family: Cerebri,sans-serif;font-size: 18px; text-align: justify;">We are not here to just empathize with your situation but to provide relief for your troubles.In fact, we have worked hard to introduce a communication system using which the inmate and their friends and family can stay in touch with ease.Our state-of-the-art and innovative    communication system let’s inmates stay in touch with their loved ones.Every person deserves a chance to stay connected with their loved ones and we have tried very hard to achieve just that.
                </p><br>    
                <p style="color: black; font-family: Cerebri,sans-serif; font-size: 18px; text-align: justify;">Mailcall communication is unique in more than one way as it is a two-way communication system. Using Mailcall communication, you can buy a phone number unique for your incarcerated loved one and connect with them whenever you want. And not just that, using Mailcall communication, you can also send text messages and leave voicemails for the incarcerated person as well. Connecting with imprisoned loved ones was never as easy before.</p> 
                <br>
                <p style="color: black;font-family: Cerebri,sans-serif; font-size: 18px; text-align: justify;">At Mailcall communication, we have the utmost love and respect for your privacy and therefore we have taken extra steps to ensure that your communication with your loved ones remains private and secure. Mailcall communication operates with complete licensing and authorizations from all the state and federal authorities.</p>
                <br>
                <p style="color: black; font-family: Cerebri,sans-serif; font-size: 18px; text-align: justify;">We, at Mailcall Communication, work as a close unit and try to produce the best possible results as a team. Working as a close unit allows us to come up with results that are hard for any single individual to achieve. MailCall Communication aims to bring even more comfort and relief to its customers and we are doing our best to bring more innovation, every step of the way.</p> 
        </div>
    </div> 
</section>
<!-- end features -->
@endsection