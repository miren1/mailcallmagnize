@extends('Admin.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('place')}}">Place</a></li>
                        <li class="breadcrumb-item active">Add</li>

                    </ol>
                </div>
                <h4 class="page-title">Add Place</h4>
            </div>
        </div>
    </div>      
  
    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="float-end">
                    <a href="{{route('place')}}"><button type="button" class="btn btn-danger btn-sm"><i class="fe-arrow-left" style="font-size: 15px; font-family: sans-serif;">Back</i></button></a>
                </div>
            <h4 class="page-title-box">Add Place</h4>
                    
<div class="row">
    <div class="col-lg">

        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{route('place.store')}}" class="needs-validation" novalidate>
                    @csrf

                    <div class="mb-3">
                        <label for="prison_type" class="form-labe">{{ __('Prison Type') }}</label>
                        <select id="prison_type" name="prison_type" class="form-control" required>
                            <option value="" disabled selected>Select Prison Type</option>  
                            <option value="Federal Prison">Federal Prison</option>        
                            <option value="State Prison">State Prison</option>        
                            <option value="County Jail">County Jail</option>        
                            <option value="Sheriff's Dept">Sheriff's Dept</option>        
                            <option value="Detention Center">Detention Center</option>        
                            
                          </select>
                        <div class="invalid-feedback">
                               This Prison Type is required.
                            </div>
                    </div>
                    <div class="mb-3">
                        <label for="prison_number" class="form-labe">{{ __('Prison Phone Number') }}</label>
                            <input id="prison_number" type="number" class="form-control" name="prison_number" required placeholder="Enter your prison phone number" min="0">
                            <div class="invalid-feedback">
                                This Prison Phone Number is required.
                            </div>
                            @error('prison_number')
                            <div style="color: red; font-weight:bold;">{{ $message }}</div>
                        @enderror
                        </div>
                    <div class="mb-3">
                        <label for="prison_name" class="form-labe">{{ __('Prison Name') }}</label>
                            <input id="prison_name" type="text" class="form-control" required placeholder="Enter your prision name" name="prison_name">
                            <div class="invalid-feedback">
                                This Name is required.
                            </div>
                        </div>
                    <div class="mb-3">
                        <label for="location" class="form-labe">{{ __('Location') }}</label>
                            <input id="location" type="text" class="form-control" required placeholder="Enter your location" name="location">
                            <div class="invalid-feedback">
                                This Location is required.
                            </div>
                        </div>
                    <div class="text-center">
                       
                            <button type="submit" class="btn btn-success">
                                Submit
                            </button>
                                    
                                </div>
                            </form>
                        </div>
                    </div> <!-- end card -->
                </div>
                <!-- end col -->
           </div>
            <!-- end row -->
        </div>
    </div> <!-- end card -->
</div>
@endsection
