@extends('Admin.master')
@section('content')
   <div class="container-fluid">                                 
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">place</li>
                    </ol>
                </div>
                <h4 class="page-title">Place Management</h4>
            </div>
        </div>
    </div>      
<div class="row">
      @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <div class="card">
            <div class="card-body">            
        
                <div class="row">
                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs">
                                  <li class="nav-item">
                                    <a class="nav-link active" data-bs-toggle="tab" href="#home">Place Management</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" data-bs-toggle="tab" href="#profile">Pending Number</a>
                                  </li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade show active" id="home">
                                    <div class="card">
                              <div class="float-end" style="padding-top: 20px;">
                                  <a href="{{route('place.add')}}" style="margin-left: 20px;"><button type="button" class="btn btn-primary btn-sm">Add Place</button> </a>
                              </div>
                                <div class="card-body">
                                    <table id="places" class="table dt-responsive nowrap w-100">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Prison Type</th>
                                                <th>Prison Number</th>
                                                <th>Name</th>
                                                <th>Location</th>
                                                <th width="100px">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div> <!-- end card body-->
                            </div> <!-- end card -->
  </div>
  <div class="tab-pane fade" id="profile">
                                <div class="row">
                                    
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                <table id="pendingnumbers" class="table dt-responsive nowrap w-100">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Prison Type</th>
                                                            <th>Prison Number</th>
                                                            <th>Name</th>
                                                            <th>Location</th>
                                                            <th>Status</th>
                                                            <th width="100px">Action</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div> <!-- end card body-->
                                        </div> <!-- end card -->
                                    
                                </div>
                                    </div>
                                    <!-- end row -->
                                </div>
                         
  </div>
</div>
                            
                        </div><!-- end col-->
                    </div>
                    <!-- end row-->

                </div>
                <!-- end row -->
            </div>
        </div> <!-- end card -->

</div>
<!-- end row -->
</div>
@endsection

@section('script')
   
<script type="text/javascript">

    $(function () {
      
      var table = $('#places').DataTable({
              processing: true,
              serverSide: true,
              dataType: "json",
              pageLength: 10,
              type: "get",
              ajax: {
                  url: "/place/list",
                  dataType: "json",
                  type: "get",
                  data: {
                      receipt_no: function() {
                          return $("#receipt_no").val();
                      },                },
              },
              columns: [{
                      data: 'DT_RowIndex',
                      orderable: false,
                      searchable: true
                  },
                  {
                      data: 'prison_type',
                  },
                  {
                      data: 'prison_number',
                  },
                  {
                      data: 'prison_name',
                  },
                  {
                      data: 'location',
                  },
                  {
                      data: 'action',
                      orderable: false,
                      searchable: false,
                  },
              ]
          });
  
          $(document).on('click','.servideletebtn' , function(e){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id  = $(this).attr('data-id');
        e.preventDefault();
        // alert(type);
    Swal.fire({
        title: 'Are you sure?',
        text: 'You want to delete the record',
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: `Yes`,
        denyButtonText: `No`,
        }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {

            var data = {

                "_token": "{{ csrf_token() }}",
                "id": id,
            };

            $.ajax({

                type: "DELETE",
                url: '/place/delete/'+ id,   
                data: data,
                success: function(result){
                    if(result.success){
                        Swal.fire({
                          title: 'Your record will be successfully deleted!',
                          icon: "success",
                          confirmButtonText: `Yes`,
                          denyButtonText: `No`,
                        }).then((result)=> {
                          $('#places').DataTable().ajax.reload();
                        });
                      
                    }
                //    $('#users').data.reload();
                //    table.reload();
                }
                
            })
         

        } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
        }
        })
    });



           var table = $('#pendingnumbers').DataTable({
              processing: true,
              serverSide: true,
              dataType: "json",
              pageLength: 10,
              type: "get",
              ajax: {
                  url: "/pendingnumbers",
                  dataType: "json",
                  type: "get",
                  data: {
                      receipt_no: function() {
                          return $("#receipt_no").val();
                      },                },
              },
              columns: [{
                      data: 'DT_RowIndex',
                      orderable: false,
                      searchable: true
                  },
                  {
                      data: 'prison_type',
                  },
                  {
                      data: 'prison_number',
                  },
                  {
                      data: 'prison_name',
                  },
                  {
                      data: 'location',
                  },
                  {
                      data: 'status',
                  },
                  {
                      data: 'action',
                      orderable: false,
                      searchable: false,
                  },
              ]
          });

         $(document).on('click','.serviacceptbtn' , function(e){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id  = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        e.preventDefault();
        // alert(type);
    Swal.fire({
        title: 'Are you sure?',
        text: 'You want to accept this Prison Number',
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: `Yes`,
        denyButtonText: `No`,
        }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {

            var data = {

                "_token": "{{ csrf_token() }}",
                "id": id,
                "data-type": type,
            };

            $.ajax({

                type: "get",
                url: '/prison_numberAccept/'+ id + '/'+ type,   
                data: data,
                success: function(result){
                    if(result.success){
                        Swal.fire({
                            title:'Prison Number Successfully Accepted',
                            icon:"success",
                        });
                      table.ajax.reload();
                    }
                //    $('#users').data.reload();
                //    table.reload();
                }
                
            })
         

        } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
        }
        })
    });
    $(document).on('click','.servirejecttbtn' , function(e){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id  = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        e.preventDefault();
        // alert(type);
    Swal.fire({
        title: 'Are you sure?',
        text: 'You want to reject this Prison Number',
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: `Yes`,
        denyButtonText: `No`,
        }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {

            var data = {

                "_token": "{{ csrf_token() }}",
                "id": id,
                "data-type": type,
            };

            $.ajax({

                type: "get",
                url: '/prison_numberAccept/'+ id + '/'+ type,   
                data: data,
                success: function(result){
                    if(result.success){
                        Swal.fire({
                            title: 'Prison Number Successfully Rejected',
                            icon:"success",
                        });
                      table.ajax.reload();
                    }
                //    $('#users').data.reload();
                //    table.reload();
                }
                
            })
         

        } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
        }
        })
    });

});
</script> 
@endsection
