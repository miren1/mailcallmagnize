@extends('Admin.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('place')}}">Place</a></li>
                        <li class="breadcrumb-item active">Edit</li>

                        </ol>
                    </div>
                    <h4 class="page-title">Edit Place</h4>
                </div>
            </div>
        </div>     
       <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="float-end">
                   <a href="{{route('place')}}"> <button type="button" class="btn btn-danger waves-effect waves-light"><i class="fe-arrow-left">Back</i></button></a>

                </div>
            <h4 class="page-title-box">Edit Place</h4>
                       
                <div class="row">
                    <div class="col-lg">

                        <div class="card">
                            <div class="card-body">
                                <form method="POST" action="{{route('place.update', $place_edit->id)}}"data-parsley-validate="">
                                    @csrf

                                    <div class="mb-3">
                                        <label for="prison_type" class="form-labe">{{ __('Prison Type') }}</label>
                                        <select id="prison_type" name="prison_type" class="form-control" required>
                                            <option value="" disabled selected>Select Prison Type</option>  
                                            <option {{ ($place_edit->prison_type) == 'Federal Prison' ? 'selected' : '' }} value="Federal Prison">Federal Prison</option>        
                                            <option {{ ($place_edit->prison_type) == 'State Prison' ? 'selected' : '' }} value="State Prison">State Prison</option>        
                                        
                                          </select>
                                        <div class="invalid-feedback">
                                               This Prison Type is required.
                                            </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="prison_number" class="form-labe">{{ __('Prison Number') }}</label>
                                            <input id="prison_number" type="text" class="form-control @error('prison_number') is-invalid @enderror" name="prison_number" required="" value="{{$place_edit->prison_number}}">
                                            @error('prison_number')
                                            <div style="color: red; font-weight:bold;">{{ $message }}</div>
                                        @enderror
                                        </div>
                                    <div class="mb-3">
                                        <label for="prison_name" class="form-labe">{{ __('Prison Name') }}</label>
                                            <input id="prison_name" type="text" class="form-control @error('prison_name') is-invalid @enderror" required="" value="{{$place_edit->prison_name}}" name="prison_name">

                                            @error('prison_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="location" class="form-labe">{{ __('Location') }}</label>
                                            <input id="location" type="text" class="form-control @error('location') is-invalid @enderror" required="" value="{{$place_edit->location}}" name="location">

                                            @error('location')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                    </div>
                                    <div class="text-center">
                                    
                                            <button type="submit" class="btn btn-success submit">
                                                {{ __('Submit') }}
                                            </button>
                                        
                                    </div>
                                </form>
                            </div>
                        </div> <!-- end card -->
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
        </div> <!-- end card -->
</div>

@endsection
