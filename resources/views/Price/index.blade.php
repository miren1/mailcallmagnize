@extends('Admin.master')
@section('content')
    
<div class="container-fluid">
                        
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">price</li>
                    </ol>
                </div>
                <h4 class="page-title">Price Management</h4>
            </div>
        </div>
    </div>  
                <div class="row">
                    @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-body">
                                    <h4 class="page-title-box">Price</h4>
                                        <div class="row">
                                            <div class="row-lg-6">
                                                <!-- Simple card -->
                                                <div class="card">
                                                <h2 style="text-align:center; font-size:35px; font-weight:bold; color:#1A7BFF">$@if(isset($data->phone_number)) {{$data->phone_number}}  @endif</h2>
                                                <p style="font-size: 17px;text-align:center; font-weight:bold; color:#001e30">Price of Phone Number per month</p>
                                                </div>
                                            </div><!-- end col -->
                                            <div class="row-lg-6">
                                                <!-- Simple card -->
                                                <div class="card">
                                                    <h2 style=" text-align:center; font-size:35px; font-weight:bold; color:#1A7BFF">$@if(isset($data->sms)) {{$data->sms}}  @endif</h2>
                                                    <p style="font-size: 17px;text-align:center; font-weight:bold; color:#001e30">Rate per Text SMS</p>
                                                </div>
                                            </div><!-- end col -->
                                            <div class="row-lg-6">
                                                <!-- Simple card -->
                                                <div class="card">
                                                    <h2 style="text-align:center; font-size:35px; font-weight:bold; color:#1A7BFF">$@if(isset($data->voice)) {{$data->voice}}  @endif</h2>
                                                    <p style="font-size: 17px;text-align:center; font-weight:bold; color:#001e30">Rate per Voicemail</p>
                                                </div>
                                            </div><!-- end col -->
                                            <div class="row-lg-6">
                                                <!-- Simple card -->
                                                <div class="card">
                                                    <h2 style="text-align:center; font-size:35px; font-weight:bold; color:#1A7BFF">$@if(isset($data->call)) {{$data->call}}  @endif</h2>
                                                    <p style="font-size: 17px; text-align:center; font-weight:bold; color:#001e30">Rate per minute to make Calls</p>
                                                </div>
                                            </div><!-- end col -->
                                        </div>
                                        <!-- end row -->
                                    </div>
                                </div> <!-- end card -->
                            </div> <!-- end col-->

                            <div class="col-lg-8">
                                <div class="card">
                                    <div class="card-body pb-2">
                                    <h4 class="header-title mb-3">Price Management</h4>
                                        <div class="card" style="margin-left:5%">
                                                <div class="card-body">
                                                    <form method="POST" action="{{url('price')}}" class="needs-validation" novalidate >
                                                        @csrf

                                                        <div class="mb-3">
                                                            <label for="phone_number" class="form-labe">{{ __('Price of Phone Number per month') }}</label>
                                                                <input id="phone_number" type="text" class="form-control" name="phone_number" required value="{{(isset($data->phone_number))?$data->phone_number:0}}" placeholder="Enter your amount">
                                                                <div class="invalid-feedback">
                                                                    This amount is required.
                                                                </div>
                                                                @error('phone_number')
                                                                <div style="color: red; font-weight:bold;">{{ $message }}</div>
                                                            @enderror
                                                        </div>

                                                        <div class="mb-3">
                                                            <label for="sms" class="form-labe">{{ __('Rate per Text SMS') }}</label>
                                                                <input id="sms" type="text" class="form-control" required placeholder="Enter your amount" value="{{(isset($data->sms))?$data->sms:0}}" name="sms">
                                                                <div class="invalid-feedback">
                                                                    This amount is required.
                                                                </div>
                                                                @error('sms')
                                                                <div style="color: red; font-weight:bold;">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="mb-3">
                                                            <label for="voice" class="form-labe">{{ __('Rate per Voicemail') }}</label>
                                                                <input id="voice" type="text" class="form-control" required placeholder="Enter your amount" value="{{(isset($data->voice))?$data->voice:0}}" name="voice">
                                                                <div class="invalid-feedback">
                                                                    This amount is required.
                                                                </div>
                                                                @error('voice')
                                                                <div style="color: red; font-weight:bold;">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="mb-3">
                                                            <label for="call" class="form-labe">{{ __('Rate per minute to make Calls') }}</label>
                                                                <input id="call" type="text" class="form-control" required placeholder="Enter your amount" value="{{(isset($data->call))?$data->call:0}}" name="call">
                                                                <div class="invalid-feedback">
                                                                    This amount is required.
                                                                </div>
                                                                @error('call')
                                                                <div style="color: red; font-weight:bold;">{{ $message }}</div>
                                                            @enderror
                                                            </div>
                                                       
                                                            <div class="text-center">
                                                        
                                                                <button type="submit" class="btn btn-success submit">
                                                                    {{ __('Submit') }}
                                                                </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div> <!-- end card -->
                                    </div>
                                </div> <!-- end card -->
                            </div> <!-- end col-->
                        </div>
                        <!-- end row -->
  
</div>


@endsection


