@extends('Admin.master')
@section('content')

<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">IVR</li>
                            <li class="breadcrumb-item active">add</li>
                        </ol>
                    </div>
                     <h4 class="page-title">IVR</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="float-end">
                    <a href="{{route('ivr')}}"> <button type="button" class="btn btn-danger btn-sm" style="font-family: sans-serif;font-size: 13px;"><i class="fe-arrow-left"></i>Back</button></a>

                </div>
            <h4 class="page-title-box">Add IVR</h4>

            <div class="row">
                <div class="col-lg">

                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{route('ivrstore')}}" enctype="multipart/form-data" class="needs-validation" novalidate >
                                @csrf

                                <div class="mb-3">
                                    <label for="title" class="form-labe">{{ __('Title') }}</label>
                                        <input id="title" type="text" class="form-control" name="title" required placeholder="Enter your Title">
                                        <div class="invalid-feedback">
                                            This Title is required.
                                        </div>
                                        @error('title')
                                        <div style="color: red; font-weight:bold;">{{ $message }}</div>
                                    @enderror
                                    </div>
                                <div class="mb-3">
                                    <label for="type" class="form-labe">{{ __('Select Type') }}</label>
                                    <select id="type" name="type" class="form-control" required >
                                        <option value="" disabled selected>Select Type</option>
                                        <option value="say">Say</option>
                                        <option value="play">Play</option>

                                      </select>
                                    <div class="invalid-feedback">
                                           This Type is required.
                                        </div>
                                </div>
                                <div class="mb-3" id="myDiv" style="display: none;">
                                    <label for="title" class="form-labe">{{ __('Say Message') }}</label>
                                    <textarea id="say" name="say_message" class="form-control" rows="5" cols="10" maxlength="200"></textarea>
                                    <a style="color: red;"><span id="messageerror"></span></a>
                                        <div class="invalid-feedback">
                                            This say message is required.
                                        </div>
                                        @error('title')
                                        <div style="color: red; font-weight:bold;">{{ $message }}</div>
                                    @enderror
                                    </div>
                                    <div class="mb-3" id="upload" style="display: none;">
                                    <label for="title" class="form-labe">{{ __('Media Upload') }}</label>
                                    <div class="row">

                                        <div class="col-md-6">
                                            <input type="file" name="medianame" accept=".mp3,audio/*" id="medianame" class="form-control">
                                            <a style="color: red;"><span id="error"></span></a>
                                            @error('medianame')
                                                <div style="color: red;font-weight: bold;">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                        <audio controls id="addIvr2">
                                          <source id="addIvr" type="audio/ogg">
                                        </audio>
                                    </div>
                                    </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="status" class="form-labe">{{ __('Select Status') }}</label>
                                        <select id="type" name="status" class="form-control" required >
                                            <option value="" disabled selected>Select Status</option>
                                            <option value="enabled">Enabled</option>
                                            <option value="disabled">Disabled</option>

                                          </select>
                                        <div class="invalid-feedback">
                                               This Status is required.
                                            </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="ivr_day" class="form-labe">{{ __('Select Day') }}</label>
                                        <select id="ivr_day" name="ivr_day" class="form-control" required >
                                            <option value="" disabled selected>Select Day</option>
                                            <option value="Sunday">Sunday</option>
                                            <option value="Monday">Monday</option>
                                            <option value="Tuesday">Tuesday</option>
                                            <option value="Wednesday">Wednesday</option>
                                            <option value="Thursday">Thursday</option>
                                            <option value="Friday">Friday</option>
                                            <option value="Saturday">Saturday</option>

                                          </select>
                                        <div class="invalid-feedback">
                                               This Day is required.
                                            </div>
                                    </div>
                                <div class="text-center">

                                        <button type="submit" id="submitbtn" class="btn btn-success">
                                            Submit
                                        </button>

                                            </div>
                                        </form>
                        </div>
                    </div> <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
    </div>
</div>

@endsection
@section('style')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.css" id="theme-styles">
@endsection
@section('script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.5.0/dist/sweetalert2.all.min.js"></script>
<script type=text/javascript>
    $('#type').change(function(e){

        e.preventDefault();
        var data = $(this).children("option:selected").val();

       if (data == "say") {

        $('#myDiv').show();
        $('#upload').hide();



       }else if(data == "play")
       {
        $('#myDiv').hide();
        $('#upload').show();


       }


    });

    $(document).ready(function(){

        $(document).on('keydown','#medianame',function(){
            $('#error').html('');
        });     
        $(document).on('keydown','#say',function(){
            $('#messageerror').html('');
        }); 

        $("#submitbtn").click(function(){
            var type = $("#type").val();
            if (type == 'play') {

                if( document.getElementById("medianame").files.length == 0 ){
                        $("#error").html('Please select file');
                        return false;
                }

            }
            if (type == 'say') {

                 if($("#say").val().length == 0)
                 {
                    $("#messageerror").html('This say message is required!');
                    return false;
                 }
            }
        });
        function handleFiles(event) {

             if(this.files[0].size > 2000000) {
               
                Swal.fire({
                    title: 'Error!',
                    text: 'Please upload file less than 2MB. Thanks!!',
                    icon: 'error',
                    confirmButtonText: 'Cool'
                });
                return false;
               
             }else{
                var files = event.target.files;
                $("#addIvr").attr("src", URL.createObjectURL(files[0]));
                document.getElementById("addIvr2").load();
             }

            
        }
        document.getElementById("medianame").addEventListener("change", handleFiles, false);


    });
  </script>
@endsection