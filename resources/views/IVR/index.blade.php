@extends('Admin.master')
@section('content')


<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">ivr</li>
                        </ol>
                    </div>
                     <h4 class="page-title">IVR</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
           @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <div class="card">
            <div class="card-body">
                <div class="float-end">
                    <div class="d-flex justify-content-end">
                        <div>   
                            <select id="select_day" name="ivr_day" class="form-control form-control-sm" required >
                               <option value="" disabled >Select Day</option>
                               <option value="Sunday" selected>Sunday</option>
                               <option value="Monday">Monday</option>
                               <option value="Tuesday">Tuesday</option>
                               <option value="Wednesday">Wednesday</option>
                               <option value="Thursday">Thursday</option>
                               <option value="Friday">Friday</option>
                               <option value="Saturday">Saturday</option>
                            </select>
                        </div>
                        &nbsp;&nbsp;
                        <a href="{{ route('addIvr') }}" ><button class="btn btn-success btn-sm">Add IVR</button></a>
                    </div>
                   
                </div>
            <h4 class="page-title-box">IVR</h4>
            <div class="row">
                <div class="col-12">
                    <div class="card" style="height: auto;">
                        <div class="card-body">
                            <div class="row">
                            <table id="ivrs" class="table display dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Type</th>
                                        <th>Message</th>
                                        <th>Status</th>
                                        <th>Day</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>

                            </table>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
                </div>
                <!-- end row -->
            </div>
        </div> <!-- end card -->
     </div>
</div>
<!-- end row -->
@endsection


@section('script')
<script type="text/javascript">
var tableData = false;
function getData(){
        if(tableData){
           tableData.destroy(); 
        }

        tableData = $('#ivrs').DataTable({
                responsive: true,        
                processing: true,
                serverSide: false,
                dataType: "json",
                pageLength: 10,
                type: "get",
                ajax: {
                url: "/ivr_list",
                dataType: "json",
                type: "get",
                data: {
                receipt_no: function(e) {

                        return $("#select_day").val();
                    },      
                },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: true
                    },
                    {
                       data: 'title',
                    },
                    {
                       data: 'type',
                    },
                    {
                       data: 'message',
                    },
                    {
                       data: 'status',
                    },
                    {
                       data: 'ivr_day' 
                    },
                    {
                        data: 'action',
                        orderable: false,
                        searchable: false,
                    },
                ]
            });
}
$(function () {
            getData();
            

                $('#select_day').change(function(){
                    getData();
                });

        $(document).on('click','.servideletebtn' , function(e){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id  = $(this).attr('data-id');
          
            e.preventDefault();
            // alert(type);
            Swal.fire({
                title: 'Are you sure?',
                text: 'You want to delete the IVR',
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: `Yes`,
                denyButtonText: `No`,
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {

                    var data = {

                        "_token": "{{ csrf_token() }}",
                        "id": id,
                    };

                    $.ajax({

                        type: "DELETE",
                        url: '/Ivr/delete/'+ id,
                        data: data,
                        success: function(result){
                            if(result.success){
                                Swal.fire({
                                    icon:"success",
                                    title:"Your ivr successfully deleted",
                                }).then((result) => {
                                          if (result.isConfirmed) {
                                           tableData.ajax.reload();
                                          } 
                              });
                              
                            }
                        //    $('#users').data.reload();
                        //    table.reload();
                        }

                    })


                } else if (result.isDenied) {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        });

    setTimeout(function () {
        $('.alert').hide();
    }, 20000);        

    // $(document).on('change','#select_day',function(){
        
    //     table.draw();
    // });

});
</script>
@endsection






