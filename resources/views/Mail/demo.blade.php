
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-4">
                        <div class="card bg-pattern">

                    <table bgcolor="#ffffff" width="600" class="m_4816109280497721507content_table m_4816109280497721507devicewidth" cellpadding="0" cellspacing="0" border="0" align="center" style="box-sizing:border-box;background-repeat:no-repeat;border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;font-weight:400;border-collapse:separate;border-radius:10px;border:1px solid #e5e5e5">
        <tbody><tr style="box-sizing:border-box;background-repeat:no-repeat;padding:0;vertical-align:top;text-align:left;font-weight:400">
          <td style="box-sizing:border-box;background-repeat:no-repeat;word-break:normal;padding:0;vertical-align:top;text-align:left;font-weight:400;line-height:160%;border-collapse:collapse!important">

            <table bgcolor="#ffffff" width="500" cellpadding="0" cellspacing="0" border="0" align="center" class="m_4816109280497721507devicewidthinner" style="box-sizing:border-box;background-repeat:no-repeat;border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;font-weight:400">
              <tbody><tr style="box-sizing:border-box;background-repeat:no-repeat;padding:0;vertical-align:top;text-align:left;font-weight:400">
                <td width="100%" align="center" class="m_4816109280497721507content" style="box-sizing:border-box;background-repeat:no-repeat;word-break:normal;padding:0;vertical-align:top;text-align:left;font-weight:400;line-height:160%;border-collapse:collapse!important;padding-top:40px">
                  <p style="box-sizing:border-box;background-repeat:no-repeat;font-family:&quot;Avenir&quot;,&quot;Avenir Next&quot;,&quot;Segoe UI&quot;,Helvetica,Arial,sans-serif;color:#3d474d;font-size:17px;line-height:160%;margin:0 0 25px 0;margin-bottom:40px">
                    <img alt="MeisterTask" style="box-sizing:border-box;background-repeat:no-repeat;outline:none;text-decoration:none;display:inline-block;width:auto" height="36" src="{{asset('images/login-logo.png')}}" class="CToWUd">
                  </p>
                </td>
              </tr>
            </tbody></table>

              <table bgcolor="#F7F9FA" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="m_4816109280497721507devicewidth" style="box-sizing:border-box;background-repeat:no-repeat;border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;font-weight:400;margin-bottom:40px">
  <tbody>
</tbody></table>


            <table bgcolor="#ffffff" width="500" cellpadding="0" cellspacing="0" border="0" align="center" class="m_4816109280497721507devicewidthinner" style="box-sizing:border-box;background-repeat:no-repeat;border-spacing:0;border-collapse:collapse;padding:0;vertical-align:top;text-align:left;font-weight:400">
              <tbody><tr style="box-sizing:border-box;background-repeat:no-repeat;padding:0;vertical-align:top;text-align:left;font-weight:400">
                <td width="100%" align="center" class="m_4816109280497721507content" style="box-sizing:border-box;background-repeat:no-repeat;word-break:normal;padding:0;vertical-align:top;text-align:left;font-weight:400;line-height:160%;border-collapse:collapse!important;padding-bottom:40px">
                <p style="box-sizing:border-box;background-repeat:no-repeat;font-family:&quot;Avenir&quot;,&quot;Avenir Next&quot;,&quot;Segoe UI&quot;,Helvetica,Arial,sans-serif;color:#3d474d;font-size:17px;line-height:160%;margin:0 0 25px 0;margin-bottom:20px">
                  Hello <i>{{ $demo->receiver }}</i>,
                </p>

                <p style="box-sizing:border-box;background-repeat:no-repeat;font-family:&quot;Avenir&quot;,&quot;Avenir Next&quot;,&quot;Segoe UI&quot;,Helvetica,Arial,sans-serif;color:#3d474d;font-size:17px;line-height:160%;margin:0 0 25px 0">
                  You are receiving an invite from mailcall communication to procced the signup by purchasing the Inmates Number to get started you can purchase the number from  button below.
                </p>
                <p style="box-sizing:border-box;background-repeat:no-repeat;font-family:&quot;Avenir&quot;,&quot;Avenir Next&quot;,&quot;Segoe UI&quot;,Helvetica,Arial,sans-serif;color:#3d474d;font-size:17px;line-height:160%;margin:0 0 25px 0;margin-bottom:40px">
                  <a class="m_4816109280497721507button m_4816109280497721507default m_4816109280497721507floating" href="{{$demo->link}}" style="box-sizing:border-box;background-repeat:no-repeat;font-family:&quot;Avenir&quot;,&quot;Avenir Next&quot;,&quot;Segoe UI&quot;,Helvetica,Arial,sans-serif;font-weight:500;color:#00aaff;border:1px solid #00aaff;border-radius:7px;font-size:15px;line-height:36px;height:36px;display:inline-block;white-space:nowrap;overflow:visible;text-align:center;text-decoration:none;border-radius:10px;line-height:48px;height:48px;padding:0 30px;border-color:#00aaff;color:white;background-color:#00aaff;font-size:17px;border:10px solid #00aaff;line-height:28px;padding:0 20px">Register</a>
                </p>
                <p style="box-sizing:border-box;background-repeat:no-repeat;font-family:&quot;Avenir&quot;,&quot;Avenir Next&quot;,&quot;Segoe UI&quot;,Helvetica,Arial,sans-serif;color:#3d474d;font-size:17px;line-height:160%;margin:0 0 25px 0;margin-top:40px;margin-bottom:40px;text-align:center">
                  <div id=":h4" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Download" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V"><div class="akn"><div class="aSK J-J5-Ji aYr"></div></div></div></div>
                </p>
                </td>
              </tr>
            </tbody></table>

          </td>
        </tr>
      </tbody></table>
</div>
</div>
</div>
</div>
        
      