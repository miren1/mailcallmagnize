   @extends('Front.master')
   @section('content')
   <!-- home start -->
   <section class="bg-home bg-gradient" id="home">
    <div class="home-center">
        <div class="home-desc-center">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-9">
                        <p style="color: white;font-weight: bold; font-size: 39px; font-family: 'Nunito', sans-serif;">Communicate with your loved ones in the prison, Hassle Free!</p>
                        <p style="color:white;font-size: 21px; font-family: 'Nunito', sans-serif;">Our goal is to build a strong connection with your loved ones in your confinement.</p>
                        <p style="color:white;font-size: 21px; font-family: 'Nunito', sans-serif;">Sign up now and get a private phone number to contact your loved ones in prison.</p>
                    </div>
                    <div class="col-xl-4 offset-xl-2 col-lg-5 offset-lg-1 col-md-7">
                </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container-fluid -->
        </div>
    </div>
</section>
<!-- home end -->
<!-- features start -->
<section class="section-sm" id="features">
    <div class="container-fluid">


        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3 style="color:black;font-size: calc(1.275rem + .6vw);font-family: 'Nunito', sans-serif; font-weight: bold;">At mail call communication, we offer more than simple voice mail. Our goal is to provide you quality calls and text messages by following easy steps and at cheap prices.</h3>&nbsp;
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="features-box">
                    <h3 style="color:black;font-family: 'Nunito', sans-serif;font-weight: bold;">Most discounted Calls</h3>      
                     <div class="mb-4">              
                    <p style="color:black; text-align: left;font-family: 'Nunito', sans-serif;"> At mail call communication, we offer you the cheapest call. You can get your private number and monthly subscription for services at very low prices. By keeping a positive balance in your account, you can connect with your loved ones.</p>
                    </div>
                </div>
            </div>
         
            <div class="col-md-6">
                <div class="features-box">
                    <h3 style="color:black;font-family: 'Nunito', sans-serif;font-weight: bold;">You can leave message as a voicemail</h3>
                    <div class="mb-4">
                        <p style="color:black; text-align: left;font-family: 'Nunito', sans-serif;">We understand how bad it seems when you have no option other than waiting for your inmate to call you. You can utilize this amazing feature of mail call communication. You can leave a voicemail to your loved ones, and they can get back to you as soon as they receive the voicemail. This is a 24-hour service so you can leave your message at any time.</p>
                    </div>
                    
                </div>
            </div>
            <!-- end col -->
            <div class="col-md-6">
                <div class="features-box">
                    <h3 style="color:black;font-family: 'Nunito', sans-serif;font-weight: bold;">Text messages</h3>
                    <div class="mb-4">
                        <p style="color:black; text-align: left;">Messages are the common and most comfortable way of communication. Your inmate can text you anytime and you can get notification directly on your smartphone. You can reply or call them whenever they leave you a message. We are trying to make this duration easy for you. That's why we encourage you to take advantage of these services.</p>
                    </div>
                   
                </div>
            </div>
         

            <div class="col-md-6">
                <div class="features-box">
                    <h3 style="color:black;font-family: 'Nunito', sans-serif;font-weight: bold;">Connecting with long distance loved ones</h3>
                    <div class="features-img mb-4">
                        <P style="color:black; text-align: left; font-family: Nunito,sans-serif;">Don't let distance restrict you from communicating to your loved ones. With our long-distance call feature, intimate can make local calls and in the entire country also. Not only calls, but you can also send voicemails, text messages at anytime from anywhere.</P>
                    </div>
                </div>
            </div>
       
            <div class="col-md-6">
                <div class="features-box">
                    <h3 style="color:black;font-family: 'Nunito', sans-serif; font-weight: bold;">Pay for your loved ones</h3>
                    <div class="features-img mb-4">
                        <p style="color:black; text-align: left; font-family: 'Nunito', sans-serif;">At mail call communication we appreciate it if you can help your inmate with fundings. There is a feature called crowd source funding that allows family, friends and others to pay for the inmate. In this sense you can make sure that your inmate is never running out of balance.</p>
                    </div>
                </div>
            </div>
           <div class="col-md-6">
                <div class="features-box">
                    <h3 style="color:black;">FREE 30-Day trial</h3>
                    <div class="features-img mb-4">
                        <p style="color:black; text-align: left; font-family: 'Nunito', sans-serif;">Doesn't it sound amazing? Yes! You can get a 30-day FREE trial also. Our intention is to serve you in the best possible way. With this Free trial, you can build trust and you can utilize our services. We are hopeful you will go for a subscription for sure.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="features-box">
                    <h3 style="color:black;">No extra transaction fee</h3>
                    <div class="features-img mb-4">
                        <p style="color:black; text-align: left;font-family: 'Nunito', sans-serif;">We don't charge any extra transaction fee as we understand your condition. Your entire top-off will be added to your inmate account without any surcharges or transaction fee.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="features-box">
                   <h3 style="color:black;">Services we offer</h3>
                    <div class="features-img mb-4">
                        <p style="color:black; text-align: left;font-family: 'Nunito', sans-serif;">Following are the un-exceptional services we offer at the mail call communication.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="features-box">
                    <h3 style="color:black;">Private number</h3>
                    <div class="features-img mb-4">
                        <p style="color:black; text-align: left;font-family: 'Nunito', sans-serif;">With mail call communication, your inmate can get a personal and private number in the prison.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="features-box">
                    <h3 style="color:black;">Voicemails</h3>
                    <div class="features-img mb-4">
                        <p style="color:black; text-align: left;font-family: 'Nunito', sans-serif;">You can voicemail your inmate and you can leave a message for them also. You can be notified on your smartphone if they leave any message. So, it's a 24/7 communication service.</p>
                    </div>
                </div>
            </div>
             <div class="col-md-6">
                <div class="features-box">
                    <h3 style="color:black;">Text Messages</h3>
                    <div class="features-img mb-4">
                        <p style="color:black; text-align: left;font-family: 'Nunito', sans-serif;">Your inmate can leave a text message for you. You can reply to them through text messages, or you can call them.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="features-box">
                    <h3 style="color:black;">Direct Call</h3>
                    <div class="features-img mb-4">
                        <p style="color:black; text-align: left;font-family: 'Nunito', sans-serif;">You can call directly to your inmate. Our call service is quite cheap. Our goal is to vanish the restrictions so you can call your inmate even though you are at a long distance from them.</p>
                    </div>
                </div>
            </div>
        </div>
    
    </div> 
</section>
<!-- end features -->
<!-- pricing start -->
<section class="section pb-0 bg-gradient" id="pricing">
    <div class="bg-shape">
        <img src="{{asset('images/bg-shape.png')}}" alt="" class="img-fluid mx-auto d-block">
    </div>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="text-center mb-4">
                    <h3 class="text-white" style="font-family: 'Nunito', sans-serif;">Our Pricing </h3>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row justify-content-center">
            <div class="col-xl-12">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <div class="pricing-plan bg-white p-4 mt-4" style="background-color: ">
                            <div class="pricing-header text-center">
                                <h5 class="plan-title text-uppercase mb-4">Price</h5>
                                <h1><sup><small>$</small></sup>@if(isset($data->phone_number)) {{$data->phone_number}}  @endif</h1>
                                <div class="plan-duration text-muted">Phone Number per month</div>
                            </div>
                        </div>
                    </div> <!-- end col --> 
                     <div class="col-lg-3">
                        <div class="pricing-plan bg-white p-4 mt-4">
                            <div class="pricing-header text-center">
                                <h5 class="plan-title text-uppercase mb-4">Rate</h5>
                                <h1><sup><small>$</small></sup>@if(isset($data->sms)) {{$data->sms}}  @endif</h1>
                                <div class="plan-duration text-muted">per Text SMS</div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-3">
                        <div class="pricing-plan bg-white p-4 mt-4">
                            <div class="pricing-header text-center">
                                <h5 class="plan-title text-uppercase mb-4">Rate</h5>
                                <h1><sup><small>$</small></sup>@if(isset($data->voice)) {{$data->voice}}  @endif</h1>
                                <div class="plan-duration text-muted">Rate per Voicemail</div>
                            </div>
                        </div>
                    </div> 
                     <div class="col-lg-3">
                        <div class="pricing-plan bg-white p-4 mt-4">
                            <div class="pricing-header text-center">
                                <h5 class="plan-title text-uppercase mb-4">Rate</h5>
                                <h1><sup><small>$</small></sup>@if(isset($data->call)) {{$data->call}}  @endif</h1>
                                <div class="plan-duration text-muted">per minute to make Calls</div>
                            </div>
                        </div>
                    </div> 
                </div>
                <!-- end row -->
            </div> <!-- end col-xl-10 -->
            &nbsp;

            <div style="text-align: center; margin-top: 25px;">

       <a href="{{route('refillaccount')}}"><button type="button" class="btn btn-primary" style="font-family: Nunito,sans-serif;">Refill Account</button></a>
            </div>
        </div>
        <!-- end row -->
    </div>
    <!-- end container-fluid -->
</section>
<!-- pricing end -->

<!-- faqs start -->
<section class="section" id="faq">
    <div class="container-fluid">

        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="text-center mb-5">
                    <h3 style="color:black;font-size: calc(1.275rem + .6vw);font-family: 'Nunito', sans-serif; font-weight: bold;">Frequently Asked Questions</h3>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-lg-5 offset-lg-1">
                <!-- Question/Answer -->
                <div>
                    <div class="faq-question-q-box">Q.1</div>
                    <h4 class="faq-question"style="color:black; font-size: 1.5rem; font-family: 'Nunito', sans-serif;font-weight: bold;">How does mail call communication work?</h4>
                    <p class="faq-answer mb-4 pb-1" style="font-size: 15px; color: black; font-family: Nunito,sans-serif;">It's simple. You just have to make an account and we will assign you a private number. Once you pay the service charges, then you will get access to our services.</p>
                </div>
                <!-- Question/Answer -->
                <div>
                    <div class="faq-question-q-box">Q.3</div>
                    <h4 class="faq-question" style="color: black; font-size: 1.5rem; font-family: 'Nunito',sans-serif; font-weight: bold;">How can I play?</h4>
                    <p class="faq-answer mb-4 pb-1" style="font-size: 15px;padding-top: 8px; color: black; font-family: Nunito,sans-serif;">You can pay directly from your card. We also offer crowd source funding in which your family and friends can pay for you so you will never run out of balance.</p>
                </div>
                 <!-- Question/Answer -->
                <div>

                    <div class="faq-question-q-box">Q.5</div>
                    <h4 class="faq-question" style="color:black; font-size: 1.5rem; font-family: 'Nunito', sans-serif;font-weight: bold;">Can I get a refund?</h4>
                    <p class="faq-answer mb-4 pb-1" style="font-size: 15px;padding-top: 8px; color: black; font-family: Nunito,sans-serif;">NO, we don't have any refund policy. But we are sure that you will like our services and you won't require any refund.</p>
                </div>
                <div>

                    <div class="faq-question-q-box">Q.7</div>
                    <h4 class="faq-question" style="color:black; font-size: 1.5rem; font-family: 'Nunito', sans-serif;font-weight: bold;">Can I get mail call communication in all prisons?</h4>
                    <p class="faq-answer mb-4 pb-1" style="font-size: 15px;padding-top: 8px; color: black; font-family: Nunito,sans-serif;">Yes, we are available in all country prisons, so you don't have to worry about location.</p>
                </div>
                <div>
                    <div class="faq-question-q-box">Q.9</div>
                    <h4 class="faq-question" style="color:black; font-size: 1.5rem; font-family: 'Nunito', sans-serif;font-weight: bold;">Does mail call communication take care of my privacy?</h4>
                    <p class="faq-answer mb-4 pb-1" style="font-size: 15px;padding-top: 8px; color: black; font-family: Nunito,sans-serif;">Yes. all the messages you send to your loved ones are private, but we encourage you to make sure that you are not sharing your personal information (e.g., your phone number and password) with anyone.</p>
                </div>
                <div>
                    <div class="faq-question-q-box">Q.11</div>
                    <h4 class="faq-question" style="color:black; font-size: 1.5rem; font-family: 'Nunito', sans-serif;font-weight: bold;">How can I refill my account?</h4>
                    <p class="faq-answer mb-4 pb-1" style="font-size: 15px;padding-top: 8px; color: black; font-family: Nunito,sans-serif;">You can refill your account by clicking the refill button below pricing. your loved ones can also refill your account. Please keep the positive balance of $10 in your account so you don't have to worry about charges. Once your account is not filled, you will lose your number and you have to start all over again.</p>
                </div>
            </div>
            <!--/col-lg-5 -->

            <div class="col-lg-6">
                <!-- Question/Answer -->
                 <div>
                    <div class="faq-question-q-box">Q.2</div>
                    <h4 class="faq-question" style="color:black; font-size: 1.5rem; font-family: 'Nunito', sans-serif;font-weight: bold;">How to make an account?</h4>
                    <p class="faq-answer mb-4 pb-1" style="font-size: 15px; color: black; font-family: Nunito,sans-serif;">Please check out the first part of this guide. We have explained the step by step process of making an account.</p>
                </div>
                <!-- Question/Answer -->
                <br><br>

                <div>
                    <div class="faq-question-q-box">Q.4</div>
                    <h4 class="faq-question" style="color:black; font-size: 1.5rem; font-family: 'Nunito', sans-serif;font-weight: bold;">How much do I have to pay?</h4>
                    <p class="faq-answer mb-4 pb-1" style="font-size: 15px;margin-top: 14px; color: black; font-family: Nunito,sans-serif;"><a>• It's quite cheap. You just have to pay $6 for getting your monthly subscription for your private phone number.</a><br><a>• Service charges for one month are $4.99.</a><br><a>• Kindly have a balance of $10 in your account as it will help you in case of low balance. In case of not paying charges, you will lose your number and you have to start all over again.</a><br><a>• Per text charges are $0.095.</a><br><a>• Per voicemail charges are $0.02.</a><br><a>• Per voicemail charges are $0.02.</a><br><a>• Per minute call charges are $0.02.</a></p>
                </div>
                <!-- Question/Answer -->
                <div>
                    <div class="faq-question-q-box">Q.6</div>
                    <h4 class="faq-question" style="color:black; font-size: 1.5rem; font-family: 'Nunito', sans-serif;font-weight: bold;">Can I get a free trial?</h4>
                    <p class="faq-answer mb-4 pb-1"  style="font-size: 15px;color: black; font-family: Nunito,sans-serif;">Yes, absolutely. We understand your condition and we want The Best for you. At mail call communication, you can get a 30-days free trial.</p>
                </div>
                <br>
                 <div>
                    <div class="faq-question-q-box">Q.8</div>
                    <h4 class="faq-question" style="color:black; font-size: 1.5rem; font-family: 'Nunito', sans-serif;font-weight: bold;">Can I make long distance calls?</h4>
                    <p class="faq-answer mb-4 pb-1"  style="font-size: 15px;color: black; font-family: Nunito,sans-serif;">yes. Of course. With mail call communication, you are not restricted to local calls only. You can call, message, voicemail to anyone anywhere in the entire country.</p>
                </div>
                <div>
                    <div class="faq-question-q-box">Q.10</div>
                    <h4 class="faq-question" style="color:black; font-size: 1.5rem; font-family: 'Nunito', sans-serif;font-weight: bold;">Can I leave the message if the receiver is not available?</h4>
                    <p class="faq-answer mb-4 pb-1"  style="font-size: 15px;color: black; font-family: Nunito,sans-serif;">Yes. you can leave a text message and a voicemail if the receiver is not available. They can get back to you whenever they are available.</p>
                </div>
                <div>
                    <div class="faq-question-q-box">Q.12</div>
                    <h4 class="faq-question" style="color:black; font-size: 1.5rem; font-family: 'Nunito', sans-serif;font-weight: bold;">Can I cancel my subscription?</h4>
                    <p class="faq-answer mb-4 pb-1"  style="font-size: 15px;color: black; font-family: Nunito,sans-serif;">Yes, anytime. We feel bad that you are leaving but you can contact us in case you want to cancel your subscription. We will get back to you as soon as possible.</p>
                </div>
            </div>
            <!--/col-lg-5-->
        </div>
        <!-- end row -->

    </div> <!-- end container-fluid -->
</section>
<!-- faqs end -->

<!-- testimonial start -->
<section class="section bg-light" id="feedback">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="text-center mb-4">
                    <h3 style="color:black;font-size: calc(1.275rem + .6vw);font-family: 'Nunito', sans-serif; font-weight: bold;">What Our Users Says</h3>
                    <p class="text-muted">The clean and well commented code allows easy customization of the theme.It's designed for describing your app, agency or business.</p>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-lg-4">
                <div class="testi-box mt-4">
                    <div class="testi-desc bg-white p-4">
                        <p class="text-muted mb-0">" Excellent support for a tricky issue related to our customization of the template. Author kept us updated as he made progress on the issue and emailed us a patch when he was done. "</p>
                    </div>
                    <div class="p-4">
                        <div class="testi-img float-start me-2">
                            <img src="{{asset('images/testi/img-2.png')}}" alt="" class="rounded-circle">
                        </div>
                        <div>
                            <h5 class="mb-0">Michael Morrell</h5>
                            <p class="text-muted m-0"><small>- Mailcall User</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end col -->
            <div class="col-lg-4">
                <div class="testi-box mt-4">
                    <div class="testi-desc bg-white p-4">
                        <p class="text-muted mb-0">" Flexible, Everything is in, Suuuuuper light, even for the code is much easier to cut and make it a theme for a productive app. "</p>
                    </div>
                    <div class="p-4">
                        <div class="testi-img float-start me-2">
                            <img src="{{asset('images/testi/img-1.png')}}" alt="" class="rounded-circle">
                        </div>
                        <div>
                            <h5 class="mb-0">John Seidel</h5>
                            <p class="text-muted m-0"><small>- Mailcall User</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end col -->
            <div class="col-lg-4">
                <div class="testi-box mt-4">
                    <div class="testi-desc bg-white p-4">
                        <p class="text-muted mb-0">" Not only the code, design and support are awesome, but they also update it constantly the template with new content, new plugins. I will buy surely another coderthemes template! "</p>
                    </div>
                    <div class="p-4">
                        <div class="testi-img float-start me-2">
                            <img src="{{asset('images/testi/img-3.png')}}" alt="" class="rounded-circle">
                        </div>
                        <div>
                            <h5 class="mb-0">Robert Taylor</h5>
                            <p class="text-muted m-0"><small>- Maillcall User</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end col -->
        </div> 
        <!-- end row -->
    </div> 
    <!-- end container-fluid -->
</section>
<!-- testimonial end -->

<!-- contact start -->
<section class="section pb-0 bg-gradient" id="contact">
    <div class="bg-shape">
        <img src="{{asset('images/bg-shape-light.png')}}" alt="" class="img-fluid mx-auto d-block">
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="title text-center mb-4">
                    <h3 class="text-white">Have any Questions ?</h3>
                    <p class="text-white-50">Please fill out the following form and we will get back to you shortly</p>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row mb-4">
            <div class="col-md-4">
                <div class="contact-content text-center mt-4">
                    <div class="contact-icon mb-2">
                        <i class="mdi mdi-email-outline text-info h2"></i>
                    </div>
                    <div class="contact-details text-white">
                        <h6 class="text-white">E-mail</h6>
                        <p class="text-white-50">example@abc.com</p>
                    </div>
                </div>
            </div>
            <!-- end col -->
            <div class="col-md-4">
                <div class="contact-content text-center mt-4">
                    <div class="contact-icon mb-2">
                        <i class="mdi mdi-cellphone-iphone text-info h2"></i>
                    </div>
                    <div class="contact-details">
                        <h6 class="text-white">Phone</h6>
                        <p class="text-white-50">012-345-6789</p>
                    </div>
                </div>
            </div>
            <!-- end col -->
            <div class="col-md-4">
                <div class="contact-content text-center mt-4">
                    <div class="contact-icon mb-2">
                        <i class="mdi mdi-map-marker text-info h2"></i>
                    </div>
                    <div class="contact-details">
                        <h6 class="text-white">Address</h6>
                        <p class="text-white-50">4413 Redbud Drive, New York</p>
                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->

        <div class="row justify-content-center">
            <div class="col-lg-10">

                <div class="custom-form p-5 bg-white">
                    <span id="error-msg"></span>
                    <form method="post" name="myForm" onsubmit="return validateForm()">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="name" class="form-label">Name</label>
                                    <input name="name" id="name" type="text" class="form-control" placeholder="Enter your name...">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email address</label>
                                    <input name="email" id="email" type="email" class="form-control" placeholder="Enter your email...">
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="mb-3">
                                    <label for="subject" class="form-label">Subject</label>
                                    <input name="subject" id="subject" type="text" class="form-control" placeholder="Enter Subject...">
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="mb-3">
                                    <label for="comments" class="form-label">Message</label>
                                    <textarea name="comments" id="comments" rows="4" class="form-control" placeholder="Enter your message..."></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-lg-12 text-end">
                                <input type="submit" id="submit" name="send" class="submitBnt btn btn-danger" value="Send Message">
                            </div>
                        </div>
                        <!-- end row -->
                    </form>
                </div>
                <!-- end custom-form -->
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container-fluid -->
</section>
<!-- contact end -->

@endsection 