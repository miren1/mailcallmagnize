
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}">

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">

        <!--Material Icon -->
        <link rel="stylesheet" type="text/css" href="{{asset('css/materialdesignicons.min.css')}}" />

        <!-- Custom  sCss -->
        <link rel="stylesheet" type="text/css" href="{{asset('css/style.min.css')}}" />