@extends('Admin.master')
@section('content')

<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Voicemail & Message</li>
                        </ol>
                    </div>
                     <h4 class="page-title">VoiceMail & Message</h4>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="float-end">
                </div>
            <h4 class="page-title-box">VoiceMail & Message</h4>
           
            <div class="row">
 	<div class="card">
        <div class="card-body">
            <div class="row">
				<div class="col-12">
				 	<table id="voicemail" class="table dt-responsive nowrap w-100" style="width:100%">
				        <thead>
				            <tr>
				                <th>No</th>
				                <th>Twilio Number</th>
				                <th>Phone Number</th>
				                <th>Type</th>
				                <th>Message</th>
				                <th>Date</th>
				            </tr>
				        </thead>
				    </table>
				</div> <!-- end card body-->
			</div> <!-- end card -->
		</div><!-- end col-->
	</div>
    </div>
   </div>   
</div><!-- end row -->

            </div>
        </div> <!-- end card -->
</div>
<!-- end row -->
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){

             var table = $('#voicemail').DataTable(
                {
            processing: true,
            searching: false,
            responsive: true,
            serverSide: true,
            dataType: "json",
            pageLength: 10,
            type: "get",
            ajax: {
            url: "/voicemail_list",
            dataType: "json",
            type: "get",
            data: {
            receipt_no: function() {
                return $("#receipt_no").val();
            },                },
            },
            columns: [{
            data: 'DT_RowIndex',
            orderable: false,
            searchable: true
            },
            {
            	data: 'twilio_number',
            },
            {
            	data: 'phone_number',
            },
            {
            	data: 'type',
            },
            {
            data: 'message',
            },
            {
            data: 'created_at',	
            },
            ]
            }
        );

});	
</script>
@endsection




