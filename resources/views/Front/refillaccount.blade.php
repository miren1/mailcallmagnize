<!DOCTYPE html>
<html lang="en" style="background-color:#1A7BFF;">
    <head>
        <meta charset="utf-8" />
        <title>Rifill Account</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
        <link href="{{asset('libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- App css -->
        <link href="{{asset('css/config/creative/bootstrap.min.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
        <link href="{{asset('css/config/creative/app1.min.css')}}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />
        <link href="{{asset('css/config/creative/bootstrap-dark.min.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
        <link href="{{asset('css/config/creative/app-dark.min.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />
        <!-- icons -->
        <link href="{{asset('css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('css/step.css')}}" rel="stylesheet" type="text/css" />
    </head>
    <body class="loading authentication-bg authentication-bg-pattern" style="background-color:#1A7BFF;">
        <div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 ">
                    <div class="card bg-pattern">
                        <div class="card-body p-6">
                            <div class="text-center w-75 m-auto">
                                <div class="auth-logo">
                                    <a href="{{route('home')}}" class="logo logo-dark text-center">
                                    <span class="logo-lg">
                                    <img src="{{asset('images/login-logo.png')}}" alt="" height="34">
                                    </span>
                                    </a>
                                </div>
                                &nbsp;&nbsp;
                            </div>
                            <div class="row justify-content-center">
                                                    <form id="example-form" action="#" class="tab-wizard wizard-circle wizard clearfix wizard-validation">
                                                        <h6>Step1</h6>
                                                        <section>
                                                            <br/>
                                                            <div class="row">
                                                                <h4 style="color: black; font-size: 21px;font-family:'Nunito',sans-serif;">Need to refill? Get started by entering your inmate's phone number.</h4>
                                                                <div class="form-group">
                                                                    <label class="col-xs-4 form-label" for="phonenumber">Inmate's Phone Number</label>
                                                                    <div class="col-xs-4">
                                                                        <input type="number" min="0" class="form-control required" placeholder="Enter Inmate's Phone Number" id="inmatesphonenumber" name="inmatesphonenumber" required>
                                                                        <div class="alert-message text-danger" id="mobileNumberError"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex justify-content-end">
                                                                    <button type="button" class="btn btn-primary float-right mt-4 b-0 inmate_number_check">Next</button>
                                                                </div>
                                                            </div>               
                                                        </section>
                                                        <h6>Step2</h6>
                                                        <section>
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <h4 style="color: black; font-size: 20px; font-family:'Nunito',sans-serif;">How much money would you like to add to the balance?</h4>
                                                                    <select name="amount_added" id="amount_added" class="form-control" required>
                                                                        <option value="1">$1</option>
                                                                        <option value="5">$5</option>
                                                                        <option value="10">$10</option>
                                                                        <option value="15">$15</option>
                                                                        <option value="20">$20</option>
                                                                        <option value="25">$25</option>
                                                                        <option value="30">$30</option>
                                                                        <option value="35">$35</option>
                                                                        <option value="40">$40</option>
                                                                        <option value="45">$45</option>
                                                                        <option value="50">$50</option>
                                                                        <option value="60">$60</option>
                                                                        <option value="70">$70</option>
                                                                        <option value="80">$80</option>
                                                                        <option value="90">$90</option>
                                                                        <option value="100">$100</option>
                                                                        <option value="110">$110</option>
                                                                        <option value="120">$120</option>
                                                                        <option value="130">$130</option>
                                                                        <option value="140">$140</option>
                                                                        <option value="150">$150</option>
                                                                        <option value="160">$160</option>
                                                                        <option value="170">$170</option>
                                                                        <option value="180">$180</option>
                                                                        <option value="190">$190</option>
                                                                        <option value="200">$200</option>
                                                                    </select>
                                                                </div>
                                                                <div class="d-flex justify-content-between">
                                                                    <button type="button" class="btn btn-primary float-right mt-4 b-0 inmate_number_check_prev">Prev</button>

                                                                    <button type="button" class="btn btn-primary float-right mt-4 b-0 inmate_number_check_next">Next</button>
                                                                </div>
                                                            </div>     
                                                        </section>
                                                        <h6>Step3</h6>
                                                        <section>
                                                            <div class="row">
                                                                <h4 style="color: black; font-family:'Nunito',sans-serif; font-size: 20px;">Now we need your billing information. <a style="color:red;">$<span id="amount"></span></a> will be billed to your account immediately.</h4>
                                                                <div class="row mb-3">
                                                                    <label class="col-xs-4 form-label" for="name">Your Name</label>
                                                                    <div class="col-xs-3">
                                                                        <input type="text" id="inmatesname" placeholder="Enter your name" name="name" value="" class="form-control" required>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label class="col-xs-4 form-label" for="number">Your Phone Number</label>
                                                                    <div class="col-xs-3">
                                                                        <input type="text" id="number" placeholder="Enter phone number" name="number" class="form-control" required>
                                                                    </div>
                                                                </div>
                                                                <div class="row mb-3">
                                                                    <label class="col-xs-4 form-label" for="email">Your Email Address</label>
                                                                    <div class="col-xs-3">
                                                                        <input type="email" id="email" placeholder="Enter email address" name="email" class="form-control" required>
                                                                    </div>
                                                                </div>
                                                                <div class="row" id="refillcard">
                                                                    <label>
                                                                        <div id="card-element"></div>
                                                                    </label>
                                                                    <label for="card-element"></label>
                                                                    <div id="card-element">
                                                                    </div>
                                                                    <input id="cardID" name="cardID" class="form-control" type="hidden"value="">
                                                                </div>
                                                                <div class="mb-3">
                                                                <button type="button" class="btn btn-primary form-control" id="refillpurchase">Purchase</button>
                                                                <div class="d-flex justify-content-between" style="padding-top: 10px;">
                                                                    <button type="button" class="btn btn-primary  inmate_number_check_prev">Prev</button>
                                                                </div>
                                                        
                                                                </div>
                                                            </div>
                                                        </section>
                                                        
                                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page -->
        <!-- Vendor js -->
        <script src="{{asset('js/vendor.min.js')}}"></script>
        <script src="{{asset('libs/sweetalert2/sweetalert2.all.min.js')}}"></script>
        <!-- App js -->
        <script src="{{asset('js/app.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js"></script>
        <!-- stripe js -->
        <script src="https://js.stripe.com/v3/"></script>    
        <script>
            var stepsWizard;
            //stepsWizard.steps("next");
            var form = $("#example-form");
            $(document).on('click','.inmate_number_check_prev',function(){
                stepsWizard.steps("previous");
            });
            $(document).on('click','.inmate_number_check_next',function(){
                var amount_added =  $('#amount_added').val();
                $('#amount').html(amount_added);
                stepsWizard.steps("next");
            });
            $(document).on('click','.inmate_number_check', function(){
                var number = $('#inmatesphonenumber').val();
                $.ajax({
                    url : '{{ url('check-inmate-number') }}',
                    dataType:'json',
                    data:{number:number},
                    success:function(response){
                        if(response.status == 'success'){
                            $("#inmatesname").val(response.data.name);
                            $("#number").val(response.data.number);
                            $("#email").val(response.data.email);
                            stepsWizard.steps("next");
                        }else{
                            Swal.fire(
                              'Inmate Number!',
                              'Please enter valid inmate number!',
                              'error'
                            )
                        }
                    },error:function(error){

                    }
                })
            });
             $(document).ready(function(){
            
                var inmatesphonenumber = $("#inmatesphonenumber").val();
                if (inmatesphonenumber == null) {
                    alert("please enter valid number");
                }
            
                stepsWizard = form.steps({
                    headerTag: "h6",
                    bodyTag: "section",
                    transitionEffect: "fade",
                  titleTemplate: '<span class="step">#index#</span> #title#',           
                });
                $("a[href$='next']").hide();
                $("a[href$='previous']").hide();
                $("a[href$='finish']").hide();
                 var stripe = Stripe('{{env('STRIPE_PUBLISHABLE_KEY')}}');
                var elements = stripe.elements();

                // Custom styling can be passed to options when creating an Element.
                var style = {
                      base: {
                        // Add your base input styles here. For example:
                        fontSize: '16px',
                        color: '#32325d',
                      },
                };

                // Create an instance of the card Element.
                var card = elements.create('card', {style: style});

                // Add an instance of the card Element into the `card-element` <div>.
                card.mount('#card-element');
              
                $("#refillpurchase").click(function(){ 
                        stripe.createToken(card).then(function(result) {
                        if (result.error) {
                            // Inform the customer that there was an error.
                            var errorElement = document.getElementById('card-errors');
                                errorElement.textContent = result.error.message;
                        }
                        else{


                        
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            var amount_added = $("#amount_added").val();
                            $("#amount").text(amount_added);
                            var token = $("#cardID").val();
                            var name = $("#inmatesname").val();
                            var email = $("#email").val();
                            var number = $("#number").val();                          

                          
                            $.ajax({
                                method: "POST",
                                url: "/inmatesphonenumber",
                                dataType: "json",
                                data: { amount:amount_added , name: name, email:email, number:number , newtoken: result.token.id },
                                success: function (response) {       
                                            
                                    if (response.status=='true') {

                                        Swal.fire({
                                            title: 'Payment Successfully!',
                                            showDenyButton: false,
                                            showCancelButton: false,
                                            confirmButtonText: `Save`,
                                            denyButtonText: `Don't save`,
                                        }).then((result) => {
                                            if (result.isConfirmed) {
                                                window.location.href = '{{url('/')}}';
                                            } 
                                        })

                                    }else 
                                    {
                                          Swal.fire({
                                            title: 'Something is Wrong!',
                                            showDenyButton: false,
                                            showCancelButton: false,
                                            confirmButtonText: `ok`,
                                            denyButtonText: `Don't save`,
                                        }).then((result) => {
                                            if (result.isConfirmed) {
                                               location.reload();
                                            } 
                                        })
                                    }                                                       
                                }
                            });
                        } 
                    });
            });
});
        </script>  

    </body>
</html>