  <!-- footer start -->
  <footer class="bg-dark footer">
    <div class="container-fluid">
        <div class="row mb-5">
            <div class="col-lg-4">
                <div class="pe-lg-4">
                    <div class="mb-4">
                        <img src="images/Mailcall-logo.png" alt="" height="32">
                    </div>
                    
                </div>
            </div>
            
            <div class="col-lg-2 col-md-6">
                <div class="footer-list">
                    <p class="text-white mb-2 footer-list-title">About</p>
                    <ul class="list-unstyled">
                        <li><a href="{{route('fronthome')}}"><i class="mdi mdi-chevron-right me-2"></i>Home</a></li>
                        <li><a href="#features"><i class="mdi mdi-chevron-right me-2"></i>Features</a></li>
                        <li><a href="#faq"><i class="mdi mdi-chevron-right me-2"></i>Faq</a></li>
                        <li><a href="#clients"><i class="mdi mdi-chevron-right me-2"></i>Clients</a></li>
                         <li><a href="#pricing"><i class="mdi mdi-chevron-right me-2"></i>Pricing</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-2 col-md-6">
                <div class="footer-list">
                    <p class="text-white mb-2 footer-list-title">Support</p>
                    <ul class="list-unstyled">
                        <li><a href="{{route('help')}}"><i class="mdi mdi-chevron-right me-2"></i>Help & Support</a></li>
                      
                    </ul>
                </div>
            </div>
          
        <!-- end row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="float-start pull-none">
                    <p class="text-white-50"><script>document.write(new Date().getFullYear())</script> &copy;Maillcall Communication </p>
                    <!-- <p class="text-white-50">2015 - 2020 © Ubold. Design by <a href="https://coderthemes.com/" target="_blank" class="text-white-50">Coderthemes</a> </p> -->
                </div>
                <div class="float-end pull-none">
                    <ul class="list-inline social-links">
                        <li class="list-inline-item text-white-50">
                            Social :
                        </li>
                        <li class="list-inline-item"><a href="https://www.facebook.com/"><i class="mdi mdi-facebook"></i></a></li>
                        <li class="list-inline-item"><a href="https://twitter.com/login"><i class="mdi mdi-twitter"></i></a></li>
                        <li class="list-inline-item"><a href="https://www.instagram.com/accounts/login/"><i class="mdi mdi-instagram"></i></a></li>
                        <li class="list-inline-item"><a href="https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin"><i class="mdi mdi-google"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- container-fluid -->
</footer>
<!-- footer end -->