@extends('Admin.master')

@section('style')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

@endsection

@section('content')


<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">add</li>
                            <li class="breadcrumb-item active">phonenumber</li>
                        </ol>
                    </div>
                     <h4 class="page-title">Add Phonenumber</h4>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="row">


            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Add Phonenumber</h4>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Area Code</h4>
                            <div class="mb-3 input-group">
                                <input type="number" name="areacode" parsley-trigger="change" required placeholder="Enter Your area" class="form-control" id="areacode" min="0"/>
                                &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<div class="input-group-btn">
                                    <button class="btn btn-primary waves-effect waves-light"  type="submit">Get Number</button>
                                </div>
                            </div>
                            <form action="{{ route('storephonenumber') }}" class="parsley-examples" method="POST">

                                @csrf
                        <div class="mb-3 input-group">
                        <h4 class="header-title">Phone number</h4>

                        <select class="dropdown form-control js-example-basic-single" name="phone_number">
                            <option value="+1 5102222555">+1 5102222555</option>
                            <option value="+1 5107894512">+1 5107894512</option>
                            <option value="+1 5203371381">+1 5203371381</option>
                            <option value="+1 5108888666">+1 5108888666</option>
                          </select>
                        </div>
                        <div class="text-center">
                        <button class="btn btn-danger waves-effect waves-light"  type="submit">Save</button>
                        </div>
                    </form>
                        </div>
                    </div>
                </div>
            </div> <!-- end card -->


    </div>
    <!-- end row -->


</div>

@endsection


@section('script')

<script type="text/javascript">
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endsection
