@extends('Admin.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">profile</li>
                    </ol> 
                </div>
                <h4 class="page-title">Profile</h4>
            </div>
        </div>
<!-- @if ($errors->any())
  <div class="alert alert-danger">
     <ul>
        @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
        @endforeach
     </ul>
     @if ($errors->has('email'))
     @endif
  </div>
@endif -->
        <div class="row">
            <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                        <div class="text-center mt-2 mb-4">
                            <h2><i class="fe-user"></i>Profile</h2>
                        </div>

                        <form method="POST" action="{{route('updateprofile')}}" class="needs-validation" novalidate >
                            @csrf

                            <div class="mb-3">
                                <label for="name" class="form-labe" style="color:black; font-weight:bold">{{ __('Name') }}</label>
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required placeholder="Enter your name" value="{{Auth::user()->name}}">
                                    <div class="invalid-feedback">
                                        This Name is required.
                                    </div>
                                    @error('name')
                                      <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                            </div>

                            <div class="mb-3">
                                <label for="email" class="form-labe"style="color:black; font-weight:bold">{{ __('Email Address') }}</label>
                                    <input id="email" type="email" class="form-control" required placeholder="Enter your email address" name="email" value="{{Auth::user()->email}}">
                                    <div class="invalid-feedback">
                                        This Email is required.
                                    </div>
                            </div>
                            <div class="mb-3">
                                <label for="number" class="form-labe" style="color:black; font-weight:bold">{{ __('Phone Number') }}</label>
                                    <input id="number" type="text" class="form-control @error('number') is-invalid @enderror"required placeholder="Enter your number" name="number" value="{{Auth::user()->number}}" min="0">
                                     @error('number')
                                      <div style="color: red;font-weight: bold;">{{ $message }}</div>
                                    @enderror
                                </div>
                            <div class="text-center">
                            
                                    <button type="submit" class="btn btn-success submit">
                                        {{ __('Submit') }}
                                    </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <!-- end col-->
            <div class="col-md-6">
    
            <div class="card">
            <div class="card-body">
                <div class="text-center mt-2 mb-4">
                    <h2><i class="fas fa-key"></i> Change Password</h2>
                </div>
               
                <form method="POST" action="{{route('updatechangepassword')}}" class="needs-validation" novalidate >
                    @csrf

                <div class="mb-3">
                  <label for="password" class="form-labe">{{ __('Current Password') }}</label>
                    <div class="input-group input-group-merge"> 
                        <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password" required placeholder="Enter current password">
                        <div class="input-group-text" data-password="false">
                            <span class="password-eye"></span>
                        </div>
                    </div>
                    <div class="invalid-feedback">
                                This Current Password is required.
                            </div>
                            @if(session('error'))
                              <div style="color: red; font-weight:bold;">{{session('error')}}</div>
                            @endif
                </div>
               
                    <div class="mb-3">
                        <label for="password" class="form-labe">{{ __('New Password') }}</label>
                        <div class="input-group input-group-merge"> 
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" required placeholder="Enter your new password" name="password" value="">
                            <div class="input-group-text" data-password="false">
                                <span class="password-eye"></span>
                            </div>
                        </div>
                        @error('password')
                                      <div style="color: red;font-weight: bold;">{{ $message }}</div>
                        @enderror
                         <div class="invalid-feedback">
                                This New Password is required.
                            </div>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-labe">{{ __('New Confirm Password') }}</label>
                         <div class="input-group input-group-merge"> 
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" required placeholder="Enter your confirm password" name="password_confirmation" value="">
                             <div class="input-group-text" data-password="false">
                                <span class="password-eye"></span>
                            </div>
                        </div>

                        <div class="invalid-feedback">
                            This New Confirm Password is required.
                        </div>
                    </div>
                    <div class="text-center">
                    
                            <button type="submit" class="btn btn-success submit">
                                {{ __('Submit') }}
                            </button>
                    </div>
                </form>

            </div>
        </div>
     </div>
     <!-- end row -->
 </div> 
</div>
</div>                 
</div>
        </div>
    </div><!-- end row -->
</div> 
@endsection
@section('script')
  <script type="text/javascript">
$(document).ready(function(){
        
             $('input:radio').change(function() {
            
                if ($("input[name='flexRadioDefault']:checked").val() == 'newcard') {

                    $("#card1").show();
                    $("#detailsofoldcards").hide();
                    var stripe = Stripe('{{env('STRIPE_PUBLISHABLE_KEY')}}');
                    var elements = stripe.elements();

                    // Custom styling can be passed to options when creating an Element.
                    var style = {
                          base: {
                            // Add your base input styles here. For example:
                            fontSize: '16px',
                            color: '#32325d',
                          },
                    };

                    // Create an instance of the card Element.
                    var card = elements.create('card', {style: style});

                    // Add an instance of the card Element into the `card-element` <div>.
                    card.mount('#card-element'); 

                    $("#newrefill").click(function(){

                       if($("input[name='flexRadioDefault']:checked").val() == 'newcard'){
                                var redio = $("input[name='flexRadioDefault']:checked").val();
                                stripe.createToken(card).then(function(result) {

                                  if (result.error) {
                                  // Inform the customer that there was an error.
                                  var errorElement = document.getElementById('card-errors');
                                  errorElement.textContent = result.error.message;
                                }else{

                                            $.ajaxSetup({
                                                headers: {
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                }
                                            });

                                            var amount = $("#amount").val();
                                            var newcard_token = $("#newcardID").val();

                                            
                                            $.ajax({
                                                
                                                method: "POST",
                                                url: "/refillwallet",
                                                dataType: "json",
                                                data: { amount:amount, type:redio, newcard_token:result.token.id },
                                                success: function (response) {
                                                    Success = true;//doesn't go here
                                                    location.reload();

                                                },
                                                error: function (textStatus, errorThrown) {
                                                    Success = false;//doesn't go here
                                                }

                                            });
                                    }
                                 });  
                            }

                        });
                }else if($("input[name='flexRadioDefault']:checked").val() == 'oldcard')
                {
                    $("#card1").hide();
                    $("#detailsofoldcards").show();
                    
                    $("#newrefill").click(function(){
                        if ($("input[name='flexRadioDefault']:checked").val() == 'oldcard') {
                           var redio = $("input[name='flexRadioDefault']:checked").val();
                           var amount = $("#amount").val();
                           var oldcarddetails = $("#olddetailscards").val();

                           $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });

                                    var amount = $("#amount").val();
                                    var cardid = $("#olddetailscards").val();
                                    
                                    $.ajax({
                                        
                                        method: "POST",
                                        url: "/refillwallet",
                                        dataType: "json",
                                        data: { amount:amount, type:redio, cardid:cardid },
                                        success: function (response) {
                                            Success = true;//doesn't go here
                                            location.reload();

                                        },
                                        error: function (textStatus, errorThrown) {
                                            Success = false;//doesn't go here
                                        }

                                    });

                         }
                    });

                }


            });

           
});

 </script>

@endsection