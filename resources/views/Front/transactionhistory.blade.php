@extends('Admin.master')
@section('content')

<div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                                            <li class="breadcrumb-item active">Transaction History</li>
                                        </ol>
                                    </div>
                                     <h4 class="page-title">Transaction History</h4>
                                </div>
                            </div>
                        </div>      
                    <div class="row">
                            <div class="card">
                                <div class="card-body">
                                    <div class="float-end">
                                    </div>
                                <h4 class="page-title-box">Transaction History</h4>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                <table id="transaction" class="table dt-responsive nowrap w-100">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Transaction ID</th>
                                                            <th>Amount</th>
                                                            <th>Type</th>
                                                            <th>Payment Date</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div> <!-- end card body-->
                                        </div> <!-- end card -->
                                    </div><!-- end col-->
                                </div>
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div> <!-- end card -->
                    </div>
                    <!-- end row -->
@endsection
 
@section('script')
   
<script type="text/javascript">

    $(function () {
      
      var table = $('#transaction').DataTable({
              processing: true,
              serverSide: true,
              dataType: "json",
              pageLength: 10,
              type: "get",
              ajax: {
                  url: "/transactionhistory_list",
                  dataType: "json",
                  type: "get",
                  data: {
                      receipt_no: function() {
                          return $("#receipt_no").val();
                      },                },
              },
              columns: [{
                      data: 'DT_RowIndex',
                      orderable: false,
                      searchable: true
                  },
                  {
                      data: 'transaction_id',
                  },
                  {
                      data: 'amount',
                  },
                  {
                      data: 'type',
                  },
                  {
                      data: 'created_at',
                  },
                  {
                      data: 'status',
                  },
              ]
          });
    });
</script>
@endsection
