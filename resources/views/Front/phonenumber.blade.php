@extends('Admin.master')
@section('content')

<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">phonenumber</li>
                        </ol>
                    </div>
                     <h4 class="page-title">PhoneNumber</h4>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="float-end">
                   <a href="{{ route('addphonenumber') }}" ><button class="btn btn-success">Add PhoneNumber</button></a>
                </div>
            <h4 class="page-title-box">Phone Number</h4>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                            <table id="phonenumbers" class="table dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Phone Number</th>
                                        <th>Action</th>
                                       </tr>
                                </thead>
                            </table>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
                </div>
                <!-- end row -->
            </div>
        </div> <!-- end card -->
</div>
<!-- end row -->
@endsection


@section('script')

<script type="text/javascript">

$(function () {

            var table = $('#phonenumbers').DataTable(
                {
            processing: true,
            serverSide: true,
            dataType: "json",
            pageLength: 10,
            type: "get",
            ajax: {
            url: "{{ route('phonenumber_list') }}",
            dataType: "json",
            type: "get",
            data: {
            receipt_no: function() {
                return $("#receipt_no").val();
            },                },
            },
            columns: [{
            data: 'DT_RowIndex',
            orderable: false,
            searchable: true
            },
            {
            data: 'phone_number',
            },
            {
            data: 'action',
            },
            ]
            }
            );

            $(document).on('click','.servideletebtn' , function(e){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id  = $(this).attr('data-id');
        e.preventDefault();
        // alert(type);
    Swal.fire({
        title: 'Are you sure?',
        text: 'You want to delete the record',
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: `Yes`,
        denyButtonText: `No`,
        }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {

            var data = {

                "_token": "{{ csrf_token() }}",
                "id": id,
            };

            $.ajax({

                type: "DELETE",
                url: '/phonenumber/delete/'+ id,
                data: data,
                success: function(result){
                    if(result.success){
                        Swal.fire({
                            icon:"success",
                            title:"Your record successfully deleted",
                        });
                      table.ajax.reload();
                    }
                //    $('#users').data.reload();
                //    table.reload();
                }

            })


        } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
        }
        })
    });
});
</script>
@endsection
