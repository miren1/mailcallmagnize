@extends('Front.master')
@section('content')
   <section class="bg-home bg-gradient" id="home" style="padding: 112px 0px 22px 0px;">
    <div class="home-center">
        <div class="home-desc-center">
            <div class="text-center">
                            <p style="color: white; font-size: 35px; font-family: 'Nunito', sans-serif;">Help</p>
            </div>
                <!-- end row -->
            </div>
            <!-- end container-fluid -->
        </div>
    </div>
</section>

<section class="section-sm" id="aboutus">
    <div class="container-fluid">
  
        <div class="row">     
                 <p style="color: black; font-size: calc(1.275rem + .3vw); font-weight: bold; font-family: 'Nunito', sans-serif;">What is Mail Call Communication?</p>

                 <p style="color: black; font-size: 15px; margin-left:3%; font-family: 'Nunito', sans-serif;">- Mail call communication is a system that is designed to help inmates to connect with their loved ones. Communication to and from prison is very difficult and Mail call Communication aims to bring ease to this process by making communication between inmates and their family easy.</p>

                 <p style="color: black; font-size: calc(1.275rem + .3vw); font-weight: bold; font-family: 'Nunito', sans-serif;">Our Goal</p>

                 <p style="color: black; font-size: 15px; margin-left:3%; font-family: 'Nunito', sans-serif;">- Our goal is to build a strong connection with your loved ones in your confinement.</p>

                 <p style="color: black; font-size: calc(1.275rem + .3vw); font-weight: bold; font-family: 'Nunito', sans-serif;">How does it work?</p>
                 <br>
                 <p style="color: black; font-size: 15px; margin-left:3%; font-family: 'Nunito', sans-serif;">- Our aim is to provide you a friendly and easy to use system so that you can connect your loved ones in no time</p>

                 <p style="color: black; font-size: 20px; font-family: 'Nunito', sans-serif;">Let’s Get Started!</p>


                 <p style="color: black; margin-left: 3%; font-size: 20px; font-family: 'Nunito', sans-serif;">• How to create an Account?</p>
 <div style="margin-left: 7%;">
                 <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;"> - It's a piece of cake. We understand your condition and we designed this in a simplest manner.First step is to register yourself as a candidate.</p>

                 <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">1. Click on the register icon.</p>

        <p style="color: black; font-size: 15px; font-weight: bold; font-family: 'Nunito', sans-serif;">- You will be directed towards the registration panel</p>

                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">2. Enter your name.(Compulsory)</p>
                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">3. Enter your DOC number.(Compulsory)</p>
                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">4. Enter your location.(Optional)</p>
                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">  5. Click Next.</p>

        <p style="color: black; font-size: 15px; font-weight: bold; font-family: 'Nunito', sans-serif;">- Now you will be directed to another panel.</p>

                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">6. Enter your area code.</p>

                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">7. Click on “Get Number”</p>

                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">8. Choose your desired number.</p>
                  
                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">9. Click next.(You can also preview your entered information by clicking PREV).</p>

                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">10. You will be directed to the verification panel.</p>
                   
                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">11. Verify if you are the main contact or not.</p>

                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">12. Click Next.</p>

        <p style="color: black; font-size: 15px; font-weight: bold; font-family: 'Nunito', sans-serif;">- Now you will be directed to your contact information panel.</p>

                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">13. Enter your Contact name.</p>

                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">14. Enter your Contact number.</p>

                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">15. Enter your contact email address.</p>
                  
                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">16. Click Next.</p>
        <p style="color: black; font-size: 15px; font-weight: bold; font-family: 'Nunito', sans-serif;">- Now, we ask you for a little favor. </p>
                 <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">17. Please let us know how you know about us. This will help us to reach more people who need our help. </p>
                  
                  <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">18. Click Next.</p>
        <p style="color: black; font-size: 15px; font-weight: bold; font-family: 'Nunito', sans-serif;">- AND BOOM! YOU ARE ALL SET.</p>
                  
                <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">19. Enter your name(optional).</p>
                  
                <p style="color: black; font-size: 15px; font-family: 'Nunito', sans-serif;">20. Enter your 6-digit password. (We care about your privacy)</p>

        <p style="color: black; font-size: 15px; font-weight: bold; font-family: 'Nunito', sans-serif;">- Let's pay a tiny amount of fee and get started!</p>
</div>
        <p style="color: black; margin-left: 3%; font-size: 20px; font-family: 'Nunito', sans-serif;">• How can I pay?</p>
                <p style="color: black; font-size: 15px; margin-left:7%; font-family: 'Nunito', sans-serif;">- After making an account you can pay directly from your card.</p>
                <p style="color: black; font-size: 15px; margin-left:7%; font-family: 'Nunito', sans-serif;"> 1. Enter card number.</p>
                <p style="color: black; font-size: 15px; margin-left:7%; font-family: 'Nunito', sans-serif;"> 2. Enter valid date for card.</p>
                <p style="color: black; font-size: 15px; margin-left:7%; font-family: 'Nunito', sans-serif;"> 3. Click purchase.</p>
        <p style="color: black; margin-left: 3%; font-size: 20px; font-family: 'Nunito', sans-serif;">• How much do I have to pay?</p>

                <p style="color: black; font-size: 15px; margin-left:7%; font-family: 'Nunito', sans-serif;"> - It's quite cheap. You just have to pay $6 for getting your monthly subscription for your private phone number.</p>
                
                <p style="color: black; font-size: 15px; margin-left:7%; font-family: 'Nunito', sans-serif;"> - Service charges for one month are $4.99.</p>
                <p style="color: black; font-size: 15px; margin-left:7%; font-family: 'Nunito', sans-serif;"> - Kindly have a balance of $10 in your account as it will help you in case of low balance. In case of not paying charges, you will lose your number and you have to start all over again. </p>

                <p style="color: black; font-size: 15px; margin-left:7%; font-family: 'Nunito', sans-serif;"> - Per text charges are $0.095. </p>
                <p style="color: black; font-size: 15px; margin-left:7%; font-family: 'Nunito', sans-serif;"> - Per voicemail charges are $0.02.</p>

                <p style="color: black; font-size: 15px; margin-left:7%; font-family: 'Nunito', sans-serif;"> - Per minute call charges are $0.02.</p>
                   
        <p style="color: black; margin-left: 3%; font-size: 20px; font-family: 'Nunito', sans-serif;">• What if I ran out of balance?</p>
                <p style="color: black; font-size: 15px; margin-left:7%; font-family: 'Nunito', sans-serif;"> - At mail call communication, we offer more than simple voice mail. Our goal is to provide you quality calls and text messages by following easy steps and at cheap prices.</p>


                      
        </div>
    </div> 
</section>
<!-- end features -->
@endsection