  <!--Navbar Start-->
  <nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky-dark" id="sticky">
    <div class="container-fluid">
        <!-- LOGO -->
        <a class="logo text-uppercase" href="{{route('fronthome')}}">
            <img src="{{asset('images/Mailcall-logo.png')}}" alt="" class="logo-light" height="42" />
            <img src="{{asset('images/login-logo.png')}}" alt="" class="logo-dark" height="34" />
        </a>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="mdi mdi-menu"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mx-auto navbar-center" id="mySidenav">
                <li class="nav-item">
                    <a href="{{route('fronthome')}}" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('features')}}" class="nav-link">Features</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('pricing')}}" class="nav-link">Pricing</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('faq')}}" class="nav-link">FAQs</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('feedback')}}" class="nav-link">Feedback</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('contact')}}" class="nav-link">Contact</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('aboutus')}}" class="nav-link">About Us</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('help')}}" class="nav-link">Help</a>
                </li>
                @guest
                @if (Route::has('login'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                @endif
      
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
            <li class="nav-item dropdown notification-list topbar-dropdown">
                <a class="nav-link dropdown-toggle nav-user me-0 waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <span class="pro-user-name ms-1">
                        {{Auth::user()->name}} <i class="mdi mdi-chevron-down"></i> 
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-end profile-dropdown ">
                    <!-- item-->
                    <div class="dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome {{Auth::user()->name}}!</h6>
                    </div>
    
                    <!-- item-->
                    <a href="{{route('home')}}" class="dropdown-item notify-item">
                        <i class="fe-airplay me-1"></i>
                        <span>Dashboard</span>
                    </a>
                    <div class="dropdown-divider"></div>
    
                    <!-- item-->
                    <a href="{{ route('logout') }}" class="dropdown-item notify-item"  onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="fe-log-out"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>       
            @endguest
            </ul>
            
        </div>
    </div>
</nav>
<!-- Navbar End -->
