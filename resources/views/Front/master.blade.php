<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8" />
        <title>Mailcall Communication</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 

        @include('Front.header-script')
        @yield('style')
    </head>
    
    <body data-bs-spy="scroll" data-bs-target=".navbar" data-bs-offset="78">

      @include('Front.header')
     
        @yield('content')

      @include('Front.footer')
        
        <!-- Back to top -->    
        <!-- <a href="#" class="back-to-top" id="back-to-top"> <i class="mdi mdi-chevron-up"> </i> </a> -->
        <!-- Back to top -->
        <a href="#" onclick="topFunction()" class="back-to-top-btn btn btn-primary" id="back-to-top-btn"><i class="mdi mdi-chevron-up"></i></a>

       @include('Front.footer-script')
       @yield('script')
      
    </body>


</html>