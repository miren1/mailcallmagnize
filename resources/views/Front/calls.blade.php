@extends('Admin.master')
@section('content')

<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Calls</li>
                        </ol>
                    </div>
                     <h4 class="page-title">Calls</h4>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="float-end">
                </div>
            <h4 class="page-title-box">Calls</h4>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                            <table id="calls" class="table dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Type</th>
                                        <th>Twilio Number</th>
                                        <th>Phone Number</th>
                                        <th>Status</th>
                                        <th>Time</th>
                                        <th>Duration</th>
                                        <th>Call Charges</th>
                                       </tr>
                                </thead>
                            </table>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
                </div>
                <!-- end row -->
            </div>
        </div> <!-- end card -->
</div>
<!-- end row -->
@endsection


@section('script')

<script type="text/javascript">

$(function () {

            var table = $('#calls').DataTable(
                {
            processing: true,
            serverSide: true,
            dataType: "json",
            pageLength: 10,
            type: "get",
            ajax: {
            url: "/calls_list",
            dataType: "json",
            type: "get",
            data: {
            receipt_no: function() {
                return $("#receipt_no").val();
            },                },
            },
            columns: [{
            data: 'DT_RowIndex',
            orderable: false,
            searchable: true
            },
            {
               data: 'type',
            },
            {
               data: 'twilio_number',
            },
            {
               data: 'phone_number',
            },
            {
               data: 'status',
            },
            {
               data: 'created_at',
            },
            {
                data: 'duration',
            },
            {
                data: 'call_charge',
            },
            ]
            }
            );

});
</script>
@endsection
