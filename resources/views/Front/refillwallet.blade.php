@extends('Admin.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Refill Account</li>
                    </ol> 
                </div>
                <h4 class="page-title">Refill Account</h4>
            </div>
        </div>
    </div>      
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif 
<div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                
                        <div class="text-center mt-2 mb-4">
                            <h2><i class="fas fa-charging-station"></i></i> Refill Account</h2>
                        </div>
                        @if(Auth::user()->wallet <= 0)
                             <a style="color: red;">You have insufficient balance, please refill your account balance!</a>
                            @endif
                       <div class="mb-3">
                            

                            <label for="text" class="form-labe" style="color:black; font-weight:bold">{{ __(' Your System Number :') }}</label>
                            <a style="color:black; font-size:20px;" >
                                @if(isset($twilioNumber->number)) {{$twilioNumber->number}}  @endif

                            </a>                      
                            <br>
                           
                            <label for="text" class="form-labe" style="color:black; font-weight:bold">{{ __(' Current Balance :') }}</label>
                            <a style="color:black; font-size:20px;" >
                            $ {{Auth::user()->wallet}}
                            </a>      
                        </div>
                            <form method="POST" action="#">
                                @csrf
                                    <div class="mb-3">
                                          <select name="amount" id="amount" class="form-control" required>
                                                      <option value="1">$1</option>
                                                      <option value="5">$5</option>
                                                      <option value="10">$10</option>
                                                      <option value="15">$15</option>
                                                      <option value="20">$20</option>
                                                      <option value="25">$25</option>
                                                      <option value="30">$30</option>
                                                      <option value="35">$35</option>
                                                      <option value="40">$40</option>
                                                      <option value="45">$45</option>
                                                      <option value="50">$50</option>
                                                      <option value="60">$60</option>
                                                      <option value="70">$70</option>
                                                      <option value="80">$80</option>
                                                      <option value="90">$90</option>
                                                      <option value="100">$100</option>
                                                      <option value="110">$110</option>
                                                      <option value="120">$120</option>
                                                      <option value="130">$130</option>
                                                      <option value="140">$140</option>
                                                      <option value="150">$150</option>
                                                      <option value="160">$160</option>
                                                      <option value="170">$170</option>
                                                      <option value="180">$180</option>
                                                      <option value="190">$190</option>
                                                      <option value="200">$200</option>
                                                      </select>
                                    </div>                                
                                       <div class="form-check">
                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="existingcard" value="oldcard" >
                                        <label class="form-check-label" for="flexRadioDefault2" style="color:black; font-size:15px;" >
                                            Saved Card
                                          </label>
                                    </div>
                                     <div class="form-check">
                                          <input class="form-check-input" type="radio" name="flexRadioDefault" id="newcard" value="newcard">
                                          <label class="form-check-label" for="flexRadioDefault1" style="color:black; font-size:15px;">
                                            New Card
                                          </label>
                                    </div>
                                
                                    &nbsp;
                                    <div class="row mb-3" id="card1">
                                        <label>
                                        <div id="card-element"></div>
                                        </label>

                                        <label for="card-element"></label>
                                        <div id="card-element">
                                            
                                        </div>
                                        <input id="newcardID" name="newcardID" class="form-control" type="hidden"value="">
                                    </div>
                                   
                                
                                <div class="mb-3" id="detailsofoldcards" style="display: none;">
                                    @if(isset($getcard) && $getcard)
                                     <select name="allcards" id="olddetailscards" class="form-control" required>
                                       @foreach($getcard as $cards)
                                       @foreach($cards as $oldcards)
                                              <option value="{{$oldcards->id}}">{{$oldcards->last4}}-{{$oldcards->brand}}</option>
                                       @endforeach
                                       @endforeach
                                     </select>
                                     @endif
                                 </div>                                
                                 
                                    <div class="row mb-3">
                                        <button type="button" id="newrefill" class="btn btn-success">
                                                        {{ __('Refill Account') }}
                                                  </button>
                                    </div>

                                </form>                        
                 </div>
            </div>
        </div>
       <div class="col-md-6">
                <div class="card">
                     <div class="card-body">
                        
                         <div class="text-center mt-2 mb-4">
                            <h2><i class="fe-bell"></i></i> Subscription Detail</h2>
                        </div>
                      <div>
                        @if(Auth::user()->status == 'Accepted')
                       <a  style="font-size:20px; font-weight: bold;"><button class="btn btn-danger servecanclesub">
                       <i class="fe-bell-off"> Cancel Subscription
                        </button></i></a>
                        @elseif(Auth::user()->status == 'Suspended')
                        <a  style="font-size:20px; font-weight: bold;"> <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#standard-modal"><i class="fe-bell"></i> Subscription</button></i></a>
                        @endif
                      </div>&nbsp;
                         <div id="standard-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="standard-modalLabel"><i class="fe-bell"></i> Re-Subscription</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                              <div class="form-check">
                                <input class="form-check-input" type="radio" name="subscriptionradiobtn" id="subexistingcard" value="subexistingcard">
                                <label class="form-check-label" for="subscriptionradiobtn2" style="color:black; font-size:15px;">
                                    Saved Card
                                  </label>
                            </div>                      
                              <div class="form-check">
                                  <input class="form-check-input" type="radio" name="subscriptionradiobtn" id="subnewcard" value="subnewcard">
                                  <label class="form-check-label" for="subscriptionradiobtn1" style="color:black; font-size:15px;">
                                    New Card
                                  </label>
                            </div>
                           
                            &nbsp;                    


                            <div class="row mb-3" id="subscription12" style="display:none;">
                                <label>
                                <div id="card-element1"></div>
                                </label>

                                <label for="card-element1"></label>
                                <div id="card-element1">
                                    
                            </div>
                                <input id="newsubscriptioncard" name="newsubscriptioncard" class="form-control" type="hidden"value="">
                            </div>
                           
                        
                        <div class="mb-3" id="existingsubcard" style="display:none;">
                             <select name="allsubcards" id="oldsubcards" class="form-control" required>
                              @foreach($getcard as $cards)
                               @foreach($cards as $oldcards)
                                      <option value="{{$oldcards->id}}">{{$oldcards->last4}}-{{$oldcards->brand}}</option>
                               @endforeach
                               @endforeach
                             </select>
                         </div>                 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="button" id="subscriptionsubmitbtn" class="btn btn-primary">Subscribe</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    
                
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                            <table id="history" class="table dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Transaction ID</th>
                                        <th>Amount</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Payment Date</th>
                                    </tr>
                                </thead>

                            </table>
                        </div> <!-- end card body-->
                    </div> <!-- end card -->
                </div><!-- end col-->
            </div>
                </div>


                    </div>
                        
            </div>
        </div>
   
    </div><!-- end row -->
</div> 
@endsection
@section('script')
  <script type="text/javascript">
$(document).ready(function(){
            // if($("input[name='flexRadioDefault']:checked").val() == 'oldcard'){
            //    $("#detailsofoldcards").show();
            // }
            // if ($("input[name='subscriptionradiobtn']:checked").val() == 'subexistingcard') {
            //    $("#existingsubcard").show();
            // }
             $('input:radio').change(function() {
            
                if ($("input[name='flexRadioDefault']:checked").val() == 'newcard') {

                    $("#card1").show();
                    $("#detailsofoldcards").hide();
                    var stripe = Stripe('{{env('STRIPE_PUBLISHABLE_KEY')}}');
                    var elements = stripe.elements();

                    // Custom styling can be passed to options when creating an Element.
                    var style = {
                          base: {
                            // Add your base input styles here. For example:
                            fontSize: '16px',
                            color: '#32325d',
                          },
                    };

                    // Create an instance of the card Element.
                    var card = elements.create('card', {style: style});

                    // Add an instance of the card Element into the `card-element` <div>.
                    card.mount('#card-element'); 

                    $("#newrefill").click(function(){
                      $(this).prop( "disabled", true );
                       if($("input[name='flexRadioDefault']:checked").val() == 'newcard'){
                                var redio = $("input[name='flexRadioDefault']:checked").val();
                                stripe.createToken(card).then(function(result) {

                                  if (result.error) {
                                  // Inform the customer that there was an error.
                                  var errorElement = document.getElementById('card-errors');
                                  errorElement.textContent = result.error.message;
                                }else{

                                            $.ajaxSetup({
                                                headers: {
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                }
                                            });

                                            var amount = $("#amount").val();
                                            var newcard_token = $("#newcardID").val();

                                            
                                            $.ajax({
                                                
                                                method: "POST",
                                                url: "/refillwallet",
                                                dataType: "json",
                                                data: { amount:amount, type:redio, newcard_token:result.token.id },
                                                success: function (response) {
                                                   if (response.status=='true') {

                                                    Swal.fire({
                                                          title: 'Success',
                                                          text: 'Payment done Successfully',
                                                          icon: 'success',
                                                          confirmButtonText: 'Ok'
                                                        }).then((result) => {
                                                          if (result.isConfirmed) {
                                                            location.reload();
                                                          }
                                                        });
                                                    // location.reload();
                                                   }else
                                                   {
                                                     Swal.fire({
                                                          title: 'Somthing is Wrong!',
                                                          icon: 'warning',
                                                          confirmButtonText: 'Ok'
                                                        }).then((result) => {
                                                          if (result.isConfirmed) {
                                                            location.reload();
                                                          }
                                                        });
                                                   }
                                                    
                                                    
                                                },
                                                error: function (textStatus, errorThrown) {
                                                   $(this).prop( "disabled", false );   
                                                    Success = false;//doesn't go here
                                                }

                                            });
                                    }
                                 });  
                            }

                    });
                }else if($("input[name='flexRadioDefault']:checked").val() == 'oldcard')
                {
                    $("#card1").hide();
                    $("#detailsofoldcards").show();

                    $("#newrefill").click(function(){
                      $(this).prop( "disabled", true );
                        if ($("input[name='flexRadioDefault']:checked").val() == 'oldcard') {
                           var redio = $("input[name='flexRadioDefault']:checked").val();
                           var amount = $("#amount").val();
                           var oldcarddetails = $("#olddetailscards").val();

                           $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });

                                    var amount = $("#amount").val();
                                    var cardid = $("#olddetailscards").val();
                                    
                                    $.ajax({
                                        
                                        method: "POST",
                                        url: "/refillwallet",
                                        dataType: "json",
                                        data: { amount:amount, type:redio, cardid:cardid },
                                        success: function (response) {
                                        if (response.status=='true') {   
                                            Swal.fire({
                                                          title: 'Success',
                                                          text: 'Payment Successfully',
                                                          icon: 'success',
                                                          confirmButtonText: 'Ok'
                                                        }).then((result) => {
                                                          if (result.isConfirmed) {

                                                            location.reload();
                                                          }
                                            });                    
                                        }else{
                                          Swal.fire({
                                                title: 'Somthing is Wrong!',
                                                icon: 'success',
                                                confirmButtonText: 'Ok'
                                          }).then((result) => {
                                                if (result.isConfirmed) {

                                                  location.reload();
                                                }
                                          });  
                                        }
                                        },
                                        error: function (textStatus, errorThrown) {
                                            $(this).prop( "disabled", false ); 
                                            Success = false;//doesn't go here
                                        }

                                    });
                         }
                    });

                }
            });   

             var table = $('#history').DataTable(
                {
            processing: true,
            serverSide: true,
            dataType: "json",
            pageLength: 10,
            type: "get",
            ajax: {
            url: "/history",
            dataType: "json",
            type: "get",
            data: {
            receipt_no: function() {
                return $("#receipt_no").val();
            },                },
            },
            columns: [{
            data: 'DT_RowIndex',
            orderable: false,
            searchable: true
            },
            {
            data: 'transaction_id',
            },
            {
            data: 'amount',
            },
            {
            data: 'type',
            },
            {
              data: 'status',
            },
            {
              data: 'created_at',
            },
            ]
            }
        );

              $(document).on('click','.servecanclesub' , function(e){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id  = $(this).attr('data-id');
        e.preventDefault();
        // alert(type);
    Swal.fire({
        title: 'Are you sure?',
        text: 'You want to Cancel Subscription?',
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: `Yes`,
        denyButtonText: `No`,
        }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {

            var data = {

                "_token": "{{ csrf_token() }}",
                "id": id,
            };

            $.ajax({

                type: "get",
                url: '/canclesubscription',   
                data: data,
                success: function(result){
                    if(result.success){
                        Swal.fire({
                          title: 'Success',
                          text: 'Your Subscription cancelled',
                          icon: 'success',
                          confirmButtonText: 'Ok'
                        }).then((result) => {
                          if (result.isConfirmed) {

                            location.reload();
                          }
                        });
                    }
                   // location.reload();
                //    $('#users').data.reload();
                //    table.reload();
                }
                
            })
         

        } else if (result.isDenied) {
            Swal.fire('Changes are not saved', '', 'info')
        }
        })
    });

              //In model input ajax
                $('input:radio').change(function() {
            
                if ($("input[name='subscriptionradiobtn']:checked").val() == 'subnewcard') {

                    $("#subscription12").show();
                    $("#existingsubcard").hide();
                    var stripe = Stripe('{{env('STRIPE_PUBLISHABLE_KEY')}}');
                    var elements = stripe.elements();

                    // Custom styling can be passed to options when creating an Element.
                    var style = {
                          base: {
                            // Add your base input styles here. For example:
                            fontSize: '16px',
                            color: '#32325d',
                          },
                    };

                    // Create an instance of the card Element.
                    var card = elements.create('card', {style: style});

                    // Add an instance of the card Element into the `card-element` <div>.
                    card.mount('#card-element1'); 

                    $("#subscriptionsubmitbtn").click(function(){

                       if($("input[name='subscriptionradiobtn']:checked").val() == 'subnewcard'){
                                var redio = $("input[name='subscriptionradiobtn']:checked").val();
                                stripe.createToken(card).then(function(result) {

                                  if (result.error) {
                                  // Inform the customer that there was an error.
                                  var errorElement = document.getElementById('card-errors');
                                  errorElement.textContent = result.error.message;
                                }else{

                                            $.ajaxSetup({
                                                headers: {
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                }
                                            });

                                            var subscripnewcard_token = $("#newsubscriptioncard").val();

                                            
                                            $.ajax({
                                                
                                                method: "POST",
                                                url: "/subscription",
                                                dataType: "json",
                                                data: { type:redio, subscripnewcard_token:result.token.id },
                                                success: function (response) {
                                                   if (response.success) {

                                                    Swal.fire({
                                                          title: 'Success',
                                                          text: 'Payment Successfully',
                                                          icon: 'success',
                                                          confirmButtonText: 'Ok'
                                                        }).then((result) => {
                                                          if (result.isConfirmed) {
                                                            location.reload();
                                                          }
                                                        });
                                                      // location.reload();
                                                   }
                                                    
                                                    
                                                },
                                                error: function (textStatus, errorThrown) {
                                                    Success = false;//doesn't go here
                                                }

                                            });
                                    }
                                 });  
                            }

                    });
                }else if($("input[name='subscriptionradiobtn']:checked").val() == 'subexistingcard')
                {
                    $("#subscription12").hide();
                    $("#existingsubcard").show();
                    
                    $("#subscriptionsubmitbtn").click(function(){
                        if ($("input[name='subscriptionradiobtn']:checked").val() == 'subexistingcard') {
                           var redio = $("input[name='subscriptionradiobtn']:checked").val();
        
                           var oldsubcards = $("#oldsubcards").val();

                           $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });

                                    var amount = $("#amount").val();
                                    var subcardid = $("#oldsubcards").val();
                                    
                                    // alert(subcardid);

                                    $.ajax({
                                        
                                        method: "POST",
                                        url: "/subscription",
                                        dataType: "json",
                                        data: { type:redio, subcardid:subcardid },
                                        success: function (response) {
                                            if (response.success) {

                                             
                                            Swal.fire({
                                                          title: 'Success',
                                                          text: 'Subscription Successfully',
                                                          icon: 'success',
                                                          confirmButtonText: 'Ok'
                                                        }).then((result) => {
                                                          if (result.isConfirmed) {
                                                            location.reload();
                                                          }
                                                        });
                                                    
                                            }   
                                            // location.reload();

                                        },
                                        error: function (textStatus, errorThrown) {
                                            Success = false;//doesn't go here
                                        }

                                    });
                         }
                    });

                }
            }); 


});

 </script>

@endsection