-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 05, 2021 at 10:15 AM
-- Server version: 10.2.37-MariaDB
-- PHP Version: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `xsquare_mailcal`
--

-- --------------------------------------------------------

--
-- Table structure for table `calls`
--

CREATE TABLE `calls` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twilio_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voicemail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `calls`
--

INSERT INTO `calls` (`id`, `user_id`, `type`, `phone_number`, `twilio_number`, `status`, `duration`, `voicemail`, `created_at`, `updated_at`) VALUES
(1, 22, 'send ', '5102653235', '5108796532', 'active', '23', 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3', '2021-07-05 10:54:02', '2021-07-05 10:54:02');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ivrs`
--

CREATE TABLE `ivrs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `say_message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sid` int(11) NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twilio_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_view` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2021_06_08_060713_create_users_table', 1),
(4, '2021_06_08_113708_add_status_to_users', 1),
(5, '2021_06_10_080131_create_prices_table', 1),
(6, '2021_06_10_113019_create_places_table', 1),
(7, '2021_06_23_120638_create_twilionumbers_table', 1),
(8, '2021_06_23_124219_create_calls_table', 1),
(9, '2021_06_24_050721_create_twilio_numbers_table', 2),
(10, '2021_06_24_054602_create_calls_table', 3),
(11, '2021_06_24_085931_add_recording_duration_to_calls', 4),
(12, '2021_06_24_114711_create_phone_number_table', 5),
(13, '2021_06_24_132705_create_message_table', 6),
(14, '2021_06_25_053345_create_message_table', 7),
(15, '2021_06_25_053900_create_message_table', 8),
(16, '2021_06_25_104945_create_ivr_table', 9),
(17, '2021_06_26_060745_create_ivr_table', 10),
(18, '2021_06_29_052618_add_status_to_places_table', 11),
(19, '2021_07_01_071404_create_plan_table', 12),
(20, '2021_07_01_122133_add_multiple_column_to_users', 13),
(21, '2021_07_01_131944_create_users_table', 14);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `phone_number`
--

CREATE TABLE `phone_number` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `sid` int(11) NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `federal_prison` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prison_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `plan_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `plan_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'plan_JmFLheodd4fP0L', 'Inactive', '2021-07-02 07:32:18', '2021-07-02 07:35:06'),
(2, 'plan_JmFOijrSfeQ3Cn', 'Inactive', '2021-07-02 07:35:06', '2021-07-02 10:13:58'),
(3, 'plan_JmHxKkdh3oTvKk', 'Active', '2021-07-02 10:13:58', '2021-07-02 10:13:58');

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE `prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sms` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `call` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prices`
--

INSERT INTO `prices` (`id`, `phone_number`, `sms`, `voice`, `call`, `created_at`, `updated_at`) VALUES
(1, '5', '5', '5', '5', '2021-07-02 07:32:16', '2021-07-02 10:13:56');

-- --------------------------------------------------------

--
-- Table structure for table `transactionhistories`
--

CREATE TABLE `transactionhistories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(5,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactionhistories`
--

INSERT INTO `transactionhistories` (`id`, `user_id`, `transaction_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 22, 'txn_1J8lvZBvllNJFnA55S8wB0LE', '70.00', '2021-07-02 12:57:05', '2021-07-02 12:57:05'),
(2, 22, 'txn_1J8mDyBvllNJFnA5xte6MjiQ', '50.00', '2021-07-02 13:16:06', '2021-07-02 13:16:06'),
(3, 22, 'txn_1J8mKvBvllNJFnA5v3R3aThv', '40.00', '2021-07-02 13:23:17', '2021-07-02 13:23:17'),
(4, 22, 'txn_1J90VUBvllNJFnA5rBV2e1qX', '50.00', '2021-07-03 04:31:09', '2021-07-03 04:31:09'),
(5, 22, 'txn_1J90W0BvllNJFnA57tzeUOAr', '50.00', '2021-07-03 04:31:41', '2021-07-03 04:31:41'),
(6, 22, 'txn_1J90X7BvllNJFnA5A9v8FKUV', '10.00', '2021-07-03 04:32:50', '2021-07-03 04:32:50'),
(7, 22, 'txn_1J90qaBvllNJFnA5NnHWgKT8', '10.00', '2021-07-03 04:52:57', '2021-07-03 04:52:57'),
(8, 22, 'txn_1J90rSBvllNJFnA5Ttf15yKq', '10.00', '2021-07-03 04:53:51', '2021-07-03 04:53:51'),
(9, 22, 'txn_1J90u4BvllNJFnA53V8dfHdV', '10.00', '2021-07-03 04:56:33', '2021-07-03 04:56:33'),
(10, 22, 'txn_3J90vEBvllNJFnA51GbBZelK', '10.00', '2021-07-03 04:57:44', '2021-07-03 04:57:44'),
(11, 22, 'txn_1J90vzBvllNJFnA5F43aqmCF', '10.00', '2021-07-03 04:58:32', '2021-07-03 04:58:32'),
(12, 22, 'txn_1J90wsBvllNJFnA5ADrjVPQU', '10.00', '2021-07-03 04:59:26', '2021-07-03 04:59:26'),
(13, 22, 'txn_1J90ydBvllNJFnA5avC8ZMXq', '20.00', '2021-07-03 05:01:15', '2021-07-03 05:01:15'),
(14, 22, 'txn_1J90zGBvllNJFnA5bB0gStVZ', '20.00', '2021-07-03 05:01:55', '2021-07-03 05:01:55'),
(15, 22, 'txn_3J911zBvllNJFnA51grVY0qW', '10.00', '2021-07-03 05:04:44', '2021-07-03 05:04:44'),
(16, 22, 'txn_1J9131BvllNJFnA5MO0Jdyiq', '10.00', '2021-07-03 05:05:48', '2021-07-03 05:05:48'),
(17, 22, 'txn_3J913oBvllNJFnA51hlaDtoS', '10.00', '2021-07-03 05:06:37', '2021-07-03 05:06:37'),
(18, 22, 'txn_1J914WBvllNJFnA5DkjHkPX3', '10.00', '2021-07-03 05:07:20', '2021-07-03 05:07:20'),
(19, 22, 'txn_1J9157BvllNJFnA5he3WfjiY', '10.00', '2021-07-03 05:07:58', '2021-07-03 05:07:58'),
(20, 22, 'txn_1J916dBvllNJFnA5WIpLif0V', '10.00', '2021-07-03 05:09:32', '2021-07-03 05:09:32'),
(21, 22, 'txn_3J917iBvllNJFnA5022nMVbu', '10.00', '2021-07-03 05:10:39', '2021-07-03 05:10:39'),
(22, 22, 'txn_1J917yBvllNJFnA5Qtqxw7P7', '10.00', '2021-07-03 05:10:54', '2021-07-03 05:10:54'),
(23, 22, 'txn_1J918CBvllNJFnA5dLDIfDtC', '10.00', '2021-07-03 05:11:09', '2021-07-03 05:11:09'),
(24, 22, 'txn_3J918OBvllNJFnA50tCDcdj0', '10.00', '2021-07-03 05:11:21', '2021-07-03 05:11:21'),
(25, 22, 'txn_1J918yBvllNJFnA5c1rnjVp5', '10.00', '2021-07-03 05:11:57', '2021-07-03 05:11:57'),
(26, 22, 'txn_1J9198BvllNJFnA53nUDmJKY', '10.00', '2021-07-03 05:12:06', '2021-07-03 05:12:06'),
(27, 22, 'txn_1J919yBvllNJFnA5fkokHTBh', '10.00', '2021-07-03 05:12:58', '2021-07-03 05:12:58'),
(28, 22, 'txn_1J91ARBvllNJFnA580ojPY9k', '10.00', '2021-07-03 05:13:27', '2021-07-03 05:13:27'),
(29, 22, 'txn_1J91AZBvllNJFnA5Hlsmz8Ci', '15.00', '2021-07-03 05:13:35', '2021-07-03 05:13:35'),
(30, 22, 'txn_1J91AlBvllNJFnA5MrHLlNKd', '1.00', '2021-07-03 05:13:48', '2021-07-03 05:13:48'),
(31, 22, 'txn_1J91BFBvllNJFnA5THz6Y8um', '10.00', '2021-07-03 05:14:18', '2021-07-03 05:14:18'),
(32, 22, '0', '35.00', '2021-07-03 10:36:05', '2021-07-03 10:36:05'),
(33, 22, 'txn_1J9jnoBvllNJFnA5IlbWt2Ww', '30.00', '2021-07-05 04:53:05', '2021-07-05 04:53:05'),
(34, 22, 'txn_1J9jx2BvllNJFnA5bPNHeJry', '20.00', '2021-07-05 05:02:36', '2021-07-05 05:02:36'),
(35, 22, 'txn_1J9k2RBvllNJFnA5v7BtFGbU', '45.00', '2021-07-05 05:08:11', '2021-07-05 05:08:11'),
(36, 22, 'txn_1J9neeBvllNJFnA5ykW7z3Y6', '10.00', '2021-07-05 08:59:53', '2021-07-05 08:59:53');

-- --------------------------------------------------------

--
-- Table structure for table `twilio_numbers`
--

CREATE TABLE `twilio_numbers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `number` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `inmates_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prison_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_number` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inmates_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_added` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('Pending','Accepted','Rejected') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `customer_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subscription_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wallet` float(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `inmates_name`, `prison_number`, `doc_number`, `inmates_location`, `area_code`, `name`, `number`, `email`, `amount_added`, `type`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `status`, `customer_id`, `subscription_id`, `wallet`) VALUES
(1, 'inmaates', NULL, '1234567890', 'fcbgnhhgg', '510', 'Admin', '3156468660', 'admin@xsquaretec.com', '25', 'admin', NULL, '$2y$10$V1yLjBaQJCpw0JJAd32S8.08UaY6u5JYBzeYh8Z6.JIwurD7CCic2', NULL, '2021-07-02 07:24:02', '2021-07-02 07:24:06', 'Pending', 'cus_JmFD9F1E0BT0xK', '', 0.00),
(22, 'inmates', NULL, '12345678', NULL, '510', 'user', '3156468660', 'user@xsquaretec.com', '10', 'user', NULL, '$2y$10$zlfj6FEIAvlQ7k2Y/G.NVe4dmexIIlZb062wOz1hplSxwXPPF1lxy', NULL, '2021-07-02 11:44:39', '2021-07-05 08:59:53', 'Pending', 'cus_JmJQ5VGMAj1K0g', 'sub_JmJQGdvzTQXaox', 1176.00);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `calls`
--
ALTER TABLE `calls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `calls_user_id_foreign` (`user_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `ivrs`
--
ALTER TABLE `ivrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `message_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `phone_number`
--
ALTER TABLE `phone_number`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phone_number_user_id_foreign` (`user_id`);

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactionhistories`
--
ALTER TABLE `transactionhistories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaction_historys_user_id_foreign` (`user_id`);

--
-- Indexes for table `twilio_numbers`
--
ALTER TABLE `twilio_numbers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `twilio_numbers_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `calls`
--
ALTER TABLE `calls`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ivrs`
--
ALTER TABLE `ivrs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `phone_number`
--
ALTER TABLE `phone_number`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `places`
--
ALTER TABLE `places`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `prices`
--
ALTER TABLE `prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transactionhistories`
--
ALTER TABLE `transactionhistories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `twilio_numbers`
--
ALTER TABLE `twilio_numbers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `calls`
--
ALTER TABLE `calls`
  ADD CONSTRAINT `calls_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `phone_number`
--
ALTER TABLE `phone_number`
  ADD CONSTRAINT `phone_number_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `transactionhistories`
--
ALTER TABLE `transactionhistories`
  ADD CONSTRAINT `transaction_historys_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `twilio_numbers`
--
ALTER TABLE `twilio_numbers`
  ADD CONSTRAINT `twilio_numbers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
