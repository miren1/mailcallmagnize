<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Classes\TwilioClass;

class SignupValidationController extends Controller
{
    public function checkNameNumber(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inmates_name' => 'required',
            'doc_number' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(['status' => 'false', 'error' => $errors]);
        }else{
            $user = User::where('doc_number', '+1'.$request->doc_number)->first();
            if($user){
                $array2 = array('Number already exists!');
                $array =  [ 'doc_number'=> $array2 ];
                return response()->json(['status' => 'false', 'error' =>  $array]);
            }else{
                return response()->json(['status' => 'success']);
            }
            
        }

    }
    public function areaCodeCheck(Request $request)
    {

            $validator = Validator::make($request->all(), [
                    'area_code' => 'required|digits:3',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                return response()->json(['status'=> 'false', 'error' => $errors]);
            }else{

                    $twilio = new TwilioClass;
                    $array = ["areaCode" => $request->area_code];
                    $dataNumber = $twilio->getAvilableNumber($array);
                    if($dataNumber){
                        $arrayNumber = [];
                        // dd($dataNumber);
                        foreach ($dataNumber as $Number) {
                            $arrayNumber[] = $Number->phoneNumber;
                        }
                        return response()->json(['status' => 'success','data'=>$arrayNumber]);
                    }else{
                        return response()->json(['status'=>'notfound','error'=>'Number Not found']);
                    }
            }
    }

    public function contactDetailsCheck(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(),[

            'contactname' => 'required',
            'contactnumber'=> 'required|digits:10',
            'contactemail'=> 'required|email',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(['status' => 'false', 'error' => $errors]);
        }else{
            $email = User::where('email',$request->contactemail)->first();
            $user = User::where('number', '+1'.$request->contactnumber)->first();
            if($user){
                $array2 = array('Number already exists!');
                $array =  [ 'contactnumber'=> $array2];
                return response()->json(['status' => 'false', 'error' =>  $array]);
            }else if ($email) {
                $array3 = array('Email already exists!');
                $array1 =  [ 'contactemail'=> $array3];
                return response()->json(['status' => 'false', 'error' =>  $array1]);

            }else{
                return response()->json(['status' => 'success']);
            }     
        }
    }
    public function lastquestionCheck(Request $request)
    {
        // dd($request->all());

            $validator = Validator::make($request->all(), [
                    'select' => 'required',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                return response()->json(['status'=> 'false', 'error' => $errors]);
            }else{
                return response()->json(['status' => 'success']);
            }
    }

    public function passwordCheck(Request $request)
    {
        // dd($request->all());
        if($request->prison_number == null)
        {
            $validator = Validator::make($request->all(), [
                'password' => 'required|confirmed|min:6',
            ]);
        }else 
        {
            $validator = Validator::make($request->all(), [
                'prison_number' => 'digits:10',
                'password' => 'required|confirmed|min:6',
            ]);
        }    
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(['status' => 'false', 'error' => $errors]);
        }else{
            if($request->prison_number != null){
                // dd($request->prison_number);
                    $user = User::where('prison_number', '+1'.$request->prison_number)->first();
                    // dd($user);
                    if($user){
                        $array2 = array('Prision Number already exists!');
                        $array =  [ 'prison_number'=> $array2 ];

                        return response()->json(['status' => 'false', 'error' =>  $array]);
                    }else{
                        return response()->json(['status' => 'success']);
                    }
            }
            else {
                return response()->json(['status' => 'success']);
            }
            
        }
    }

}
