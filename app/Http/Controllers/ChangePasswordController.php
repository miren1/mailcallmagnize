<?php

namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('User.changepassword');
    } 

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => 'required|confirmed',
        ]);
        $user = Auth::user();
        
      if(!Hash::check($request->current_password, $user->password)){
        
        return back()->with('error', 'Current password does not match!');
      }

        User::find(Auth::user()->id)->update(['password'=> Hash::make($request->new_password)]);
   
        return redirect()->route('home')->with('success','Password successfully Changed!');
    }

    public function adminindex()
    {
        return view('Admin.changepassword');
    }

    public function adminupdatechangepassword(Request $request)
    {
        // dd($request->all());
         $request->validate([
            // 'current_password' => ['required', new MatchOldPassword],
            'password' => 'required|confirmed',
        ]);
        $user = Auth::user();
        // dd($user);
      if(!Hash::check($request->current_password, $user->password)){
        
        return back()->with('error', 'Current password does not match!');
      }

        User::find(Auth::user()->id)->update(['password'=> Hash::make($request->password)]);
   
        return redirect()->route('home')->with('success','Password successfully Changed!');
    }

}
