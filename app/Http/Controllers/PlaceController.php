<?php

namespace App\Http\Controllers;

use App\Models\Place;
use Illuminate\Http\Request;
use DataTables;
use Facade\FlareClient\Http\Response;
use Brian2694\Toastr\Facades\Toastr;

class PlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Place.index');
    }

    public function placelist(Request $request)
    {
        // if ($request->ajax()) {
            $data = Place::where('status','Accepted')->get();
            // dd($data);
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $edit = route('place.edit', $row->id);
                $btn1 ='<a href="'.$edit.'"><button type="button" class="btn btn-success waves-effect waves-light" title="Edit" tabindex="0" data-plugin="tippy" data-tippy-placement="top"><i class="fe-edit"></i></button></a>&nbsp;&nbsp;';
                $btn2 ='<button type="button" class="btn btn-danger waves-effect waves-light" title="Delete" tabindex="0" data-plugin="tippy" data-tippy-placement="top"><i class="fe-trash-2 servideletebtn" data-id="'.$row->id.'"></i></button></button>';
                return $btn1.''.$btn2;
            })
            ->rawColumns(['action'])
            ->make(true);
        // }
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Place.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd('+1'.$request->prison_number);
        $request->validate([

            'prison_type'    =>      'required',
            'prison_number'  =>      'required|numeric|digits:10|unique:places',
            'prison_name'    =>      'required',
            'location'       =>      'required'

        ],['prison_number.digits'=>'This Prison Phone Number must be 10 digits.']);


        $add = new Place;
        $add->prison_type = $request->prison_type;
        $add->prison_number = '+1'.$request->prison_number;
        $add->prison_name = $request->prison_name;
        $add->location = $request->location;
        $add->status = 'Accepted';
        $add->save();
        toastr()->success('Success Message');
        return redirect()->route('place');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function show(Place $place)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $place_edit = Place::find($id);
        return view('Place.edit',compact('place_edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $request->validate([

            'prison_type'    =>      'required',
            'prison_number'  =>      'required|numeric|digits:10',
            'prison_name'    =>      'required',
            'location'       =>      'required'

        ],[
            'prison_number.digits'=>'This Prison Phone Number must be 10 digits',
        ]);

        $upd = Place::find($id);
        $upd->prison_type = $request->prison_type;
        $upd->prison_number = '+1'.$request->prison_number;
        $upd->prison_name = $request->prison_name;
        $upd->location = $request->location;
        $upd->save();

        return redirect()->route('place')->with('success','Place Information Updated Successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Place  $place
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $dlt = Place::find($id);
        $dlt->delete();

        return Response()->json(['success'=>'Your record will be deleted successfully']);
    }

    public function pendingnumbers(Request $request)
    {
        // if ($request->ajax()) {
                
            $pendingdata = Place::where('status','Pending')->get();

            return DataTables::of($pendingdata)
                    ->addIndexColumn()
                     ->addColumn('action', function ($row) {
                       $btn1 ='<button type="button" class="btn btn-success serviacceptbtn waves-effect waves-light" data-id="'.$row->id.'" data-type="Accepted" title="Accept" tabindex="0" data-plugin="tippy" data-tippy-placement="top"><i class="fe-check-square"></i></button>&nbsp;&nbsp;';
                         $btn2 ='<button type="button" class="btn btn-danger servirejecttbtn waves-effect waves-light" data-id="'.$row->id.'" data-type="Rejected" title="Reject" tabindex="0" data-plugin="tippy" data-tippy-placement="top"><i class="fe-x-square"></i></button>';
                        
                         return $btn1 .''.$btn2;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

        // }
    }
     public function prison_numberAccept($id,$type,Request $request)
    {
        $status = $type;
        // dd($status);
        $add = Place::find($id);
        $add->status = $status;
        $add->save();
        return response()->json(['success'=>'User Successfully']);
    }


}
