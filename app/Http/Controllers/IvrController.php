<?php

namespace App\Http\Controllers;

use App\Models\Ivr;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Collection;

class IvrController extends Controller
{
    public function index()
    {
        return view('IVR.index');
    }

    public function ivr_list(Request $request)
    {
        $arrayIvr = Ivr::whereIn('ivr_day',['default',$request->receipt_no])->get()->toArray();
        return DataTables::of($arrayIvr)
               ->addIndexColumn()
               ->addColumn('action', function($row) {
                
                // dd($row['id']);
                if($row['status'] != 'default'){
                  $edit = route('Ivr.edit', $row['id']);
                  $btn1 ='<a href="'.$edit.'"><button type="button" class="btn btn-success waves-effect waves-light" title="Edit" tabindex="0" data-plugin="tippy" data-tippy-placement="top"><i class="fe-edit"></i></button></a>&nbsp;&nbsp;';
                  $btn2 ='<button type="button" class="btn btn-danger servideletebtn waves-effect waves-light" title="Delete" tabindex="0" data-plugin="tippy" data-id="'.$row['id'].'" data-tippy-placement="top"><i class="fe-trash-2"></i></button></button>';
                  return $btn1.''.$btn2;
                }else{
                   $edit = route('Ivr.edit', $row['id']);
                   $btn1 ='<a href="'.$edit.'"><button type="button" class="btn btn-success waves-effect waves-light" title="Edit" tabindex="0" data-plugin="tippy" data-tippy-placement="top"><i class="fe-edit"></i></button></a>&nbsp;&nbsp;';
                   return $btn1;
                }
               })
               ->editColumn('message',function($row){
                   if($row['type'] == 'say')
                   {
                    $data = $row['say_message'];
                    return  mb_strimwidth($data,0,25,'......');
                   }elseif($row['type'] == 'play')
                   {
                        $newdata = '<audio controls style="width: 110px;">
                                        <source src="'.url('uploads/'.$row['medianame']).'" type="audio/mpeg">
                                      </audio>';
                       return $newdata;
                   }
               })
               ->editColumn('title',function($row){
                   $title = $row['title'];
                   return mb_strimwidth($title,0,35,'......');
               })
               ->rawColumns(['action','message','title'])
               ->make(true);
        
    }

    public function addIvr()
    {
        return view('IVR.add');
    }
    public function ivrstore(Request $request)
    {
        // dd($request->all());
        // $request->validate([
        //     'file' => 'required|mimes:pdf,xlx,csv|max:2048',
        // ]);

        if($request->say_message != null)
        {
            $data = new Ivr;
            $data->title = $request->title;
            $data->type = $request->type;
            $data->say_message = $request->say_message;
            $data->status = $request->status;
            $data->ivr_day = $request->ivr_day;

            $data->save();

            return redirect()->route('ivr')->with('success','IVR Create Successfully!');



        }
        elseif($request->medianame != null)
        {
            $request->validate([

                'medianame' => 'required|mimes:application/octet-stream,audio/mpeg,mpga,mp3,wav|size:2000',

            ],
            [
                'medianame.mimes'=>'Please Uploads only mp3 and max 2MB!',
            ],
            [
                 'medianame.size'=> 'Please Uploads only mp3 and max 2MB!'
            ]
        );
            // dd($request->medianame);

        $fileName = time().'.'.str_replace(' ', '_', $request->file('medianame')->getClientOriginalName());
// dd($fileName);
               $newdata =new Ivr;
               $newdata->title = $request->title;
               $newdata->type = $request->type;
               $newdata->medianame = $fileName;
               $newdata->status = $request->status;
               $newdata->ivr_day = $request->ivr_day;

                $file = $request->file('medianame');

               $destinationPath = 'uploads';

               $file->move($destinationPath,$fileName);
               $newdata->save();

               return redirect()->route('ivr')->with('success','IVR Create Successfully!');;
        }
    }

    public function editIvr($id)
    {
     $Ivrdata = Ivr::find($id);
     return view('IVR.edit',compact('Ivrdata'));
    }

    public function ivrupdate($id,Request $request)
    {
      // dd($request->all());
        $Ivr = Ivr::find($id);

         if($request->type == 'say')
        {
            $Ivr->title = $request->title;
            $Ivr->type = $request->type;
            $Ivr->say_message = $request->say_message;
            if(isset($request->status)){
             $Ivr->status = $request->status;
            }
            $Ivr->ivr_day = $request->ivr_day;
            $Ivr->save();
            return redirect()->route('ivr')->with('success','IVR Update Successfully!');
        }
        elseif($request->type == 'play')
        {
            // dd($request->all());
            // dd('dfg',$Ivr);
            if(isset($request->medianame)){
                $request->validate([
                        'medianame' => 'required|mimes:application/octet-stream,audio/mpeg,mpga,mp3,wav|size:2000',
                    ],
                    [
                        'medianame.mimes'=>'Please Uploads only mp3 and max 2MB!',
                    ],
                    [
                         'medianame.size'=> 'Please Uploads only mp3 and max 2MB!'
                    ]
                );
                $fileName = time().'.'.str_replace(' ', '_', $request->file('medianame')->getClientOriginalName());
                $Ivr->medianame = $fileName;
                $file = $request->file('medianame');
                $destinationPath = 'uploads';
                $file->move($destinationPath,$fileName);
            }else{
                $Ivr->medianame = $Ivr->medianame;
            }
            
            // dd($request->medianame);
            
            $Ivr->title = $request->title;
            $Ivr->type = $request->type;
            if(isset($request->status)){
                $Ivr->status = $request->status;
            }
            $Ivr->ivr_day = $request->ivr_day;
          
            $Ivr->save();

           return redirect()->route('ivr')->with('success','IVR Update Successfully!');
        }
    }

    public function Ivrdelete($id)
    {
        $delete = Ivr::find($id);
        // dd($delete);
        $delete->delete();

        return Response()->json(['success'=>'Record Successfully Deleted']);

    }

}
