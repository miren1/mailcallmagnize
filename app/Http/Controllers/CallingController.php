<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\TwiML\VoiceResponse;
use App\Models\Calls;
use App\Models\TwilioNumber;
use App\Models\User;
use App\Models\Price;
use App\Models\Ivr;
use App\Models\Place;
use Twilio\Rest\Client;
use App\Models\Recording;


class CallingController extends Controller
{
    public function reciveSms(Request $request)
    {
        $twilioNumber = TwilioNumber::where('number', $request->To)->with('user')->first();
        if($twilioNumber){
            $NumMedia = (int)$request->NumMedia;
            $arrMedia = [];
            $recordingName = null;
            for ($i=0; $i < $NumMedia; $i++) {
                $arrMedia[] = $_REQUEST["MediaUrl$i"];
                $image = file_get_contents($_REQUEST["MediaUrl$i"]);
                $recordingName = $request->MessageSid.$i.'.mp3';
                file_put_contents(public_path('recording/'.$recordingName), $image);
            }
            $price = Price::first();

            $recording2 = new Recording;
            if($price){
                $call->voice_charge = $price->sms;  
            }

            $doc_number = Place::where('prison_number', $request->From)->first();
            if($doc_number){
                $recording2->call_type = 'inmate_call'; 
            }else{
                $recording2->call_type = 'user_call'; 
            }

            $recording2->sid = $request->MessageSid;
            $recording2->type = 'message';
            $recording2->message = $request->Body;
            $recording2->call_type = 'inmate_call';
            $recording2->play_status = 'false';
            $recording2->voice_for = 'all';
            $recording2->status = 'received';
            $recording2->voice_charge = $price->sms; 
            $recording2->twilio_number = $request->To;
            $recording2->phone_number = $request->From;
            $recording2->user_id = $twilioNumber->user_id;
            $recording2->save();


        }
    }
    public function incomingcall(Request $request)
    {
        $response = new VoiceResponse();

        $twilioNumber = TwilioNumber::where('number', $request->To)->with('user')->first();
        $doc_number = Place::where('prison_number', $request->From)->first();
        if($twilioNumber){
            if((float)$twilioNumber->user->wallet > 0){
                $getVoice = Price::first();
                if($getVoice && $getVoice->voice_type){
                    $sayVoice = $getVoice->voice_type;
                }else{
                    $sayVoice = 'Polly.Joanna';
                }
                $call = Calls::where('sid',$request->CallSid)->first();
                if(!$call){
                    $call = new Calls;
                    //$call->call_type = ($twilioNumber->user->doc_number == $request->From) ? 'inmate_call' : 'user_call';
                    $call->call_type = ($doc_number) ? 'inmate_call' : 'user_call';
                    $call->sid = $request->CallSid;
                    $call->twilio_number = $request->To;
                    $call->phone_number = $request->From;
                    $call->user_id = $twilioNumber->user_id;
                    $call->type = 'incoming';

                    $is_save = $call->save();
                }
                //dd($call);
                $gather = $response->gather(['action' => url('/api/gather-url?user_id='.$twilioNumber->user_id.'&gather_action=first&call_type='.$call->call_type), 'method' => 'get' , 'speechTimeout' => 15]);
                $say = $gather->say('Hello, ', ['voice' => $sayVoice]);
                $say->break_(['strength' => 'x-weak', 'time' => '1000ms']);
                $say->prosody('Press 1 for listen new previous voice mail, Press 2 for record new voice mail, ', ['rate' => '85%']);
                $ivrs = false;
                if($call->call_type == 'inmate_call'){
                    //$say->append(' Press 3 for listen new previous message, Press 4 for send new message', ['voice' => 'Polly.Joanna']);
                    $today = date('l');
                    $ivrs = Ivr::where('status', 'enabled')->whereIn('ivr_day',['default',$today])->get();
                    foreach($ivrs as $key => $ivr){
                         $say->prosody(' Press '.($key + 3).' for '.$ivr->title, ['rate' => '85%']);
                         $say->break_(['strength' => 'x-weak', 'time' => '1000ms']);
                    }
                } 
                $say->prosody('We didn\'t receive any input. Please enter any input!', ['rate' => '85%']);
                $say->prosody('Press 1 for listen new previous voice mail, Press 2 for record new voice mail, ', ['rate' => '85%']);
                if($call->call_type == 'inmate_call' && $ivrs){
                    //$say->append(' Press 3 for listen new previous message, Press 4 for send new message', ['voice' => 'Polly.Joanna']);
                    // $ivrs = Ivr::where('status', 'enabled')->get();
                    foreach($ivrs as $key => $ivr){
                         $say->prosody(' Press '.($key + 3).' for '.$ivr->title, ['rate' => '85%']);
                         $say->break_(['strength' => 'x-weak', 'time' => '1000ms']);
                    }
                } 

            }
        }
        return response($response, 200)->header('Content-Type', 'text/xml');
    } 

    public function MessageGather(Request $request)
    {
        $response = new VoiceResponse();
        $arrCall = [
            'message_type' => 'message',
            'message' => $request->SpeechResult
        ];
        $price = Price::first();
        if($price){
            $arrCall['sms_charge'] = $price->sms;
        }
        Calls::wherer('sid', $request->CallSid)->update($arrCall);
        $response->say('Your message send successfully!', ['voice' => 'Polly.Joanna']);
        return response($response, 200)->header('Content-Type', 'text/xml');
    }


    public function incominggather(Request $request)
    {
        // dd($_REQUEST);
        $response = new VoiceResponse();
        if($request->call_type == 'inmate_call'){
            $call_type = 'user_call';
        }else{
            $call_type = 'inmate_call';
        }
        $getVoice = Price::first();
        if($getVoice && $getVoice->voice_type){
            $sayVoice = $getVoice->voice_type;
        }else{
            $sayVoice = 'Polly.Joanna';
        }

        if($request->Digits || $request->SpeechResult){
            
            switch ($request->gather_action) {
                case "first": // first action 
                    //Press 1 for listen previous voice mail, Press 2 for record new voice mail
                    switch ($request->Digits) {
                        case '1': // first action press 1 - previous voice mail
                            if($request->call_type == 'user_call'){
                                $voicemail = Recording::where('call_type',$call_type)
                                    ->orderBy('id','DESC')
                                    ->where('user_id',$request->user_id)
                                    ->where('voicemail_status','false')
                                    ->where('play_status','true')
                                    ->where('voicemail_status','false')
                                    ->where(function($query) use ($request){
                                         $query->where('voice_for', 'all');
                                         $query->orWhere('voice_for', $request->From);
                                    })
                                    ->first();
                                //dd($voicemail);
                            }else{
                                $voicemail = Recording::where('call_type',$call_type)
                                    ->orderBy('id','DESC')
                                    ->where('user_id',$request->user_id)
                                    ->where('voicemail_status','false')
                                    ->where('play_status','true')
                                    //->Call()
                                    ->where('voicemail_status','false')
                                    ->where(function($query) use ($request){
                                         $query->where('voice_for', 'all');
                                         $query->orWhere('voice_for', $request->From);
                                    })
                                    //->MessageVoice()
                                    ->first();
                            }
                            //dd($voicemail);
                            if($voicemail){
                                // new voice mail found
                                if($request->call_type == 'user_call'){
                                    $voicemail2 = Recording::where('call_type',$call_type)
                                        ->orderBy('id','DESC')
                                        ->where('play_status','true')
                                        ->where('listen_number',null)
                                        ->where('voicemail_status','false')
                                        //->Call()
                                        ->where('id','<=',$voicemail->id)
                                        ->where('user_id',$request->user_id)
                                        //->MessageVoice()
                                        ->where(function($query) use ($request){
                                             $query->where('voice_for', 'all');
                                             $query->orWhere('voice_for', $request->From);
                                        })
                                        ->first();

                                    }else{
                                        $voicemail2 = Recording::where('call_type',$call_type)
                                        ->orderBy('id','DESC')
                                        ->where('play_status','true')
                                        ->where('listen_number',null)
                                        ->where('voicemail_status','false')
                                        //->Call()
                                        ->where('id','<=',$voicemail->id)
                                        ->where('user_id',$request->user_id)
                                        //->MessageVoice()
                                        ->first();
                                    }
                                //dd($voicemail2);
                                if($voicemail2){
                                    if($voicemail2->message_type == 'message'){
                                        $say = $response->say('this is text message. ', ['voice' => $sayVoice]);
                                        $say->prosody($voicemail2->message, ['rate' => '85%']);
                                        //$response->say($voicemail2->message, ['voice' => 'Polly.Joanna']);
                                    }else{
                                        $response->say('this is voicemail. ',['voice' => $sayVoice]);
                                        $response->play(url($voicemail2->message));
                                    }  
                                    if($request->call_type == 'inmate_call'){
                                        $gather = $response->gather([
                                            'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=message&call_type='.$request->call_type.'&next_gather_action=first&number='.$voicemail2->id), 
                                            'method' => 'get',
                                            'speechTimeout' => 15
                                        ]);
                                        $say = $gather->say('Press ', ['voice' => $sayVoice]); 
                                        $say->prosody(' 1 for send voicemail to this number, Press 2 for send message to this number, ', ['rate' => '85%']);
                                    } 
                                    $voicemail2->listen_number = $request->From;
                                    $voicemail2->voicemail_status = 'true';
                                    $voicemail2->save();
                                    $gather = $response->gather([
                                        'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=first&call_type='.$request->call_type),
                                         'speechTimeout' => 15,
                                         'method' => 'get',
                                     ]);
                                    $say = $gather->say('Press ', ['voice' => $sayVoice]); 
                                    $say->prosody(' 1 for listen more new previous voice mail, Press 2 for record new voice mail, ', ['rate' => '85%']);
                                }else{
                                    $gather = $response->gather([
                                        'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=all_voice_not_found&call_type='.$request->call_type),
                                        'method' => 'get',
                                         'speechTimeout' => 15
                                     ] );
                                    $say = $gather->say('No ', ['voice' => $sayVoice]);

                                    $say->prosody(' voice mail found to listen', ['rate' => '85%']);
                                    $response->redirect(url('/api/incoming-call'), ['method' => 'POST']);
                                }
                            }else{
                                if($request->call_type == 'user_call'){
                                    $voicemail = Recording::where('call_type',$call_type)
                                        ->orderBy('id','DESC')
                                        ->where('play_status','true')
                                        ->where('user_id',$request->user_id)
                                        //->Call()
                                        ->where('voicemail_status','false')
                                        //->MessageVoice()
                                        ->where(function($query) use ($request){
                                             $query->where('voice_for', 'all');
                                             $query->orWhere('voice_for', $request->From);
                                        })
                                        ->first();
                                    }else{
                                         $voicemail = Recording::where('call_type',$call_type)
                                            ->orderBy('id','DESC')
                                            ->where('play_status','true')
                                            ->where('user_id',$request->user_id)
                                            //->Call()
                                            ->where('voicemail_status','false')
                                            //->MessageVoice()
                                            ->first();
                                    }
                                if($voicemail){
                                    if($voicemail->type == 'message'){
                                        $say = $response->say('this is text message. ',['voice' => $sayVoice]);
                                        $say->prosody($voicemail->message, ['rate' => '85%']);
                                        //$response->say($voicemail->message,['voice' => 'Polly.Joanna']);
                                    }else{
                                        $response->say('this is voicemail. ',['voice' => $sayVoice]);
                                        $response->play(url($voicemail->message));
                                    }  

                                    if($request->call_type == 'inmate_call'){
                                        $gather = $response->gather([
                                            'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=message&call_type='.$request->call_type.'&next_gather_action=first&number='.$voicemail->id),
                                             'method' => 'get',
                                             'speechTimeout' => 15]);
                                        $say = $gather->say('Press ', ['voice' => $sayVoice]); 
                                        $say->prosody(' 1 for send voicemail to this number, Press 2 for send message to this number, ', ['rate' => '85%']);
                                    } 

                                   //$response->play(url($voicemail->voicemail));
                                    $voicemail->listen_number = $request->From;
                                    $voicemail->voicemail_status = 'true';
                                    $voicemail->save(); 
                                    $gather = $response->gather([
                                        'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=first&call_type='.$request->call_type),
                                          'method' => 'get',
                                         'speechTimeout' => 15]);
                                    $say = $gather->say('Press ', ['voice' => $sayVoice]); 
                                    $say->prosody(' 1 for listen more new previous voice mail, Press 2 for record new voice mail, ', ['rate' => '85%']);
                                    // new voice mail not found
                                }else{
                                    $gather = $response->gather([
                                        'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=voice_not_found&call_type='.$request->call_type),
                                        'method' => 'get',
                                        'speechTimeout' => 15]);
                                    $say = $gather->say('No ', ['voice' => $sayVoice]);
                                    $say->prosody(' voice mail found to listen, Press 1 for listen old voice mail, Press 2 for record new voice mail, ', ['rate' => '85%']);
                                }
                            }
                            if($request->call_type == 'inmate_call'){
                                //$say->append(' Press 3 for listen new previous message, Press 4 for send new message', ['voice' => 'Polly.Joanna']);
                                $today = date('l');
                                $ivrs = Ivr::where('status', 'enabled')->whereIn('ivr_day',['default',$today])->get();
                                foreach($ivrs as $key => $ivr){
                                    $say->prosody('Press '.($key + 3).' for '.$ivr->title, ['rate' => '85%']);
                                    $say->break_(['strength' => 'x-weak', 'time' => '1000ms']);
                                }
                            }  
                            break; 
                        case '2': // first action press 2 - record voice mail
                            $response->say('Please leave a message at the beep. Press the star key when finished.', ['voice' => $sayVoice]);
                            $response->record([
                                'action' => url('/api/record-url?gather_action=start_over&call_type='.$request->call_type),
                                'method' => 'get',
                                'finishOnKey' => '*'
                            ]);
                            break; 
                    }
                    if($request->Digits != '1' && $request->Digits != '2' && $request->call_type == 'inmate_call'){
                        $today = date('l');
                        $ivrs = Ivr::where('status', 'enabled')->whereIn('ivr_day',['default',$today])->get();
                        $digit = $request->Digits - 3;
                        if(count($ivrs) + 2 >= $request->Digits){
                            if($ivrs[$digit]->type == 'say'){
                                $say = $response->say('a',['voice' => $sayVoice]);
                                $say->prosody($ivrs[$digit]->say_message, ['rate' => '85%']);
                            }else{
                                $response->play(url('uploads').'/'.$ivrs[$digit]->medianame);
                            }
                        }
                        $response->redirect(url('/api/incoming-call'), ['method' => 'POST']);
                        
                    }   
                break;
                case "voice_not_found":
                    // voice mail not found event 
                    switch($request->Digits) {
                        case '1': // voice_not_found action press 1 - old previous voice mail
                            if($request->call_type == 'inmate_call'){
                                $call_type = 'user_call';
                            }else{
                                $call_type = 'inmate_call';
                            }
                            if($request->call_type == 'user_call'){
                                $voicemail = Recording::where('call_type',$call_type)
                                    ->orderBy('id','DESC')
                                    ->where('play_status','true')
                                    ->where('user_id',$request->user_id)
                                    ->where('listen_number', $request->From)
                                    //->MessageVoice()
                                    ->where(function($query) use ($request){
                                         $query->where('voice_for', 'all');
                                         $query->orWhere('voice_for', $request->From);
                                    })
                                    ->first();
                            }else{
                                $voicemail = Recording::where('call_type',$call_type)
                                    ->orderBy('id','DESC')
                                    ->where('play_status','true')
                                    ->where('user_id',$request->user_id)
                                    ->where('listen_number', $request->From)
                                    //->MessageVoice()
                                    ->first();
                            }
                            if($voicemail){
                                if($request->call_type == 'user_call'){
                                    $voicemail2 = Recording::where('call_type',$call_type)
                                        ->orderBy('id','DESC')
                                        ->where('play_status','true')
                                        ->where('listen_number',null)
                                        //->where('listen_number','<>',$request->From)
                                        ->where('id','<=',$voicemail->id)
                                        ->where('user_id',$request->user_id)
                                        //->MessageVoice()
                                        ->where(function($query) use ($request){
                                             $query->where('voice_for', 'all');
                                             $query->orWhere('voice_for', $request->From);
                                        })
                                        ->first();
                                }else{
                                    $voicemail2 = Recording::where('call_type',$call_type)
                                    ->orderBy('id','DESC')
                                    ->where('play_status','true')
                                    ->where('listen_number',null)
                                    //->where('listen_number','<>',$request->From)
                                    ->where('id','<=',$voicemail->id)
                                    ->where('user_id',$request->user_id)
                                    //->MessageVoice()
                                    ->first();
                                }
                                if($voicemail2){
                                    if($voicemail2->message_type == 'message'){
                                        $response->say('this is text message. ', ['voice' => $sayVoice]);
                                        $response->say($voicemail2->message);
                                    }else{
                                        $response->say('this is voicemail. ',['voice' => $sayVoice]);
                                        $response->play(url($voicemail2->message));
                                    } 

                                    if($request->call_type == 'inmate_call'){
                                        $gather = $response->gather([
                                            'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=message&call_type='.$request->call_type.'&next_gather_action=voice_not_found&number='.$voicemail2->id), 
                                            'method' => 'get',
                                            'speechTimeout' => 15]);
                                        $say = $gather->say('Press ', ['voice' => $sayVoice]); 
                                        $say->prosody(' 1 for send voicemail to this number, Press 2 for send message to this number, ', ['rate' => '85%']);
                                    }


                                    //$response->play(url($voicemail->voicemail));
                                    $voicemail2->listen_number = $request->From;
                                    $voicemail2->save();

                                    $gather = $response->gather([
                                        'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=voice_not_found&call_type='.$request->call_type),
                                        'method' => 'get',
                                        'speechTimeout' => 15]);
                                    $say = $gather->say('Press ', ['voice' => $sayVoice]);
                                    $say->prosody(' 1 for listen more previous voice mail, Press 2 for record new voice mail', ['rate' => '85%']);
                                }else{
                                    $gather = $response->gather([
                                        'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=all_voice_not_found&call_type='.$request->call_type), 
                                        'method' => 'get',
                                        'speechTimeout' => 15]);
                                    $say = $gather->say('No ', ['voice' => $sayVoice]);
                                    $say->prosody('voice mail found to listen,', ['rate' => '85%']);
                                    $response->redirect(url('/api/incoming-call'), ['method' => 'POST']);
                                    
                                }
                            }else{
                                if($request->call_type == 'user_call'){
                                    $voicemail = Recording::where('call_type',$call_type)
                                        ->orderBy('id','DESC')
                                        ->where('play_status','true')
                                        ->where('user_id',$request->user_id)
                                        //->MessageVoice()
                                        ->where(function($query) use ($request){
                                             $query->where('voice_for', 'all');
                                             $query->orWhere('voice_for', $request->From);
                                        })
                                        ->first(); 
                                }else{
                                    $voicemail = Recording::where('call_type',$call_type)
                                        ->orderBy('id','DESC')
                                        ->where('play_status','true')
                                        ->where('user_id',$request->user_id)
                                        //->MessageVoice()
                                        ->first(); 
                                }
                                if($voicemail){
                                    if($voicemail->type == 'message'){
                                        $say = $response->say('this is text message. ', ['voice' => $sayVoice]);
                                        $say->prosody($response->say($voicemail->message), ['rate' => '85%']);
                                        
                                    }else{
                                        $response->say('this is voicemail. ', ['voice' => $sayVoice]);
                                        $response->play(url($voicemail->message));
                                    } 

                                    if($request->call_type == 'inmate_call'){
                                        $gather = $response->gather([
                                            'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=message&call_type='.$request->call_type.'&next_gather_action=voice_not_found&number='.$voicemail->id),
                                            'method' => 'get',
                                             'speechTimeout' => 15]);
                                        $say = $gather->say('Press ', ['voice' => $sayVoice]); 
                                        $say->prosody(' 1 for send voicemail to this number, Press 2 for send message to this number, ', ['rate' => '85%']);
                                    }

                                    //$response->play(url($voicemail->voicemail));
                                    $voicemail->listen_number = $request->From;
                                    $voicemail->save();

                                    $gather = $response->gather([
                                        'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=voice_not_found&call_type='.$request->call_type), 
                                        'method' => 'get',
                                        'speechTimeout' => 15]);
                                    $say = $gather->say('Press ', ['voice' => $sayVoice]);
                                    $say->prosody(' 1 for listen more new previous voice mail, Press 2 for record new voice mail', ['rate' => '85%']);
                                    //
                                }else{
                                    $gather = $response->gather([
                                        'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=all_voice_not_found&call_type='.$request->call_type), 
                                        'method' => 'get',
                                        'speechTimeout' => 15]);
                                    //$say = $gather->say('No ', ['voice' => 'Polly.Joanna']);
                                    $say = $gather->say('No ', ['voice' => $sayVoice]);
                                    $say->prosody(' voice mail found to listen,', ['rate' => '85%']);
                                    $response->redirect(url('/api/incoming-call'), ['method' => 'POST']);
                                }
                            }
                            if($request->call_type == 'inmate_call'){
                                //$say->append(' Press 3 for listen new previous message, Press 4 for send new message', ['voice' => 'Polly.Joanna']);
                                $today = date('l');
                                $ivrs = Ivr::where('status', 'enabled')->whereIn('ivr_day',['default',$today])->get();
                                foreach($ivrs as $key => $ivr){
                                    $say->append('Press '.($key + 3).' for '.$ivr->title, ['voice' => $sayVoice]);
                                    $say->break_(['strength' => 'x-weak', 'time' => '1000ms']);

                                }
                                $response->redirect(url('/api/incoming-call'), ['method' => 'POST']);
                            } 
                            break; 
                        case '2': // first action press 2 - record voice mail
                            $response->say('Please leave a message at the beep. Press the star key when finished.', ['voice' => $sayVoice]);
                            $response->record([
                                'action' => url('/api/record-url?gather_action=start_over&call_type='.$request->call_type), 
                                'method' => 'get',
                                'finishOnKey' => '*'
                            ]);
                            break;
                        default:
                    }

                    if($request->Digits != '1' && $request->Digits != '2' && $request->call_type == 'inmate_call'){
                        $today = date('l');
                        $ivrs = Ivr::where('status', 'enabled')->whereIn('ivr_day',['default',$today])->get();
                        $digit = $request->Digits - 3;
                        if(count($ivrs) + 2 >= $request->Digits){
                            if($ivrs[$digit]->type == 'say'){
                                $say = $response->say('a',['voice' => $sayVoice]);
                                $say->prosody($ivrs[$digit]->say_message, ['rate' => '85%']);
                            }else{
                                $response->play(url('uploads').'/'.$ivrs[$digit]->medianame);
                            }
                        }
                        $response->redirect(url('/api/incoming-call'), ['method' => 'POST']);
                        
                    } 
                    
                break;

                case "all_voice_not_found":
                    switch ($request->Digits) {
                        case '2': // first action
                            $response->say('Please leave a message at the beep. Press the star key when finished.', ['voice' => $sayVoice]);
                            $response->record([
                                'action' => url('/api/record-url?gather_action=start_over&call_type='.$request->call_type), 
                                'method' => 'get',
                                'finishOnKey' => '*'
                            
                            ]);
                        break;
                    }
                    if($request->Digits != '1' && $request->Digits != '2' && $request->call_type == 'inmate_call'){
                        $today = date('l');
                        $ivrs = Ivr::where('status', 'enabled')->whereIn('ivr_day',['default',$today])->get();
                        $digit = $request->Digits - 3;
                        if(count($ivrs) + 2 >= $request->Digits){
                            if($ivrs[$digit]->type == 'say'){
                                $say = $response->say('a',['voice' => $sayVoice]);
                                $say->prosody($ivrs[$digit]->say_message, ['rate' => '85%']);
                            }else{
                                $response->play(url('uploads').'/'.$ivrs[$digit]->medianame);
                            }
                        }
                        $response->redirect(url('/api/incoming-call'), ['method' => 'POST']);
                        
                    } 
                break;

                case "message":
                    switch ($request->Digits) {
                        case '1': // voicemail action
                            //$response = new VoiceResponse();
                            $response->say('Please leave a message at the beep. Press the star key when finished.', ['voice' => $sayVoice]);
                            $response->record([
                                'action' => url('/api/record-url?user_id='.$request->user_id.'&gather_action='.$request->next_gather_action.'&call_type='.$request->call_type.'&send_sms=true&number='.$request->number.'&call_type='.$request->call_type), 
                                'method' => 'get',
                                'finishOnKey' => '*'
                            ]);
                            //

                           /*$response->redirect(url('/api/gather-url?user_id='.$request->user_id.'&gather_action='.$request->next_gather_action.'&call_type='.$request->call_type.'&Digits=1'), ['method' => 'POST']);*/
                        break;
                        case '2': // message action
                            //input="speech"
                            $response = new VoiceResponse();
                            $gather = $response->gather([
                                'action' => url('/api/gather-url?user_id='.$request->user_id.'&gather_action=get_message&call_type='.$request->call_type.'&next_gather_action='.$request->next_gather_action.'&number='.$request->number),
                                 'method' => 'get',
                                 'speechTimeout' => 15, 'input' => 'speech']);
                            $say = $gather->say('Please ', ['voice' => $sayVoice]); 
                            $say->prosody(' say your message to send, ', ['rate' => '85%']);
                        break;
                    }
                break;
                case "get_message":
                    $response = new VoiceResponse();
                    $smsCaller = Recording::where('id',$request->number)->first();
                    $twilio = new Client(env('TWILIO_SID'), env('TWILIO_TOKEN'));
                    $sendArray = array(
                                        "from" => $smsCaller->twilio_number,
                                        "body" => $request->SpeechResult,
                                        "StatusCallback" => url('api/get-sms-status-inbox'),    
                                    );
                    $message = $twilio->messages
                                    ->create($smsCaller->phone_number, 
                                         $sendArray   
                                    );
                    $recording = new Recording;
                    $recording->sid = $message->sid;
                    $recording->type = 'message';
                    $recording->message = $request->SpeechResult;
                    $recording->call_type = 'inmate_call';
                    $recording->voice_for = $smsCaller->phone_number;
                    $recording->status = 'sent';
                    $recording->twilio_number = $smsCaller->twilio_number;
                    $recording->phone_number = $request->From;
                    $recording->user_id = $smsCaller->user_id;
                    $price = Price::first();
                    if($price){
                        $recording->voice_charge = $price->sms; 
                    }
                    $recording->save();
                    $say = $response->say('Your ', ['voice' => $sayVoice]); 
                    $say->prosody(' message recorded successfully, Thank you ', ['rate' => '85%']);
                    $response->redirect(url('/api/incoming-call'), ['method' => 'POST']);
                break;
                
                default:
                   //echo "Your favorite color is neither red, blue, nor green!";
            } 
        }
        return response($response, 200)->header('Content-Type', 'text/xml');
    }



    public function incomingrecord(Request $request)
    {
        //dd($request->all());
        $response = new VoiceResponse();

        $client = new \GuzzleHttp\Client();
        sleep(2);
        $res = $client->get($request->RecordingUrl);
        $image = (string) $res->getBody();
        //$image = file_get_contents($request->RecordingUrl);
        $recordingName = 'recording/'.$request->RecordingSid.'.wav';
        file_put_contents(public_path($recordingName), $image);

        $recording = new Recording;
        $recording->sid = $request->CallSid;
        $recording->rsid = $request->RecordingSid;
        $recording->type = 'voice';
        $recording->message = $recordingName;
        $recording->call_type = $request->call_type;
        $recording->voice_for = 'all';
        $recording->twilio_number = $request->To;
        $recording->phone_number = $request->From;
        $price = Price::first();
        if($price){
            $recording->voice_charge = $price->voice; 
        }
        
        $twilioNumber = TwilioNumber::where('number', $request->To)->first();
        if($twilioNumber){
            $user = User::find($twilioNumber->user_id);
            if($user){
                $user->wallet = $user->wallet - (float)$price->voice;
                $user->save();
            }
            $recording->user_id = $twilioNumber->user_id;
        }
        $recording->save();
        if(isset($request->send_sms)){
            $smsCaller = Recording::where('id',$request->number)->first();
            $recording->voice_for = $smsCaller->phone_number;
            $recording->save();

            $twilio = new Client(env('TWILIO_SID'), env('TWILIO_TOKEN'));
            $sendArray = array(
                                        "from" => $smsCaller->twilio_number,
                                        "body" => 'You have new voicemail '.url($recordingName),
                                        "StatusCallback" => url('api/get-sms-status-inbox'),    
                                    );
            $message = $twilio->messages
                            ->create($smsCaller->phone_number, 
                                 $sendArray   
                            );
            $recording2 = new Recording;
            $recording2->sid = $message->sid;
            $recording2->type = 'message';
            $recording2->message = 'You have new voicemail '.url($recordingName);
            $recording2->call_type = 'inmate_call';
            $recording2->play_status = 'false';
            $recording2->voice_for = 'all';
            $recording2->voice_charge = $price->sms; 
            $recording2->twilio_number = $smsCaller->twilio_number;
            $recording2->phone_number = $request->From;
            $recording2->user_id = $smsCaller->user_id;
            $recording2->save();

        }
        if($request->gather_action){
            if($price && $price->voice_type){
                $sayVoice = $price->voice_type;
            }else{
                $sayVoice = 'Polly.Joanna';
            }
            $say = $response->say('Your ', ['voice' => $sayVoice]); 
            $say->prosody(' message recorded successfully, Thank you ', ['rate' => '85%']);
            $response->redirect(url('/api/incoming-call'), ['method' => 'POST']);
            /*if($request->gather_action == 'start_over'){
                 $response->redirect(url('/api/incoming-call'), ['method' => 'POST']);
            }else{
                $response->redirect(url('/api/gather-url?user_id='.$request->user_id.'&gather_action='.$request->gather_action.'&call_type='.$request->call_type.'&Digits=1'), ['method' => 'POST']);
            }*/
        }
        return response($response, 200)
                  ->header('Content-Type', 'text/xml');
    }

    public function callStatus(Request $request)
    {
        $response = new VoiceResponse();
        $twilioNumber = TwilioNumber::where('number', $request->To)->with('user')->first();
        if($twilioNumber){
            $doc_number = Place::where('prison_number', $request->From)->first();
            //if($twilioNumber->user->doc_number == $request->From){
            if($doc_number){
                $call_type = 'inmate_call'; 
            }else{
                $call_type = 'user_call'; 
            }
            $callArray = [
                'status' => $request->CallStatus,
                'duration' => $request->CallDuration
            ];
            $price = Price::first();
            if($price){
                $seconds = $request->CallDuration;
                $minutes = intval($seconds/60);
                if($request->CallDuration > $minutes * 60){
                    $minutes = $minutes + 1;
                }
                $callArray['call_charge'] = $price->voice * $minutes;
            }
            Calls::where('sid',$request->CallSid)->update($callArray);

            $calls = Recording::where('listen_number', $request->From)
                        ->update(['listen_number' => null]);
            //dd($calls);
            $user = User::find($twilioNumber->user_id);
            if($user){
                $user->wallet = $user->wallet - (float)$price->voice;
                $user->save();
            }
            
        }
        
        return response($response, 200)
                  ->header('Content-Type', 'text/xml');
    }


    public function getMessageStatus(Request $request)
    {   
        //\Log::info('sms status.', ['id' => $request->all()]);
        $call = Recording::where('sid',$request->MessageSid)->first();
        if($call){
            $call->status = $request->MessageStatus;
            $call->save();
        }
        //$request->MessageSid
        $response = new VoiceResponse();
        return response($response, 200)
                  ->header('Content-Type', 'text/xml');
    }
}
