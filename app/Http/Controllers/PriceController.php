<?php

namespace App\Http\Controllers;

use App\Models\Price;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Plan;
use App\Helpers\StripeHelper;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $Price = Price::first();
        //dd($Price);
        $data['data'] = $Price;
        return view('Price.index', $data);
        // dd($add->count())
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'phone_number' => 'required|numeric|min:0',
            'sms'=>'required|numeric|min:0',
            'voice'=>'required|numeric|min:0',
            'call'=>'required|numeric|min:0'
        ],[
            'phone_number.numeric'=>'This value must be Numeric.',
            'sms.numeric'=>'This value must be Numeric',
            'voice.numeric'=>'This value must be Numeric',
            'call.numeric'=>'This value must be Numeric'

        ]);
        $data = [
            'phone_number' =>  $request->phone_number, 
            'sms' =>  $request->sms, 
            'voice' =>  $request->voice, 
            'call' =>  $request->call, 
        ];
        //dd($data);
        $Price = Price::first();

        if($Price){
            $Price =Price::where('id',$Price->id)->update($data);
        }else{
            $Price =Price::create($data);
        }


        if ($Price) {
            
            $stripe = new StripeHelper;
            $array = array('amount'=>$request->phone_number * 100 ,'currency'=>'USD','interval'=>'day','product'=>['name'=>'Price of Phone Number per month']);
            $plan = $stripe->createPlan($array);
            // dd($plan);
            if($plan){
                Plan::where('status', 'Active')->update(['status' => 'Inactive']);
                $Plan = new Plan;
                $Plan->plan_id = $plan;
                $Plan->status = 'Active';
                $Plan->save();
            }else{

            }
        }
        return redirect()->route('price')->with('success','Price Updated Successfully!');
    }
    public function amazonepolly()
    {
        $data['voicetype'] = Price::first();
        return view('Admin.amazonepolly',$data);
    }
    public function Amazonepollyupdate(Request $request)
    {
        $voicedata = [
                'voice_type'=> $request->voice_type,
        ];
        // dd($request->all());
        $voice = Price::first();
        // dd($voice);
        if ($voice){
            $voice = Price::where('id',$voice->id)->update($voicedata);
        }else{
            $voice = Price::create($voicedata);
        }          
        return redirect()->route('amazonepolly')->with('success','Amazonepolly Successfully Changed!');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function show(Price $price)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function edit(Price $price)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Price $price)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function destroy(Price $price)
    {
        //
    }
}
