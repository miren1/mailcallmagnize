<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Helpers\StripeHelper;
use App\Models\Plan;
use App\Models\Transactionhistory;
use App\Models\Price;
use App\Models\Place;
use App\Classes\TwilioClass;
use App\Models\TwilioNumber;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestEmail;
use Carbon\Carbon;

class UserController extends Controller
{
    public function testtw(Request $request)
    {
        /*$twilio = new TwilioClass;
        $array = ["areaCode" => 22];
        $dataNumber = $twilio->getAvilableNumber($array);
        //dd($dataNumber);
        //if($dataNumber){
            if(count($dataNumber) > 0){
                $purchaseNumber = $dataNumber[0]->phoneNumber;
            }else{
              $dataNumber2 = $twilio->getAvilableNumber([]);  
              $purchaseNumber = $dataNumber2[0]->phoneNumber;
            }
            $arrNumber = array(
                "phoneNumber" =>$purchaseNumber,
                "voiceUrl" => url('api/incoming-call'),
                'statusCallback' => url('api/call-satatus'),
                "smsUrl" => url('api/recive-sms')
            );
            //$sid = $twilio->purchaseNumber($arrNumber);
            dd($arrNumber);
            //$sid = '56456d4asd654dasda4d6a54d';
            if($sid){
                $number = new TwilioNumber;
                $number->sid = $sid;
                $number->user_id = '22';
               // $number->user_id = $userdata->id;
                $number->number = $purchaseNumber;
                $is_save = $number->save();
            }
            dd($sid);*/
        //}    
        dd('done');
        
    }

    public function registerdata(Request $request)
    {
        // dd($request->all());
        //["areaCode" => 510]



        $request->validate([

            'inmates_name'=>' required',


        ]);
        $userdata = new User;
        $userdata->inmates_name = $request->inmates_name;
        $userdata->doc_number = $request->doc_number;
        $userdata->inmates_location = $request->inmates_location;
        $userdata->area_code = $request->area_code;
        $userdata->name = $request->name;
        $userdata->number ='+1'.$request->number;
        $userdata->email = $request->email;
        if ($request->prison_number != null) {
            $userdata->prison_number = '+1'.$request->prison_number;
            // dd('success');
        }else{
        $userdata->prison_number = $request->prison_number;
        }
        $userdata->password = Hash::make($request->password);
         $userdata->amount_added = $request->amount_added;

        $is_save = $userdata->save();
        if ($is_save) {
          $stripe = new StripeHelper;
          $array = [
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'phone'=>'+1'.$request->number, 
                    'address' => ['country' => 'US'] 
                   ];
          $customer =  $stripe->createCustomer($array);
          if ($customer) {
            $userdata->customer_id = $customer;
            $userdata->save();
            $payment = $stripe->createPaymentMethod($customer,$request->card_token);
           
            //dd($payment);
            if ($payment) {
             $planID = Plan::where('status','Active')->first();   
             $subscribe = $stripe->createSubscription($customer,$planID->plan_id);   
             //dd($subscribe);
             $userdata->subscription_id = $subscribe->id;
             $is_save = $userdata->save();
             if ($is_save) {
                    $data = Price::select('phone_number')->first();
                    $substract = $request->amount_added - $data->phone_number;
                    $userdata->wallet = $substract;
                    $userdata_save = $userdata->save();
                    if ($userdata_save) {
                            if($request->prison_number != null) {
                            $prison_numberadd = new Place;
                            $prison_numberadd->prison_number = $request->prison_number;
                            $prison_numberadd->save();
                        }
                             
                    }
                    $twilio = new TwilioClass;
                    // $array = ["areaCode" => $request->area_code];
                    // $dataNumber = $twilio->getAvilableNumber($array);
                    // //if($dataNumber){
                    // if($dataNumber && count($dataNumber) > 0){
                    //     $purchaseNumber = $dataNumber[0]->phoneNumber;
                    // }else{
                    //   $dataNumber2 = $twilio->getAvilableNumber([]);  
                    //   $purchaseNumber = $dataNumber2[0]->phoneNumber;
                    // }
                    $arrNumber = array(
                        "phoneNumber" =>$request->selectnumber,
                        "voiceUrl" => url('api/incoming-call'),
                        'statusCallback' => url('api/call-satatus'),
                        "smsUrl" => url('api/recive-sms')
                    );
                    $sid = $twilio->purchaseNumber($arrNumber);
                    // $sid = 'dummy_56456d4asd654dasda4d6a54d';
                    if($sid){
                        $number = new TwilioNumber;
                        $number->sid = $sid;
                        $number->user_id = $userdata->id;
                        $number->number = $request->selectnumber;
                        $is_save = $number->save();
                    }
                        //dd($sid);
                    //}    

             }
            return response()->json(['success' => 'Signup successfully!']);  
             }else{
                return response()->json(['status' => 'error', 'message' => 'Signup failed!']);  
             }
            }else{
               return response()->json(['status' => 'error', 'message' => 'Signup failed!']); 
            }
        }else{
            return response()->json(['status' => 'error', 'message' => 'Signup failed!']);
        }
       //return Response()->json(['success'=>'operation success']);
    }
    public function index()
    {
        return view('User.index');
    }
    
    public function userslist(Request $request)
    {

        
       
                    // dd($request->all());
           $data = User::where('type','user')->get();
           return DataTables::of($data)
            ->addIndexColumn() 
            ->addColumn('action',function($row){
                 $var =  route('viewuser', base64_encode($row->id));
                 $btn1 ='<a href="'.$var.'"><button type="button" class="btn btn-success serviacceptbtn waves-effect waves-light" data-type="View" title="View" tabindex="0" data-plugin="tippy" data-tippy-placement="top"><i class="fa fa-eye"></i></button></a>';
                return $btn1;
              
            })
            ->rawColumns(['action'])
            ->make(true);
    
    }
    public function users_accept($id,$type,Request $request)
    {
        $status = $type;
        // dd($status);
        $add = User::find($id);
        $add->status = $status;
        $add->save();
        return response()->json(['success'=>'User Successfully']);
    }
    public function editprofile()
    {
        return view('Admin.editprofile');
    }
    public function editprofile_update(Request $request)
    {

            $request->validate([
                'name' => 'required',
                'email' => 'required|email',
                'number' => 'required|numeric|digits:10'
            ],[
                'number.digits'=>'This Phone Number must be 10 digits.'
            ]);
            $profileupdate = User::find(Auth::user()->id);
            $profileupdate->name = $request->name;
            $profileupdate->email = $request->email;
            $profileupdate->number = '+1'.$request->number;
            $profileupdate->save();

            return  redirect()->route('home')->with('success','Profile Update Successfully!');
    }
    public function todaynewuser()
    {
        $pendingusers = User::whereDay('created_at',now()->day)->where('type','user')->get();
        // dd($pendingusers); 
        return DataTables::of($pendingusers)
                ->addIndexColumn()
                ->make(true);

    }   
     public function allpendingplace(Request $request)
    {
        if ($request->ajax()) {
                
            $allpendingplace = Place::where('status','Pending')->whereDay('created_at',now()->day)->get();

            return DataTables::of($allpendingplace)
                    ->addIndexColumn()
                     ->addColumn('action', function ($row) {
                       $btn1 ='<button type="button" class="btn btn-success serviacceptbtn waves-effect waves-light" data-id="'.$row->id.'" data-type="Accepted" title="Accept" tabindex="0" data-plugin="tippy" data-tippy-placement="top"><i class="fe-check-square"></i></button>&nbsp;&nbsp;';
                         $btn2 ='<button type="button" class="btn btn-danger servirejecttbtn waves-effect waves-light" data-id="'.$row->id.'" data-type="Rejected" title="Reject" tabindex="0" data-plugin="tippy" data-tippy-placement="top"><i class="fe-x-square"></i></button>';
                        
                         return $btn1 .''.$btn2;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

        }
    }  
    // public function accepteduser()
    // {
    //     $accepteduser = User::where('status','Accepted')->where('type','user')->get();
    //     return DataTables::of($accepteduser)
    //             ->addIndexColumn()
    //             ->make(true);
    // }
    // public function rejecteduser()
    // {
    //     $rejecteduser = User::where('status','Rejected')->where('type','user')->get();
    //     return DataTables::of($rejecteduser)
    //             ->addIndexColumn()
    //             ->make(true);
    // }

    public function viewuser($id)
    {   
        $Id = base64_decode($id);
        $data['viewuser'] = User::where('type','user')->find($Id);
        // dd($data);
        return view('User.viewuser',$data);
    }



    public function allttransaction()
    {
        return view('User.allttransaction');
    }
    public function transactionhistory_list(Request $request)
    {
         // if ($request->ajax()) {
                //$data = ->get();
                //dd($data[0]->username->name);
                return DataTables::of(Transactionhistory::with(['username'])->orderBy('user_id','DESC'))
                       ->addIndexColumn() 
                       ->editColumn('user_id', function ($row) {
                                
                                    return $row->username->name;
                        })
                       ->editColumn('created_at',function($row){
                           return  $row->created_at->format('d/m/Y');
                       })
                        ->rawColumns(['user_id'])
                        ->make(true);
        // }

    }
    public function adduser()
    {
       
        return view('User.add');
    }

    public function storeuser(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'inmates_name'=>'required',
            'doc_number'=> 'required',
            'username' => 'required',
            'number'=> 'required|digits:10|unique:users',
            'email'=> 'required|unique:users',
        ]);
        $newuser =  new User;
        $newuser->inmates_name = $request->inmates_name;
        $newuser->prison_number = $request->prison_number;
        $newuser->doc_number = '+1'.$request->doc_number;
        $newuser->inmates_location = $request->inmates_location;
        $newuser->name = $request->username;
        $newuser->number = '+1'.$request->number;
        $newuser->email = $request->email;
        if($request->prison_number != null) {
            $prison_numberadd = new Place;
            $prison_numberadd->prison_number = $request->prison_number;
            $prison_numberadd->save();
        }
        $is_save = $newuser->save();

        if($is_save){
            $getuserId = User::where('email',$request->email)->first();
            // dd($getuserId);
            $id = base64_encode($getuserId->id);
            // dd($id);
            $demo = new \stdClass();
            $demo->link = url('/new/user/'.$id);
            $demo->sender = 'mirenpatel23@gmail.com';
            $demo->receiver = $request->email;
            // dd($demo->link);
            try {
            $test = Mail::to('mirenpatel23@gmail.com')->send(new TestEmail($demo));
            return redirect()->route('users')->with('success','User created successfully & User get register link  successfully!');

            }catch (\Exception $e) {
                return redirect()->route('users')->with('error','Something is Wrong!');
             }
            // dd('https://mailcall.xsquarestaging.in/new/user/'.$id);
            // return redirect('new/user/'.$id);
            // dd($test);
        }
    }

    public function newuserRegister($id)
    {
        $userid = base64_decode($id);
        $Id = $userid;
        // dd($Id);
        $plan = Price::select('phone_number')->first();

        return view('User.newuser',compact('Id','plan'));

    }
    public function checkandupdatenewuser(Request $request)
    {
        // dd($request->all());
            $validator = Validator::make($request->all(), [
                    'password' => 'required|confirmed|min:6',
                    'area_code'=> 'required|digits:3',
                    'amount_added'=>'required',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                return response()->json(['status'=> 'false', 'error' => $errors]);
            }else
            {
               
               $newdata = User::find($request->userId);
               

               if ($newdata->area_code != null) {
                    
                 return response()->json(['status'=> 'exists', 'error' => 'already exist!']);
               }else{

                // dd($newdata);
                $newdata->password = Hash::make($request->password);
                $newdata->area_code = $request->area_code;
                $newdata->amount_added = $request->amount_added;
                $is_saved = $newdata->save();
                if ($is_saved) {
                    $stripe = new StripeHelper;
                    $array = [
                            'name'=>$newdata->name,
                            'email'=>$newdata->email,
                            'phone'=>$newdata->number, 
                            'address' => ['country' => 'US'] 
                           ];
                    $customer =  $stripe->createCustomer($array);
                    if ($customer) {
                        $newdata->customer_id = $customer;
                        $newdata->save();
                        $payment = $stripe->createPaymentMethod($customer,$request->card_token);
                        if ($payment) {
                            $planID = Plan::where('status','Active')->first();   
                            $subscribe = $stripe->createSubscription($customer,$planID->plan_id); 
                            $newdata->subscription_id = $subscribe->id;
                            $its_save = $newdata->save();
                            if($its_save) {
                                    $data = Price::select('phone_number')->first();
                                    $substract = $request->amount_added - $data->phone_number;
                                    $newdata->wallet = $substract;
                                    $newdata_save = $newdata->save();
                                    if ($newdata_save) {
                                            $twilio = new TwilioClass;
                                            // $array = ["areaCode" => $request->area_code];
                                            // $dataNumber = $twilio->getAvilableNumber($array);
                                            // //if($dataNumber){
                                            // if($dataNumber && count($dataNumber) > 0){
                                            //     $purchaseNumber = $dataNumber[0]->phoneNumber;
                                            // }else{
                                            //   $dataNumber2 = $twilio->getAvilableNumber([]);  
                                            //   $purchaseNumber = $dataNumber2[0]->phoneNumber;
                                            // }
                                            $arrNumber = array(
                                                "phoneNumber" =>$request->selectnumber,
                                                "voiceUrl" => url('api/incoming-call'),
                                                'statusCallback' => url('api/call-satatus'),
                                                "smsUrl" => url('api/recive-sms')
                                            );
                                            $sid = $twilio->purchaseNumber($arrNumber);
                                            // $sid = 'dummy_56456d4asd654dasda4d6a54d';
                                            if($sid){
                                                $number = new TwilioNumber;
                                                $number->sid = $sid;
                                                $number->user_id = $newdata->id;
                                                $number->number = $request->selectnumber;
                                                $its_saved = $number->save();
                                                if ($its_saved) {
                                                    $newdata->status = 'Accepted';
                                                    $newdata->save();
                                                    // dd('success');
                                                }
                                            }
                                    }
                            } 
                            return response()->json(['status'=>'success', 'success' => 'Signup successfully!']);   
                        }else{
                            return response()->json(['status' => 'error', 'message' => 'Signup is failed!']);  
                        }
                    }else{
                       return response()->json(['status' => 'error', 'message' => 'Signup or failed!']); 
                    }
                }else{
                    return response()->json(['status' => 'error', 'message' => 'Signup and failed!']);
                }  
            }
        }
    }

    
}
