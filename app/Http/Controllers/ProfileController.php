<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\StripeHelper;
use App\Models\User;
use App\Models\Transactionhistory;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
  
      public function profile()
    {
        $stripe = new StripeHelper;
        $getcard['cards'] = $stripe->getCard(Auth::user()->customer_id);
            // dd($getcard);
            // foreach($getcard as $cards)
            // foreach($cards as $newcards)
            // dd($newcards->id);   

       return view('Front.profile'); 
    }

        // public function refillexistingcard(Request $request)
    // {
    //     dd($request->all());

         
    // }

    public function updateprofile(Request $request)
    {
        // dd($request->all());
        $validatedata =  $request->validate([
            'number'=>'required|digits:10',
        ],[ 
            'number.digits' => 'The Phone Number must be 10 digits',
        ]);


        $updateprofile = User::find(Auth::user()->id);
        // dd($updateprofile);
        $updateprofile->name = $request->name;
        $updateprofile->email = $request->email;
        $updateprofile->number = '+1'.$request->number;
        $updateprofile->save();

        return redirect()->route('home')->with('success','Profile Detail change successfully!');
    }

    public function changepassword()
    {
        return view('Front.changepassword');
    }
    public function updatechangepassword(Request $request)
    {
        // dd($request->all());
        $request->validate([

            'password'=>'required|confirmed|min:8',

        ]);      


        $user = Auth::user();    
        if(!Hash::check($request->current_password, $user->password)){
            
            return back()->with('error', 'Current password does not match!');
        }
        User::find(Auth::user()->id)->update(['password'=> Hash::make($request->password)]);
        return redirect()->route('home')->with('success','Password successfully Changed!');
    }
}
