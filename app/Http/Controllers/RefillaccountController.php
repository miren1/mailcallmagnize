<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\StripeHelper;
use App\Models\User;
use App\Models\Transactionhistory;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Hash;
use DataTables;
use App\Models\Plan;
use App\Models\TwilioNumber;


class RefillaccountController extends Controller
{

    public function refill()
    {
         $stripe = new StripeHelper;
        $getcard['cards'] = $stripe->getCard(Auth::user()->customer_id);
        if (!$getcard['cards']) {
           $getcard['cards'] = [];
        }
        $twilioNumber = TwilioNumber::where('user_id',Auth::user()->id)->first();
        return view('Front.refillwallet',compact('getcard','twilioNumber'));
    }

    public function refillcharges(Request $request)
    {
         //dd($request->all());
        if ($request->type == 'newcard') {
            
                $data = User::find(Auth::user()->id);
                $stripe = new StripeHelper;
                 
                $payment = $stripe->createPaymentMethod($data->customer_id,$request->newcard_token);
                
                $array = [
                            'amount'=>$request->amount * 100,
                            'currency'=>'USD',
                            'customer'=>$data->customer_id,
                            'source'=>$payment  
                           ];
                $charges = $stripe->createpayment($array);
                if($charges){

                // dd($charges->status);    

                $transactiondata = new Transactionhistory;
                $transactiondata->user_id = Auth::user()->id;
                $transactiondata->transaction_id = $charges->balance_transaction;
                $transactiondata->amount = $request->amount;
                $transactiondata->type = 'Refill';
                $transactiondata->status = $charges->status;
                 // dd($transactiondata);
                $transactiondata->save();

                $data->wallet  = $data->wallet + (int)$request->amount;
                $data->save();

                return response()->json(['status'=>'true','success'=>'successfully!']);
            }else{
                return response()->json([ 'status'=>'false' ,'error'=>'Something is Wrong!']);
            }
        }elseif ($request->type == 'oldcard') {
                $data = User::find(Auth::user()->id);
                $stripe = new StripeHelper;
                $customer = $data->customer_id;
                $array = [
                            'amount'=>$request->amount * 100,
                            'currency'=>'USD',
                            'customer'=>$data->customer_id,
                            'source'=>$request->cardid  
                           ];
                $charges = $stripe->createpayment($array);
                // dd($charges);
                if ($charges) {
                    
                $transactiondata = new Transactionhistory;
                $transactiondata->user_id = Auth::user()->id;
                $transactiondata->transaction_id = $charges->balance_transaction;
                $transactiondata->amount = $request->amount;
                $transactiondata->type = 'Refill';
                $transactiondata->status = $charges->status;
                $transactiondata->save();

                $data->wallet  = $data->wallet + (int)$request->amount;
                $data->save();

                return response()->json(['status'=>'true','success'=>'Payment Successfully']);
                }
                else 
                {
                return response()->json(['status'=>'false','error'=>'Something is Wrong!']);

                }

        }       
    }

    public function subcribtionResponse(Request $request)
    {
        if($request->type == 'charge.failed'){
            $status = 'failed';
        }else{
            $status = 'success';
        }
        $stripe = new StripeHelper();
        $invoice = $stripe->getIncoice($request->data['object']['invoice']);
        //$payment = new Payment;
        $subscription = false;
        //dd($invoice);
        if($invoice){

            $subscription = User::where('subscription_id',$invoice->subscription)->first();
            if($subscription){
                \Log::info('User failed to login.', ['user' => $subscription]);
                if($status == 'failed'){
                    $unsubscribe = $stripe->subscriptionDelete($invoice->subscription);
                    $subscription->status = 'Suspended';
                    $subscription->save();
                    
                }

                $transactiondata = new Transactionhistory;
                $transactiondata->user_id = $subscription->id;
                $transactiondata->transaction_id = $request->data['object']['balance_transaction'];
                $transactiondata->amount = ($request->data['object']['amount'])/100;
                $transactiondata->type = 'subscribe';
                if ($status == 'success') {
                    
                $transactiondata->status = 'succeeded';
                }else
                {
                    $transactiondata->status = 'Failed';
                }
                
                // dd($transactiondata);
                $transactiondata->save();
            }  
            //$payment->amount = ($request->data['object']['amount'])/100; 
            //$payment->tx_id = $request->data['object']['balance_transaction'];
        }
        return response()->json('',200);
    }

    public function history(Request $request)
    {
        $historydata = Transactionhistory::where('user_id',Auth::user()->id)->where('type','subscribe')->get();
        // dd($historydata);
        return DataTables::of($historydata)
                ->addIndexColumn()
                ->editColumn('created_at', function ($row) {
                    return  $row->created_at->format('d/m/Y');
                })
                ->rawColumns(['created_at'])
                ->make(true);
    }


    public function canclesubscription()
    {

        $stripe = new StripeHelper();
        $user = User::find(Auth::user()->id);
        $unsubscribe = $stripe->subscriptionDelete($user->subscription_id);
        $user->status = 'Suspended';
        $user->save();

        return response()->json(['success'=>'Subscription Cancle Successfully!']);

    }

    public function subscription(Request $request)
    {
        if ($request->type == 'subnewcard')
        {
            // dd($request->all());
            $user = User::find(Auth::user()->id);

            $stripe = new StripeHelper();
            $customer = $user->customer_id;
            $payment = $stripe->createPaymentMethod($customer,$request->subscripnewcard_token);
            if ($payment) {
            
                     $planID = Plan::where('status','Active')->first();   
                     $subscribe = $stripe->createSubscription($customer,$planID->plan_id);

                     // dd($subscribe->id);

                     $user->subscription_id = $subscribe->id;
                     $user->status = 'Accepted';
                     $user->save();
                     return response()->json(['success'=>'Subscription Successfully!']);
            }

        }
        elseif($request->type == 'subexistingcard')
        {
            // dd($request->all());
            $data = User::find(Auth::user()->id);
            $stripe = new StripeHelper;
            // $getcard['cards'] = $stripe->getCard(Auth::user()->customer_id);
            // dd($getcard);
            $customer = $data->customer_id;
            $planID = Plan::where('status','Active')->first();   
            $subscribe = $stripe->createSubscription($customer,$planID->plan_id);
           
            $data->subscription_id = $subscribe->id;
            $data->status = 'Accepted';
            $its_save =  $data->save();
            if ($its_save) {
                
                if ($subscribe->status == 'active') {
                    $datahistory = new Transactionhistory;
                    $datahistory->status = 'Succeeded';
                }else
                {
                     $datahistory = new Transactionhistory;
                    $datahistory->status = 'Failed';
                    }


            }
            return response()->json(['success'=>'Subscription Successfully']);
        }
    }
}