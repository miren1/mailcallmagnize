<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Price;
use App\Models\Calls;
use App\Models\Phonenumber;
use DataTable;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Contracts\DataTable as ContractsDataTable;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Messages;
use DB;
use App\Models\TwilioNumber;
use App\Models\User;
use App\Models\Transactionhistory;
use App\Helpers\StripeHelper;
use App\Models\Recording;

class FrontController extends Controller
{

    public function checkNumber(Request $request){
        $user = User::where('doc_number','+1'.$request->number)->first();
        if($user){
            return response()->json(['status' => 'success', 'data' => $user]);
        }else{
            return response()->json(['status' => 'error']);
        }
    }
   
    public function index()
    {
        //dd(env('STRIPE_SECRET'));
        /*$stripe = new StripeHelper;
        $subscribe = $stripe->createSubscription('cus_JmHVkeQnjGkChM','plan_JmFOijrSfeQ3Cn');
        dd($subscribe);*/
        $Price = Price::first();
        // dd($Price);
        $data['data'] = $Price;
        return view('Frontend',$data);
    }
    public function aboutus()
    {
       return view('Front.Aboutus');
    }

    public function help()
    {
      return view('Front.help');
    }
    public function calls()
    {
        return view('Front.calls');
    }
    public function calls_list(Request $request)
    {
        if($request->ajax())
        {
            $data = Calls::where('user_id',Auth::user()->id)->orderBy('created_at','DESC')->get();
            return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('created_at', function ($user) {
                return  $user->created_at->format('d/m/Y H:i:s A');
             })
            ->rawColumns(['created_at'])
            ->make(true);
        }

    }
    public function voicemail_message()
    {
      return view('Front.voicemail_message');
    }
    public function voicemail_list(Request $request)
    {
            $data = Recording::all()->where('user_id',Auth::user()->id);
            return DataTables()::of($data)
                  ->addIndexColumn()
                  ->editColumn('created_at', function ($user) {
                    return  $user->created_at->format('d/m/Y H:i:s');
                   })
                  ->addColumn('action',function($row){
                    $btn ='<button type="button" class="btn btn-danger waves-effect waves-light" title="Delete" tabindex="0" data-plugin="tippy" data-tippy-placement="top"><i class="fe-trash-2 servideletebtn" data-id="'.$row->id.'"></i></button></button>';
                    return $btn;
                  })
                  ->editColumn('message',function($row){
                    $message = $row->type;
                    // dd($message);
                    if ($message == 'voice') {
                        $ad = '<audio controls style="width: 200px; height:30px;">
                              <source src="'.$row->message.'" type="audio/mpeg">
                              </audio>';
                        return $ad;
                    }else{
                      $data1 = mb_strimwidth($row->message, 0, 26, "...");
                      return $data1;
                    }
                  
                  })
                  ->rawColumns(['created_at','action','message'])
                  ->make(true);

    
    }
   

    public function phonenumber_list(Request $request)
    {
          if($request->ajax())
          {

            $data = Phonenumber::all();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action',function($row){
                        $btn ='<button type="button" class="btn btn-danger waves-effect waves-light" title="Delete" tabindex="0" data-plugin="tippy" data-tippy-placement="top"><i class="fe-trash-2 servideletebtn" data-id="'.$row->id.'"></i></button></button>';
                    return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
          }


    }
    public function refillaccount()
    {
        return view('Front.refillaccount');
    }

    public function inmatesphonenumber(Request $request)
    {
      // dd(Auth::user()->id);
        // dd($request->all());

         $data = User::where('email',$request->email)->first();
         // dd($data->id);
                $stripe = new StripeHelper;
                 
                $payment = $stripe->createPaymentMethod($data->customer_id,$request->newtoken);
                
                $array = [
                            'amount'=>$request->amount * 100,
                            'currency'=>'USD',
                            'customer'=>$data->customer_id,
                            'source'=>$payment  
                           ];
                $charges = $stripe->createpayment($array);
                // dd($charges);
                $transactiondata = new Transactionhistory;
                $transactiondata->user_id = $data->id;
                $transactiondata->transaction_id = $charges->balance_transaction;
                $transactiondata->amount = $request->amount;
                $transactiondata->type = 'Refill';
                $transactiondata->status = 'succeeded';
                // dd($transactiondata);
                $transactiondata->save();

                $data->wallet  = $data->wallet + (int)$request->amount;
                $is_save =  $data->save();
                if ($is_save) {
                      
                      $data->name = $request->name;
                      $data->email = $request->email;
                      $data->number = $request->number;

                      $data->save();
                }
                return response()->json(['success'=>'successfully!']);
        

    }

  public function transactionhistory()
  {

        return view('Front.transactionhistory');
  }
  public function transaction_list(Request $request)
  {
    if ($request->ajax()) {
        $data = Transactionhistory::where('user_id',Auth::user()->id)->orderBy('created_at','DESC')->get();
        // dd($data);
         return DataTables()::of($data)
                  ->addIndexColumn()
                  ->editColumn('created_at', function ($user) {
                    return  $user->created_at->format('d/m/Y');
                   })
                  ->rawColumns(['created_at'])
                  ->make(true);
    }
  }


}
