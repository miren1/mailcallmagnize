<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use  Carbon\Carbon;
use App\Models\Place;
use App\Models\Transactionhistory;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //connectify('success', 'Connection Found', 'Success Message Here');

        //notify()->success('Welcome to Laravel Notify ⚡️');

        $type = Auth::user()->type;
        // dd($type);
       if($type =='user')
       {
            $balance = Auth::user()->wallet;
            // dd($balance);
            if (($balance) <= 4) {
              // Alert::success('Success Title', 'You have insufficient balance, please refill your account balance!');
              session()->flash("error", "You have insufficient balance, please refill your account balance!");
              return view('dashboard');
              
            }else{

            return view('dashboard');
            }
       }
       elseif($type=='admin'){
           
           $data['totaluser'] = User::where('type','=','user')->count(); 
           $data['newuser'] = User::whereDate('created_at', Carbon::today())->count();
           $data['pendingplace'] = Place::where('status','=','Pending')->count();
           $data['rifillaccount'] = Transactionhistory::where('type','Refill')->whereDay('created_at',now()->day)->sum('amount');
            // dd($data);

           return view('dashboard',$data);
        }
    }
}
