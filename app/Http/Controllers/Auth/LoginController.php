<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

      public function login(Request $request)
    {

        $request->request->add(['status' => 'Accepted']);

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
               
            return $this->sendLoginResponse($request);       

        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password','status');
    }

    public function logincheck(Request $request)
    {
        // dd($request->all());
        $user = User::Where('email',$request->email)->first();
        if ($user) {
            if($user->status=='Accepted'){
                return response()->json(['status'=>'success','success'=>'successfully confirmation!']);
            }else{
                return response()->json(['status'=>'false','error'=>'Your account is suspended!']);
            }
            // dd('wait admin confirmation');
            

        }else
        {
            // dd('success');
               $userdata  = User::where('email',$request->email)->first();
               if ($userdata == null) {
                   
                    return response()->json(['status'=>'not_exists','error'=>'check email']);
               }
        }

    }
}
