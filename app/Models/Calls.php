<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calls extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','sid','type','call_type','message_type','phone_number','twilio_number','status','duration','voicemail','message','voicemail_status','listen_number','call_charge','voice_charge','sms_charge'];

    /**
     * Scope a query to get call.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCall($query)
    {
        return $query->where('message_type','call');
    }

    public function scopePlay($query)
    {
        return $query->where('play_status','true');
    }

    

    /**
     * Scope a query to get message.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMessage($query)
    {
        return $query->where('message_type','message')->where('message','<>',null);
    }

    /**
     * Scope a query to DESC order by id.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDescOrderId($query)
    {
        return $query->orderBy('id','DESC');
    }

    public function scopeMessageVoice($query)
    {
        return $query->where(function($query){
                 $query->where('voicemail', '<>', null);
                 $query->orWhere('message', '<>', null);
             });
    }

    /**
     * Scope a query to get voicemail.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVoicemail($query)
    {
        return $query->where('voicemail','<>',null);
    }

    /**
     * Scope a query to get user call.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUserCall($query,$user_id)
    {
        return $query->where('user_id',$user_id);
    }


    /**
     * Scope a query to get unlisten voice mail.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUnlisten($query)
    {
        return $query->where('voicemail_status','false');
    }


    /**
     * Scope a query to get number voice mail.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNumberListen($query,$number)
    {
        return $query->where('voicemail_status','false');
    }

    /**
     * Scope a query to get inmate or  user call.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTypeCall($query,$type)
    {
        return $query->where('call_type',$type);
    }

}
