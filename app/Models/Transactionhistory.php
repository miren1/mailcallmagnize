<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Transactionhistory extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','transaction_id','amount','payment_date'];

    public function username()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

}
