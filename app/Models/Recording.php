<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recording extends Model
{
    use HasFactory;


    protected $fillable = ['sid','rsid','type','message','voice_for','voicemail_status','status','play_status','listen_number','voice_charge','call_type','twilio_number','phone_number','user_id'];

   

}
