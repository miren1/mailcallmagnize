<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ivr extends Model
{
    use HasFactory;

    protected $fillable = ['title','type','say_message','medianame','status','ivr_day'];
}
