<?php
namespace App\Classes;
use Illuminate\Support\Facades\Crypt;
class EncryptionClass {
	function __construct()
	{
		$this->method = 'AES-128-CBC';
		$this->password = 'cALm"wf"fo>H]YSE.A#5JgUu$YJt:i*}.&2@$L{egdPGE?1=qwr?f4AS3toF0Lx';
	}
	function encryptionData($data)
	{

		$iv = substr(sha1(mt_rand()), 0, 16);
		$password = sha1($this->password);

		$salt = sha1(mt_rand());
		$saltWithPassword = hash('sha256', $password.$salt);

		$encrypted = openssl_encrypt(
		  "$data", $this->method, "$saltWithPassword", null, $iv
		);
		$msg_encrypted_bundle = "$iv:$salt:$encrypted";
		return $msg_encrypted_bundle;
	}

	function decryptionData($msg_encrypted_bundle)
	{
		$password = sha1($this->password);

		$components = explode( ':', $msg_encrypted_bundle);
		$iv            = $components[0];
		$salt          = hash('sha256', $password.$components[1]);
		$encrypted_msg = $components[2];

		$decrypted_msg = openssl_decrypt(
		  $encrypted_msg, $this->method, $salt, null, $iv
		);

		if ( $decrypted_msg === false )
			return false;

		$msg = substr( $decrypted_msg, 41 );
		return $decrypted_msg;
	}
}
