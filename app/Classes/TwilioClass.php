<?php
namespace App\Classes;
use App\Setting;
use Illuminate\Support\Facades\Auth;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;
use Illuminate\Support\Facades\Log;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VoiceGrant;
use Twilio\Jwt\Grants\SyncGrant;
//use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\ChatGrant;
use Twilio\Jwt\Grants\VideoGrant;
class TwilioClass {
	//get access function for twilio
	function getAccess() {
		//$where = array('type'=>'twilio');
		//$encryptionClass = new \App\Classes\EncryptionClass();
	  	/*$setting = Setting::where($where)->first();
	  	$newObject = (object)[];
	  	$newObject->token = $encryptionClass->decryptionData($setting->token);
	  	$newObject->sid = $encryptionClass->decryptionData($setting->sid);*/
	  	//$setting = \DB::table('settings')->where('type','twilio')->first();
		//$newObject = $setting;
		//$newObject->token = $encryptionClass->decryptionData($setting->token);
	  	//$newObject->sid = $encryptionClass->decryptionData($setting->sid);
	  	//dd($newObject);
	  	/*$setting->token = $encryptionClass->decryptionData($smsCallSetting->token);
	  	$setting->sid = $encryptionClass->decryptionData($smsCallSetting->sid);*/
	  	$newObject = (object)['sid' => env('TWILIO_SID'), 'token' => env('TWILIO_TOKEN') ];
	  	return $newObject;
	}

	function createCall($to_number, $twilio_no, $url,$statusUrl, $machineDetection = false)
	{
		$arrCall =  [
						"StatusCallbackEvent" => "completed",
	                    "StatusCallback" => $statusUrl,
	                    "url" => $url,
	                ];
	    if($machineDetection){
	    	$arrCall['machineDetection'] = $machineDetection;
	    	$arrCall['AsyncAMD'] = true;
	    	$arrCall['AsyncAmdStatusCallback'] = $statusUrl;
	    }
		try {
			$auth = $this->getAccess();
			$client = new Client($auth->sid, $auth->token);
			//$client = new Client($account_sid, $auth_token);
			$call = $client->calls
	                ->create(
                 		  	$to_number,
                          	$twilio_no, // from
                          	$arrCall
	                );
	        return $call;
		} catch (\Exception $e) {
			return false;
		}

	}

	function testCall()
	{
		$auth = $this->getAccess();
			$client = new Client($auth->sid, $auth->token);
		//$client = new Client($account_sid, $auth_token);
		$to_number = '+13156468660';
		$call = $client->account->calls->create(
			'sip:'.$to_number.'@wap.thinq.com?X-account-id=14800&X-account-token=83418cfabddb166c8cf8f8b468afdb4bc12c6f09',
		    '+19494075474',
		    array(
		        "url" => "http://demo.twilio.com/docs/voice.xml"
		    )
		);
		dd($call);
	}

	function getTwilioToken($id)
	{
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
    		//return $twilio->incomingPhoneNumbers->read();
    		$clientToken = new ClientToken($auth->sid, $auth->token);
	        $clientToken->allowClientOutgoing($auth->app_sid);
	        $clientToken->allowClientIncoming($id);
	        return $clientToken->generateToken();
	        //return response()->json(['token' => $token]);
		} catch (\Exception $e) {
			//return array('status' => 'error', 'error' => $e->getMessage());
			return false;
		}
		//$this->getAccess();
		//$applicationSid = config('services.twilio')['applicationSid'];

	}

	function getTwilioJwt($id)
	{
		try {
			$auth = $this->getAccess();
	        $twilioAccountSid = $auth->sid;
			$twilioApiKey = 'SK52bf01c852af59e881e86d2cc9120db2';
			$twilioApiSecret = '4mfcB2jpzdRAmRrn1oDrEp7HCUOkIZQT';
			$outgoingApplicationSid = $auth->app_sid;
			$identity = $id;
			$token = new AccessToken(
			    $twilioAccountSid,
			    $twilioApiKey,
			    $twilioApiSecret,
			    3600,
			    $identity
			);
			$syncGrant = new SyncGrant();
			$syncGrant->setServiceSid(config('disrdialer.SYNC_API'));
			$token->addGrant($syncGrant);
			return $token->toJWT();
		} catch (\Exception $e) {
			return false;
		}
	}

	public function getDocument($name)
	{
		try {
			$auth = $this->getAccess();
			$client = new Client($auth->sid, $auth->token);

	        $sync = $client->sync;
	        if($_SERVER['SERVER_NAME'] == 'staging.disrdialer.com' || $_SERVER['SERVER_NAME'] == '127.0.0.1'){
	            $service = $sync->services->getContext(config('disrdialer.SYNC_API_STAGING')); //staging
	        }else{
	            $service = $sync->services->getContext(config('disrdialer.SYNC_API')); //live
	        }

	        $state = $service->documents($name);
	        return $state;
		} catch (\Exception $e) {
			return array('status' => 'error', 'error' => $e->getMessage());
		}

	}


	function getNumbers(){
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
    		return $twilio->incomingPhoneNumbers->read();
		} catch (\Exception $e) {
			return array('status' => 'error', 'error' => $e->getMessage());
		}
	}

	function getCalls($sid){
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
			return $twilio->calls($sid)->fetch();
		} catch (\Exception $e) {
			//dd($e->getMessage());
			return false;
		}
	}

	function getChaildCall($sid)
	{
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
			$calls = $twilio->calls->read(array(
			    "parentCallSid" => $sid
			));
			return $calls;
		} catch (\Exception $e) {
			return false;
		}

	}

	function getAvilableNumber($array,$type=null)
	{
		try {
			$auth = $this->getAccess();
			//dd($auth);
			$twilio = new Client($auth->sid, $auth->token);
			if($type && $type == 'toll_free'){
				$local = $twilio->availablePhoneNumbers("US")
	                ->tollFree
	                ->read($array, 20);
			}else{
				$local = $twilio->availablePhoneNumbers("US")
	                ->local
	                ->read($array, 20);
			}
			return $local;
		} catch (\Exception $e) {
			dd($e->getMessage());
			return false;
		}
	}

	function purchaseNumber($array)
	{
		try {
        	$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
			$incoming_phone_number = $twilio->incomingPhoneNumbers
                               		 ->create($array);
            return $incoming_phone_number->sid;
        } catch (\Exception $e) {
        	return false;
        	dd($e->getMessage());
        	//return array( 'status' => 'error', 'error' => json_encode($e->getMessage()));
        }
	}

	function sendFax($to_number,$twilio_number,$file)
	{
		$objSetting = $this->getAccess();
    	$twilio = new Client($objSetting->sid, $objSetting->token);
    	try {
        	$fax = $twilio->fax->v1->faxes
                           ->create($to_number, // to
                                    $file, // mediaUrl
                                    array(
                                        "from" => $twilio_number,
                                        'StatusCallback' => url('fax-status')
                                    )
                           );
        	return array('status' => 'success','sid'=> $fax->sid);
    	} catch (\Exception $e) {
        	return array( 'status' => 'error', 'error' => json_encode($e->getMessage()));
    	}
	}

	function getFaxData($sid){
		$objSetting = $this->getAccess();
    	$twilio = new Client($objSetting->sid, $objSetting->token);
		return $fax = $twilio->fax->v1->faxes($sid)
                       ->fetch();
	}

	function getConferenceData($cid)
	{
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
			$participants = $twilio->conferences($cid)
                       ->participants
                       ->read(array(), 20);
            return array( 'status' => 'success', 'data' => $participants);
		} catch (\Exception $e) {
			return array( 'status' => 'error', 'error' => json_encode($e->getMessage()));
		}
	}

	function fetchConferenceData($cid)
	{
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
			$participants = $twilio->conferences($cid)
                        ->fetch();
            return $participants;
		} catch (\Exception $e) {
			return false;
		}
	}



	function deleteConferenceData($cid,$callSid)
	{
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
			$twilio->conferences($cid)
			       ->participants($callSid)
			       ->delete();
            return array( 'status' => 'success');
		} catch (\Exception $e) {
			return array( 'status' => 'error', 'error' => json_encode($e->getMessage()));
		}
	}

	function updateConferenceData($cid,$callSid,$array)
	{
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
			$participant = $twilio->conferences($cid)
			 ->participants($callSid)
                      ->update($array);
            return array( 'status' => 'success');
		} catch (\Exception $e) {
			return array( 'status' => 'error', 'error' => json_encode($e->getMessage()));
		}
	}

	function endConference($cid)
	{
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
			$conference = $twilio->conferences($cid)
                     ->update(["status" => "completed"]);
            return true;
		} catch (\Exception $e) {
			return false;
		}
	}

	function fnDeleteRecording($rid)
	{
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
			$twilio->recordings($rid)
       		->delete();
            return true;
		} catch (\Exception $e) {
			return false;
			//return array( 'status' => 'error', 'error' => json_encode($e->getMessage()));
		}
	}

	function fnGetRecording($rid)
	{
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
			$recording = $twilio->video->v1->recordings($rid)
                               ->fetch();
            return $recording;
		} catch (\Exception $e) {
			return array( 'status' => 'error', 'error' => json_encode($e->getMessage()));
		}
	}

	function fnDeleteNumber($sid)
	{
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
			$twilio->incomingPhoneNumbers($sid)
       				->delete();
            return true;
		} catch (\Exception $e) {
			return false;
			//return array( 'status' => 'error', 'error' => json_encode($e->getMessage()));
		}
	}


	function fnCallUpdate($sid,$number)
	{
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
			$call = $twilio->calls($sid)
               ->update(
               	$number
               );
            return true;
		} catch (\Exception $e) {
			return false;
			//return array( 'status' => 'error', 'error' => json_encode($e->getMessage()));
		}
	}

	function fnAddSmsSetting($name,$array)
	{
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
            $service = $twilio->messaging->v1->services
                             ->create($name, // friendlyName
                                      $array
                             );
            return $service->sid;
		}catch (\Exception $e) {
			//return false;
			return array( 'status' => 'error', 'error' => $e->getMessage());
		}
	}

	function fnAddPhoneNumber($message_id,$number_id){
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
            $phone_number = $twilio->messaging->v1->services($message_id)
                                      ->phoneNumbers
                                      ->create($number_id);

			return $phone_number->sid;
		}catch (\Exception $e) {
			return false;
			//return array( 'status' => 'error', 'error' => json_encode($e->getMessage()));
		}
	}

	function fnRemovePhoneNumber($message_id,$number_id){
		try {
			$auth = $this->getAccess();
			$twilio = new Client($auth->sid, $auth->token);
            $twilio->messaging->v1->services($message_id)
                      ->phoneNumbers($number_id)
                      ->delete();
            return true;
		}catch (\Exception $e) {
			return false;
			//return array( 'status' => 'error', 'error' => json_encode($e->getMessage()));
		}
	}


	//function fnSendSms($to_number, $group_id, $body,$statusUrl)
	function fnSendSms($data)
	{
		//dd($data);
		try {
			$twilio = new Client($data['sid'], $data['token']);
			if(isset($data['images'])){
				 $message = $twilio->messages
                  ->create($data['number'], // to
                           array(
                                "body" => $data['message'],
                             	"StatusCallback" => $data['status_url'],
                             	"MessagingServiceSid" => $data['sms_setting'],
                             	'mediaUrl' => $data['images']
                           )
                  );
			}else{
				 $message = $twilio->messages
                  ->create($data['number'], // to
                           array(
                                "body" => $data['message'],
                             	"StatusCallback" => $data['status_url'],
                             	"MessagingServiceSid" => $data['sms_setting']
                           )
                  );
            }
           return array('status'=> 'success','sid' => $message->sid);
		}catch (\Exception $e) {
			return array( 'status' => 'error', 'error' => $e->getMessage());
		}

	}

	function fnSendSingleSms($to,$from,$body,$image = null)
	{
		try{
			$objSetting = getTwilioAccess();
			//$client = new Client($objSetting->project, $objSetting->signalwire_token, array("signalwireSpaceUrl" => $objSetting->signalwire_url));
			$twilio = new Client($objSetting->sid, $objSetting->token);
			$sendArray = array(
			                        	//"from" => $from,
			                        	"body" => $body,
			                        	'StatusCallback' => url('get-sms-status-inbox'),
			                        	"MessagingServiceSid" => $from
			                        );
			if($image){
				$sendArray['mediaUrl'] = $image;
			}
			$message = $twilio->messages
			                ->create($to, // to
			                     $sendArray   
			                );
			return array('status'=>$message->status,'sid'=>$message->sid);
		}catch (\Exception $e) {
			return array('status'=> 'error','error' => $e->getMessage());
		}
	}

	function getNumberType($number)
	{
		try{
			$objSetting = getTwilioAccess();
			//$client = new Client($objSetting->project, $objSetting->signalwire_token, array("signalwireSpaceUrl" => $objSetting->signalwire_url));
			$twilio = new Client($objSetting->sid, $objSetting->token);
			$phone_number = $twilio->lookups->v1->phoneNumbers($number)
                                    ->fetch(array("type" => array("carrier")));
            //dd($phone_number);
            return $phone_number->carrier['type'];
		}catch (\Exception $e) {
			//dd($e->getMessage());
			//Log::error('Twilio Lookup try error', ['error' => $e->getMessage()]);
			return null;
			//return array('status'=> 'error','error' => $e->getMessage());
		}
	}

	function geTwilioLookup($number)
	{
		try{
			$objSetting = getTwilioAccess();
			$twilio = new Client($objSetting->sid, $objSetting->token);
			$phone_number = $twilio->lookups->v1->phoneNumbers($number)
                                    ->fetch(array("type" => array("carrier")));
            return $phone_number;
		}catch (\Exception $e) {
			//Log::error('Twilio Lookup try error', ['error' => $e->getMessage()]);
			return null;
		}
	}


	function fnRecordCall($sid,$array)
	{
		try{
			$objSetting = getTwilioAccess();
			$twilio = new Client($objSetting->sid, $objSetting->token);

			$recording = $twilio->calls($sid)
			                    ->recordings
			                    ->create($array);
            return $recording->sid;
		}catch (\Exception $e) {
			//Log::error('Twilio recording try error', ['error' => $e->getMessage()]);
			return null;
		}
	}

	function fnRecordCallOff($sid,$rid)
	{
		try{
			$objSetting = getTwilioAccess();
			$twilio = new Client($objSetting->sid, $objSetting->token);

            $recording = $twilio->calls($sid)
                    ->recordings($rid)
                    ->update("stopped");
            return true;
		}catch (\Exception $e) {
			//Log::error('Twilio recording stop try error', ['error' => $e->getMessage()]);
			return null;
		}
	}

	function fnGetSync()
	{
		try {
			$objSetting = getTwilioAccess();
			$client = new Twilio\Rest\Client($objSetting->sid, $objSetting->token);

	        $sync = $client->sync;
	        if($_SERVER['SERVER_NAME'] == 'staging.disrdialer.com' || $_SERVER['SERVER_NAME'] == '127.0.0.1'){
	            $service = $sync->services->getContext(config('disrdialer.SYNC_API_STAGING')); //staging
	        }else{
	            $service = $sync->services->getContext(config('disrdialer.SYNC_API')); //live
	        }

	        $state = $service->documents("gamescore");
		} catch (\Exception $e) {
			return null;
		}

	}

	public function numberUpdate($phone_id,$array)
	{
		try {
			$objSetting = getTwilioAccess();
			$twilio = new Client($objSetting->sid, $objSetting->token);

			$incoming_phone_number = $twilio->incomingPhoneNumbers($phone_id)
			                                ->update($array);
			return true;
			//print($incoming_phone_number->friendlyName);
		} catch (\Exception $e) {
			return false;
		}
	}


	public function documentUpdate($sid,$data)
	{
		try {
			$objSetting = getTwilioAccess();
			$twilio = new Client($objSetting->sid, $objSetting->token);
			if($_SERVER['SERVER_NAME'] == 'staging.disrdialer.com' || $_SERVER['SERVER_NAME'] == '127.0.0.1'){
	            $document = $twilio->sync->v1->services(config('disrdialer.SYNC_API_STAGING')) //staging
                             ->documents($sid)
                             ->update(["data" => $data]);
	        }else{
	            $document = $twilio->sync->v1->services(config('disrdialer.SYNC_API')) //live
                             ->documents($sid)
                             ->update(["data" => $data]);
	        }
            return true;
		} catch (\Exception $e) {
			return false;
			//return array('status' => 'error', 'error' => $e->getMessage());
		}
		//$twilio = new Client($sid, $token);
	}

	public function getDocumentByID($sid)
	{
		try {
			$objSetting = getTwilioAccess();
			$twilio = new Client($objSetting->sid, $objSetting->token);
            if($_SERVER['SERVER_NAME'] == 'staging.disrdialer.com' || $_SERVER['SERVER_NAME'] == '127.0.0.1'){
	            $document = $twilio->sync->v1->services(config('disrdialer.SYNC_API_STAGING')) //staging
                             ->documents($sid)
                             ->fetch();
	        }else{
	            $document = $twilio->sync->v1->services(config('disrdialer.SYNC_API')) //live
                             ->documents($sid)
                             ->fetch();
	        }
            return $document;
		} catch (\Exception $e) {
			return false;
		}
	}

	public function getSmsInfo($sid){
		try {
			$objSetting = getTwilioAccess();
			$twilio = new Client($objSetting->sid, $objSetting->token);
            $message = $twilio->messages($sid)
                  ->fetch();
            return $message;
		} catch (\Exception $e) {
			return false;
		}
	}
}
