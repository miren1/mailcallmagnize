<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('inmates_name');
            $table->string('prison_number')->nullable();
            $table->string('doc_number');
            $table->string('inmates_location')->nullable();
            $table->string('area_code');
            $table->string('name');
            $table->string('number');
            $table->string('email')->unique();
            $table->string('amount_added');
            $table->enum('type',['admin','user'])->default('user');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->enum('status',['Accepted','Rejected'])->default('Accepted');
            $table->string('customer_id');
            $table->string('subscription_id');
            $table->float('wallet',10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
